<?php include(APPPATH."views/includes/header.php"); ?>
<div class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $title; ?></h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Examples</a></li>
			<li class="active">Blank page</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title"><?php echo $menu_title; ?></h3>
				<div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
					<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div style="margin-bottom: 10px;">
	                <a href="<?php echo base_url().'dealer/add'; ?>"><button class="btn btn-primary">Add Dealer</button></a>
	                <input type="text" id="search" data-url="<?php echo base_url().'dealer/data_search/0/'; ?>" class="form-control" placeholder="Search" style="max-width: 250px;float: right;" />
	            </div>
	            <?php if(isset($_SESSION['dealer']['message'])){ ?>
	                <div style="color:<?php echo $_SESSION['dealer']['message_color']; ?>;"><?php echo $_SESSION['dealer']['message']; ?></div>
	            <?php } ?>
	            <div id="data_search"></div>
			</div>
		</div>
	</section>
</div>
<?php 
	unset($_SESSION['dealer']);
	include(APPPATH."views/includes/footer.php"); 
?>
<script type="text/javascript">
$( document ).ready(function(){
    $('#data_search').load("<?php echo base_url().'dealer/data_search'; ?>");
});

$('#search').on('keydown', function(e){
    if(e.which == 13){
        var url_search = $(this).attr('data-url') + $(this).val();
        $('#data_search').load(url_search);
    }
});

function load_page(url_page){
    var data_search = $('#search').val();
    var url_search = url_page + data_search;
    $('#data_search').load(url_search);
}
</script>