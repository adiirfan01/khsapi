<?php include(APPPATH."views/includes/header.php"); ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1><?php echo $title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $menu_title; ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form action="<?php echo base_url().'kontraktor/edit/?id='.$id; ?>" method="POST">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-space">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            Nama Kontraktor
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" name="nama_kontraktor" value="<?php echo $detail_kontraktor->nama_kontraktor; ?>" />
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-space">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="<?php echo base_url().'Kontraktor'; ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </section>
</div>
<?php include(APPPATH."views/includes/footer.php"); ?>