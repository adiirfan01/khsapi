            <div style="margin-bottom: 10px;">
                <button class="btn btn-primary" onclick="addDealerItem()">Add Dealer for Item</button>
            </div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Dealer</td>
                        <td width="30%">Action</td>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $nourut = 1;
                    foreach($all_dealer as $data_dealer){ 
                ?>
                    <tr>
                        <td><?php echo $nourut; ?></td>
                        <td><?php echo $data_dealer->nama_dealer; ?></td>
                        <td>
                            <a href="#" onclick="deleteDealerItem('<?php echo base_url().'weekly_item/delete_dealer_item/?id_weekly_item_dealer='.base64_encode($data_dealer->id_weekly_item_dealer); ?>')"><button type="button" class="btn btn-danger">Delete</button></a>
                        </td>
                    </tr>
                <?php $nourut++; } ?>
                </tbody>
            </table>
            <div id="pagination" style="margin-bottom: 20px; margin-top: 20px;">
                <button type="button" class="btn btn-default"><<</button>
                <?php 
                    for($numb=1; $numb<=$pages; $numb++){ 
                        $curr_page = 0 + ($currentPage / 2);
                ?>
                    <button type="button" onclick="load_page('<?php echo base_url().'weekly_item/data_search/'.$numb.'/'; ?>')" class="btn btn-<?php echo ($numb-1 == (int)$curr_page ? 'reverse' : 'default'); ?> btn-page"><?php echo $numb; ?></button>
                <?php } ?>
                <button type="button" class="btn btn-default">>></button>
            </div>
