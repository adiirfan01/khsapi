            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Item</td>
                        <td width="30%">Action</td>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $nourut = 1;
                    foreach($all_weekly_item_child as $data_weekly_item_child){ 
                ?>
                    <tr>
                        <td><?php echo $nourut; ?></td>
                        <td><?php echo $data_weekly_item_child->nama_item; ?></td>
                        <td>
                            <a href="<?php echo base_url().'weekly_item_child/?parent='.base64_encode($data_weekly_item_child->id_weekly_item); ?>"><button type="button" class="btn btn-primary">Child Item</button></a>
                            <a href="<?php echo base_url().'weekly_item_child/edit/?id='.base64_encode($data_weekly_item_child->id_weekly_item_child); ?>"><button type="button" class="btn btn-default">Edit</button></a>
                            <a href="#" onclick="deleteData('<?php echo base_url().'weekly_item_child/delete/?id='.base64_encode($data_weekly_item_child->id_weekly_item_child); ?>')"><button type="button" class="btn btn-danger">Delete</button></a>
                        </td>
                    </tr>
                <?php $nourut++; } ?>
                </tbody>
            </table>
            <div id="pagination" style="margin-bottom: 20px; margin-top: 20px;">
                <button type="button" class="btn btn-default"><<</button>
                <?php 
                    for($numb=1; $numb<=$pages; $numb++){ 
                        $curr_page = 0 + ($currentPage / 2);
                ?>
                    <button type="button" onclick="load_page('<?php echo base_url().'weekly_item_child/data_search/'.$numb.'/'; ?>')" class="btn btn-<?php echo ($numb-1 == (int)$curr_page ? 'reverse' : 'default'); ?> btn-page"><?php echo $numb; ?></button>
                <?php } ?>
                <button type="button" class="btn btn-default">>></button>
            </div>

<!-- Modal -->
<div class="modal fade" id="dealerModal" tabindex="-1" role="dialog" aria-labelledby="dealerModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="dealerModalLabel" style="float: left;">Dealer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button><br/>
      </div>
      <div id="dealerModalBody" class="modal-body">
            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<input type="hidden" id="activeItem">

<script type="text/javascript">
function openModal(id){
    $.get( "<?php echo base_url().'weekly_item_child/list_dealer/'; ?>" + id, function( data ) {
        $( "#activeItem" ).val(id);
        $( "#dealerModalBody" ).html( data );
    });
    //$('#dealerModal').modal('show');
}

function deleteDealerItem(delete_url){
    var answer = confirm("Apakah Anda yakin ingin menghapus data ini ?");
    
    if (answer!=0) { 
        $.get( delete_url, function( data ) {
            var obj = jQuery.parseJSON(data);
            alert(obj.message);
            $.get( "<?php echo base_url().'weekly_item_child/list_dealer/'; ?>" + obj.id_weekly_item_child, function( data ) {
                $( "#dealerModalBody" ).html( data );
            });
        });
        $(this).closest('li').hide("slow"); 
    } 
    
    return false;
};

function addDealerItem(){
    var id = $( "#activeItem" ).val();

    $.get( "<?php echo base_url().'weekly_item_child/add_dealer_item/'; ?>" + id, function( data ) {
        $( "#dealerModalBody" ).html( data );
    });
}

function saveDealerItem(){
    var id = $( "#activeItem" ).val();

    $.post("<?php echo base_url().'weekly_item_child/add_dealer_item/'; ?>" + id, $("#frm_dealer_item").serialize(), function(data) {
        alert(data);
        $.get( "<?php echo base_url().'weekly_item_child/list_dealer/'; ?>" + id, function( data ) {
            $( "#dealerModalBody" ).html( data );
        });
    });
}

function cancelDealerItem(){
    var id = $( "#activeItem" ).val();

    $.get( "<?php echo base_url().'weekly_item_child/list_dealer/'; ?>" + id, function( data ) {
        $( "#dealerModalBody" ).html( data );
    });
}
</script>