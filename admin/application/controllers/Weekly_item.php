<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Weekly_item extends CI_Controller {

	function __construct() {
		parent::__construct();
        if ( !isset($_SESSION['login']) ) {
			redirect('login'); 
		}
		$this->load->vars(array('controller'=>'weekly_item'));
    }

	public function index()
	{
		$data['title'] = "Weekly Item";
		$data['menu_title'] = "Weekly Item - List Data";

		$this->load->view('weekly_item/data', $data);
	}

	public function data_search($page=0, $search='')
	{
		$search = urldecode($search);

		$offset = 10;

		if($page != 0){
			$limit = 0 + (($page - 1) * $offset);
		} else{
			$limit = 0;
		}

		if($search != ''){
			/*$data['all_weekly_item'] = $this->supervisor_model->data_supervisor($limit, $offset, $search);
			$all_pages = $this->supervisor_model->count_all_weekly_item($search);*/
			$all_weekly_item = curl_api('weeklyitem/listparent', 'GET');
			$data['all_weekly_item'] = $all_weekly_item->data;
			$all_pages = sizeof($data['all_weekly_item']);
		} else{
			/*$data['all_weekly_item'] = $this->supervisor_model->data_supervisor($limit, $offset);
			$all_pages = $this->supervisor_model->count_all_weekly_item();*/
			$all_weekly_item = curl_api('weeklyitem/listparent', 'GET');
			$data['all_weekly_item'] = $all_weekly_item->data;
			$all_pages = sizeof($data['all_weekly_item']);
		}

		$pages = ($all_pages % $offset == 0 ? $all_pages / $offset : ($all_pages / $offset)+1 );
		$data['pages'] = (int)$pages;
		$data['currentPage'] = $page;

		$this->load->view('weekly_item/data-search', $data);
	}

	public function add()
	{
		$data['title'] = "Add Weekly Item";
		$data['menu_title'] = "Weekly Item - Add Weekly Item";

		$post = $this->input->post();
		if($post){
			$data_item = array(
					'nama_item'	=> $post['nama_item']
				);
			$add_item = curl_api('weeklyitem/addparent', 'POST', http_build_query($data_item));
			if($add_item->success == TRUE){

				foreach($post['id_dealer'] as $id_dealer){
					$data_item_dealer = array(
							'id_dealer'	=> $id_dealer,
							'id_weekly_item'	=> $add_item->data[0]->id_weekly_item
						);
					$add_item_dealer = curl_api('weeklyitem/addparentdealer', 'POST', http_build_query($data_item_dealer));
				}

				$_SESSION['weekly_item']['message_color'] = "green";
				$_SESSION['weekly_item']['message'] = "Berhasil menambahkan Item";
				redirect('weekly_item');
			} else{
				$_SESSION['weekly_item']['message_color'] = "red";
				$_SESSION['weekly_item']['message'] = "Gagal menambahkan Item. Silahkan coba kembali nanti.";
				redirect('weekly_item');
			}
		}

		$data['list_dealer'] = curl_api('dealer', 'GET')->data;

		$this->load->view('weekly_item/add', $data);
	}

	public function edit()
	{
		$data['title'] = "Edit Supervisor";
		$data['menu_title'] = "Supervisor - Edit Supervisor";

		$id = $this->input->get('id');
		$data['id'] = $id;

		$post = $this->input->post();
		if($post){
			$data_supervisor = array(
					'nama_supervisor'	=> $post['nama_supervisor']
				);
			$update_supervisor = curl_api('weekly_item/'.base64_decode($id), 'PATCH', json_encode($data_supervisor));
			if($update_supervisor->success == TRUE){
				$_SESSION['supervisor']['message_color'] = "green";
				$_SESSION['supervisor']['message'] = "Berhasil edit data Supervisor";
				redirect('supervisor');
			} else{
				$_SESSION['supervisor']['message_color'] = "red";
				$_SESSION['supervisor']['message'] = "Gagal edit data Supervisor. Silahkan coba kembali nanti.";
				redirect('supervisor');
			}
		} else{
			$data['detail_supervisor'] = curl_api('weekly_item/'.base64_decode($id), 'GET')->data;
		}

		$this->load->view('weekly_item/edit', $data);
	}

	public function delete()
	{
		$id = $this->input->get('id');
		
		$delete_supervisor = curl_api('weekly_item/'.base64_decode($id), 'DELETE');

		if($delete_supervisor->success == TRUE){
			$_SESSION['supervisor']['message_color'] = "green";
			$_SESSION['supervisor']['message'] = "Berhasil hapus data Supervisor";
			redirect('supervisor');
		} else{
			$_SESSION['supervisor']['message_color'] = "red";
			$_SESSION['supervisor']['message'] = "Gagal hapus data Supervisor. Silahkan coba kembali nanti.";
			redirect('supervisor');
		}
	}

	public function list_dealer($id_weekly_item, $page=0, $search='')
	{
		$search = urldecode($search);

		$offset = 10;

		if($page != 0){
			$limit = 0 + (($page - 1) * $offset);
		} else{
			$limit = 0;
		}

		if($search != ''){
			/*$data['all_dealer'] = $this->supervisor_model->data_supervisor($limit, $offset, $search);
			$all_pages = $this->supervisor_model->count_all_weekly_item($search);*/
			$all_dealer = curl_api('weeklyitem/listparentdealer/'.$id_weekly_item, 'GET');
			$data['all_dealer'] = $all_dealer->data;
			$all_pages = sizeof($data['all_dealer']);
		} else{
			/*$data['all_dealer'] = $this->supervisor_model->data_supervisor($limit, $offset);
			$all_pages = $this->supervisor_model->count_all_weekly_item();*/
			$all_dealer = curl_api('weeklyitem/listparentdealer/'.$id_weekly_item, 'GET');
			$data['all_dealer'] = $all_dealer->data;
			$all_pages = sizeof($data['all_dealer']);
		}

		$pages = ($all_pages % $offset == 0 ? $all_pages / $offset : ($all_pages / $offset)+1 );
		$data['pages'] = (int)$pages;
		$data['currentPage'] = $page;

		$this->load->view('weekly_item/data-dealer', $data);
	}

	public function delete_dealer_item()
	{
		$id = $this->input->get('id_weekly_item_dealer');

		$id_weekly_item = curl_api('weeklyitem/showparentdealer/'.base64_decode($id), 'GET')->data[0]->id_weekly_item;
		
		$delete_dealer_item = curl_api('weeklyitem/deleteparentdealer/'.base64_decode($id), 'GET');

		$data_json = array();
		if($delete_dealer_item->success == TRUE){
			$data_json['message'] = "Berhasil hapus data Dealer Item";
		} else{
			$data_json['message'] = "Gagal hapus data Dealer Item. Silahkan coba kembali nanti";
		}

		$data_json['id_weekly_item'] = $id_weekly_item;

		echo json_encode($data_json);
	}

	public function add_dealer_item($id_weekly_item)
	{
		$post = $this->input->post();
		if($post){
			foreach($post['id_dealer'] as $id_dealer){
				$data_item_dealer = array(
						'id_dealer'	=> $id_dealer,
						'id_weekly_item'	=> $id_weekly_item
					);
				$add_item_dealer = curl_api('weeklyitem/addparentdealer', 'POST', http_build_query($data_item_dealer));
			}
			if(isset($add_item_dealer) && $add_item_dealer->success == TRUE){
				echo "Berhasil menambahkan data Dealer Item";
			} else{
				echo "Gagal menambahkan data Dealer Item. Silahkan coba kembali nanti";
			}
		} else{
			$data['list_dealer'] = curl_api('dealer/notinweeklyitem/'.$id_weekly_item, 'GET')->data;

			$this->load->view('weekly_item/add-dealer', $data);
		}
	}
}
