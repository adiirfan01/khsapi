<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_dealer extends CI_Controller {

	function __construct() {
		parent::__construct();
        if ( !isset($_SESSION['login']) ) {
			redirect('login'); 
		}
		$this->load->vars(array('controller'=>'main_dealer'));
    }

	public function index()
	{
		$data['title'] = "Main Dealer";
		$data['menu_title'] = "Main Dealer - List Data";

		$this->load->view('main_dealer/data', $data);
	}

	public function data_search($page=0, $search='')
	{
		$search = urldecode($search);

		$offset = 10;

		if($page != 0){
			$limit = 0 + (($page - 1) * $offset);
		} else{
			$limit = 0;
		}

		if($search != ''){
			/*$data['all_main_dealer'] = $this->Main Dealer_model->data_main_dealer($limit, $offset, $search);
			$all_pages = $this->Main Dealer_model->count_all_main_dealer($search);*/
			$all_main_dealer = curl_api('maindealer', 'GET');
			$data['all_main_dealer'] = $all_main_dealer->data;
			$all_pages = sizeof($data['all_main_dealer']);
		} else{
			/*$data['all_main_dealer'] = $this->Main Dealer_model->data_main_dealer($limit, $offset);
			$all_pages = $this->Main Dealer_model->count_all_main_dealer();*/
			$all_main_dealer = curl_api('maindealer', 'GET');
			$data['all_main_dealer'] = $all_main_dealer->data;
			$all_pages = sizeof($data['all_main_dealer']);
		}

		$pages = ($all_pages % $offset == 0 ? $all_pages / $offset : ($all_pages / $offset)+1 );
		$data['pages'] = (int)$pages;
		$data['currentPage'] = $page;

		$this->load->view('main_dealer/data-search', $data);
	}

	public function add()
	{
		$data['title'] = "Add Main Dealer";
		$data['menu_title'] = "Main Dealer - Add Main Dealer";

		$post = $this->input->post();
		if($post){
			$data_main_dealer = array(
					'nama_main_dealer'	=> $post['nama_main_dealer']
				);
			$add_main_dealer = curl_api('maindealer', 'POST', http_build_query($data_main_dealer));
			if($add_main_dealer->success == TRUE){
				$_SESSION['main_dealer']['message_color'] = "green";
				$_SESSION['main_dealer']['message'] = "Berhasil menambahkan Main Dealer";
				redirect('main_dealer');
			} else{
				$_SESSION['main_dealer']['message_color'] = "red";
				$_SESSION['main_dealer']['message'] = "Gagal menambahkan Main Dealer. Silahkan coba kembali nanti.";
				redirect('main_dealer');
			}
		}

		$this->load->view('main_dealer/add', $data);
	}

	public function edit()
	{
		$data['title'] = "Edit Main Dealer";
		$data['menu_title'] = "Main Dealer - Edit Main Dealer";

		$id = $this->input->get('id');
		$data['id'] = $id;

		$post = $this->input->post();
		if($post){
			$data_main_dealer = array(
					'nama_main_dealer'	=> $post['nama_main_dealer']
				);
			$update_main_dealer = curl_api('maindealer/'.base64_decode($id), 'PATCH', json_encode($data_main_dealer));
			if($update_main_dealer->success == TRUE){
				$_SESSION['main_dealer']['message_color'] = "green";
				$_SESSION['main_dealer']['message'] = "Berhasil edit data Main Dealer";
				redirect('main_dealer');
			} else{
				$_SESSION['main_dealer']['message_color'] = "red";
				$_SESSION['main_dealer']['message'] = "Gagal edit data Main Dealer. Silahkan coba kembali nanti.";
				redirect('main_dealer');
			}
		} else{
			$data['detail_main_dealer'] = curl_api('maindealer/'.base64_decode($id), 'GET')->data;
		}

		$this->load->view('main_dealer/edit', $data);
	}

	public function delete()
	{
		$id = $this->input->get('id');
		
		$delete_main_dealer = curl_api('maindealer/'.base64_decode($id), 'DELETE');

		if($delete_main_dealer->success == TRUE){
			$_SESSION['main_dealer']['message_color'] = "green";
			$_SESSION['main_dealer']['message'] = "Berhasil hapus data Main Dealer";
			redirect('main_dealer');
		} else{
			$_SESSION['main_dealer']['message_color'] = "red";
			$_SESSION['main_dealer']['message'] = "Gagal hapus data Main Dealer. Silahkan coba kembali nanti.";
			redirect('main_dealer');
		}
	}
}
