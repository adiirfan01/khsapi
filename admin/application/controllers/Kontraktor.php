<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontraktor extends CI_Controller {

	function __construct() {
		parent::__construct();
        if ( !isset($_SESSION['login']) ) {
			redirect('login'); 
		}
		$this->load->vars(array('controller'=>'kontraktor'));
    }

	public function index()
	{
		$data['title'] = "Kontraktor";
		$data['menu_title'] = "Kontraktor - List Data";

		$this->load->view('kontraktor/data', $data);
	}

	public function data_search($page=0, $search='')
	{
		$search = urldecode($search);

		$offset = 10;

		if($page != 0){
			$limit = 0 + (($page - 1) * $offset);
		} else{
			$limit = 0;
		}

		if($search != ''){
			/*$data['all_kontraktor'] = $this->Kontraktor_model->data_kontraktor($limit, $offset, $search);
			$all_pages = $this->Kontraktor_model->count_all_kontraktor($search);*/
			$all_kontraktor = curl_api('kontraktor', 'GET');
			$data['all_kontraktor'] = $all_kontraktor->data;
			$all_pages = sizeof($data['all_kontraktor']);
		} else{
			/*$data['all_kontraktor'] = $this->Kontraktor_model->data_kontraktor($limit, $offset);
			$all_pages = $this->Kontraktor_model->count_all_kontraktor();*/
			$all_kontraktor = curl_api('kontraktor', 'GET');
			$data['all_kontraktor'] = $all_kontraktor->data;
			$all_pages = sizeof($data['all_kontraktor']);
		}

		$pages = ($all_pages % $offset == 0 ? $all_pages / $offset : ($all_pages / $offset)+1 );
		$data['pages'] = (int)$pages;
		$data['currentPage'] = $page;

		$this->load->view('kontraktor/data-search', $data);
	}

	public function add()
	{
		$data['title'] = "Add Kontraktor";
		$data['menu_title'] = "Kontraktor - Add Kontraktor";

		$post = $this->input->post();
		if($post){
			$data_kontraktor = array(
					'nama_kontraktor'	=> $post['nama_kontraktor']
				);
			$add_kontraktor = curl_api('kontraktor', 'POST', http_build_query($data_kontraktor));
			if($add_kontraktor->success == TRUE){
				$_SESSION['kontraktor']['message_color'] = "green";
				$_SESSION['kontraktor']['message'] = "Berhasil menambahkan Kontraktor";
				redirect('kontraktor');
			} else{
				$_SESSION['kontraktor']['message_color'] = "red";
				$_SESSION['kontraktor']['message'] = "Gagal menambahkan Kontraktor. Silahkan coba kembali nanti.";
				redirect('kontraktor');
			}
		}

		$this->load->view('kontraktor/add', $data);
	}

	public function edit()
	{
		$data['title'] = "Edit Kontraktor";
		$data['menu_title'] = "Kontraktor - Edit Kontraktor";

		$id = $this->input->get('id');
		$data['id'] = $id;

		$post = $this->input->post();
		if($post){
			$data_kontraktor = array(
					'nama_kontraktor'	=> $post['nama_kontraktor']
				);
			$update_kontraktor = curl_api('kontraktor/'.base64_decode($id), 'PATCH', json_encode($data_kontraktor));
			if($update_kontraktor->success == TRUE){
				$_SESSION['kontraktor']['message_color'] = "green";
				$_SESSION['kontraktor']['message'] = "Berhasil edit data Kontraktor";
				redirect('kontraktor');
			} else{
				$_SESSION['kontraktor']['message_color'] = "red";
				$_SESSION['kontraktor']['message'] = "Gagal edit data Kontraktor. Silahkan coba kembali nanti.";
				redirect('kontraktor');
			}
		} else{
			$data['detail_kontraktor'] = curl_api('kontraktor/'.base64_decode($id), 'GET')->data;
		}

		$this->load->view('kontraktor/edit', $data);
	}

	public function delete()
	{
		$id = $this->input->get('id');
		
		$delete_kontraktor = curl_api('kontraktor/'.base64_decode($id), 'DELETE');

		if($delete_kontraktor->success == TRUE){
			$_SESSION['kontraktor']['message_color'] = "green";
			$_SESSION['kontraktor']['message'] = "Berhasil hapus data Kontraktor";
			redirect('kontraktor');
		} else{
			$_SESSION['kontraktor']['message_color'] = "red";
			$_SESSION['kontraktor']['message'] = "Gagal hapus data Kontraktor. Silahkan coba kembali nanti.";
			redirect('kontraktor');
		}
	}
}
