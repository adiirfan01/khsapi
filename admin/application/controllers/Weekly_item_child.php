<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class weekly_item_child extends CI_Controller {

	function __construct() {
		parent::__construct();
        if ( !isset($_SESSION['login']) ) {
			redirect('login'); 
		}
		$this->load->vars(array('controller'=>'weekly_item_child'));
    }

	public function index()
	{
		$parent_data = curl_api('weeklyitem/showparent/'.base64_decode($_GET['parent']), 'GET')->data[0];

		$data['title'] = "Weekly Item Child";
		$data['menu_title'] = $parent_data->nama_item." - List Child Data";

		$this->load->view('weekly_item_child/data', $data);
	}

	public function data_search($page=0, $search='')
	{
		$search = urldecode($search);

		$offset = 10;

		if($page != 0){
			$limit = 0 + (($page - 1) * $offset);
		} else{
			$limit = 0;
		}

		if($search != ''){
			/*$data['all_weekly_item_child'] = $this->supervisor_model->data_supervisor($limit, $offset, $search);
			$all_pages = $this->supervisor_model->count_all_weekly_item_child($search);*/
			$all_weekly_item_child = curl_api('weeklyitem/listchild/'.base64_decode($_GET['parent']), 'GET');
			$data['all_weekly_item_child'] = $all_weekly_item_child->data;
			$all_pages = sizeof($data['all_weekly_item_child']);
		} else{
			/*$data['all_weekly_item_child'] = $this->supervisor_model->data_supervisor($limit, $offset);
			$all_pages = $this->supervisor_model->count_all_weekly_item_child();*/
			$all_weekly_item_child = curl_api('weeklyitem/listchild/'.base64_decode($_GET['parent']), 'GET');
			$data['all_weekly_item_child'] = $all_weekly_item_child->data;
			$all_pages = sizeof($data['all_weekly_item_child']);
		}

		$pages = ($all_pages % $offset == 0 ? $all_pages / $offset : ($all_pages / $offset)+1 );
		$data['pages'] = (int)$pages;
		$data['currentPage'] = $page;

		$this->load->view('weekly_item_child/data-search', $data);
	}

	public function add()
	{
		$data['title'] = "Add Weekly Item Child";
		$data['menu_title'] = "Weekly Item - Add Weekly Item Child";

		$post = $this->input->post();
		if($post){
			$data_item = array(
					'nama_item'	=> $post['nama_item'],
					'unit_item' => $post['unit_item']
				);
			$add_item = curl_api('weeklyitem/addchild/'.base64_decode($_GET['parent']), 'POST', http_build_query($data_item));
			if($add_item->success == TRUE){
				$_SESSION['weekly_item_child']['message_color'] = "green";
				$_SESSION['weekly_item_child']['message'] = "Berhasil menambahkan Item";
				redirect('weekly_item_child/?parent='.$_GET['parent']);
			} else{
				$_SESSION['weekly_item_child']['message_color'] = "red";
				$_SESSION['weekly_item_child']['message'] = "Gagal menambahkan Item. Silahkan coba kembali nanti.";
				redirect('weekly_item_child/?parent='.$_GET['parent']);
			}
		}

		$this->load->view('weekly_item_child/add', $data);
	}

	public function edit()
	{
		$data['title'] = "Edit Supervisor";
		$data['menu_title'] = "Supervisor - Edit Supervisor";

		$id = $this->input->get('id');
		$data['id'] = $id;

		$post = $this->input->post();
		if($post){
			$data_supervisor = array(
					'nama_supervisor'	=> $post['nama_supervisor']
				);
			$update_supervisor = curl_api('weekly_item_child/'.base64_decode($id), 'PATCH', json_encode($data_supervisor));
			if($update_supervisor->success == TRUE){
				$_SESSION['supervisor']['message_color'] = "green";
				$_SESSION['supervisor']['message'] = "Berhasil edit data Supervisor";
				redirect('supervisor');
			} else{
				$_SESSION['supervisor']['message_color'] = "red";
				$_SESSION['supervisor']['message'] = "Gagal edit data Supervisor. Silahkan coba kembali nanti.";
				redirect('supervisor');
			}
		} else{
			$data['detail_supervisor'] = curl_api('weekly_item_child/'.base64_decode($id), 'GET')->data;
		}

		$this->load->view('weekly_item_child/edit', $data);
	}

	public function delete()
	{
		$id = $this->input->get('id');
		
		$delete_supervisor = curl_api('weekly_item_child/'.base64_decode($id), 'DELETE');

		if($delete_supervisor->success == TRUE){
			$_SESSION['supervisor']['message_color'] = "green";
			$_SESSION['supervisor']['message'] = "Berhasil hapus data Supervisor";
			redirect('supervisor');
		} else{
			$_SESSION['supervisor']['message_color'] = "red";
			$_SESSION['supervisor']['message'] = "Gagal hapus data Supervisor. Silahkan coba kembali nanti.";
			redirect('supervisor');
		}
	}

	public function list_dealer($id_weekly_item_child, $page=0, $search='')
	{
		$search = urldecode($search);

		$offset = 10;

		if($page != 0){
			$limit = 0 + (($page - 1) * $offset);
		} else{
			$limit = 0;
		}

		if($search != ''){
			/*$data['all_dealer'] = $this->supervisor_model->data_supervisor($limit, $offset, $search);
			$all_pages = $this->supervisor_model->count_all_weekly_item_child($search);*/
			$all_dealer = curl_api('weeklyitem/listparentdealer/'.$id_weekly_item_child, 'GET');
			$data['all_dealer'] = $all_dealer->data;
			$all_pages = sizeof($data['all_dealer']);
		} else{
			/*$data['all_dealer'] = $this->supervisor_model->data_supervisor($limit, $offset);
			$all_pages = $this->supervisor_model->count_all_weekly_item_child();*/
			$all_dealer = curl_api('weeklyitem/listparentdealer/'.$id_weekly_item_child, 'GET');
			$data['all_dealer'] = $all_dealer->data;
			$all_pages = sizeof($data['all_dealer']);
		}

		$pages = ($all_pages % $offset == 0 ? $all_pages / $offset : ($all_pages / $offset)+1 );
		$data['pages'] = (int)$pages;
		$data['currentPage'] = $page;

		$this->load->view('weekly_item_child/data-dealer', $data);
	}

	public function delete_dealer_item()
	{
		$id = $this->input->get('id_weekly_item_child_dealer');

		$id_weekly_item_child = curl_api('weeklyitem/showparentdealer/'.base64_decode($id), 'GET')->data[0]->id_weekly_item_child;
		
		$delete_dealer_item = curl_api('weeklyitem/deleteparentdealer/'.base64_decode($id), 'GET');

		$data_json = array();
		if($delete_dealer_item->success == TRUE){
			$data_json['message'] = "Berhasil hapus data Dealer Item";
		} else{
			$data_json['message'] = "Gagal hapus data Dealer Item. Silahkan coba kembali nanti";
		}

		$data_json['id_weekly_item_child'] = $id_weekly_item_child;

		echo json_encode($data_json);
	}

	public function add_dealer_item($id_weekly_item_child)
	{
		$post = $this->input->post();
		if($post){
			foreach($post['id_dealer'] as $id_dealer){
				$data_item_dealer = array(
						'id_dealer'	=> $id_dealer,
						'id_weekly_item_child'	=> $id_weekly_item_child
					);
				$add_item_dealer = curl_api('weeklyitem/addparentdealer', 'POST', http_build_query($data_item_dealer));
			}
			if(isset($add_item_dealer) && $add_item_dealer->success == TRUE){
				echo "Berhasil menambahkan data Dealer Item";
			} else{
				echo "Gagal menambahkan data Dealer Item. Silahkan coba kembali nanti";
			}
		} else{
			$data['list_dealer'] = curl_api('dealer/notinweeklyitem/'.$id_weekly_item_child, 'GET')->data;

			$this->load->view('weekly_item_child/add-dealer', $data);
		}
	}
}
