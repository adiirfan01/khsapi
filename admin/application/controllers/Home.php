<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
		parent::__construct();
        if ( !isset($_SESSION['login']) ) {
			redirect('login');
		}
    }

	public function index()
	{
		$data['title'] = "Dashboard";

		$this->load->view('home', $data);
	}

	public function logout()
	{
		$logout = curl_api('login/destroytoken', 'GET')->data;
		if($logout->logout_status == FALSE){
			echo "<script>alert('".$logout->message."');</script>";
		} else{
			session_destroy();
			redirect('home');
		}
	}
}
