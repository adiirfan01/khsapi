<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supervisor extends CI_Controller {

	function __construct() {
		parent::__construct();
        if ( !isset($_SESSION['login']) ) {
			redirect('login'); 
		}
		$this->load->vars(array('controller'=>'supervisor'));
    }

	public function index()
	{
		$data['title'] = "Supervisor";
		$data['menu_title'] = "Supervisor - List Data";

		$this->load->view('supervisor/data', $data);
	}

	public function data_search($page=0, $search='')
	{
		$search = urldecode($search);

		$offset = 10;

		if($page != 0){
			$limit = 0 + (($page - 1) * $offset);
		} else{
			$limit = 0;
		}

		if($search != ''){
			/*$data['all_supervisor'] = $this->supervisor_model->data_supervisor($limit, $offset, $search);
			$all_pages = $this->supervisor_model->count_all_supervisor($search);*/
			$all_supervisor = curl_api('supervisor', 'GET');
			$data['all_supervisor'] = $all_supervisor->data;
			$all_pages = sizeof($data['all_supervisor']);
		} else{
			/*$data['all_supervisor'] = $this->supervisor_model->data_supervisor($limit, $offset);
			$all_pages = $this->supervisor_model->count_all_supervisor();*/
			$all_supervisor = curl_api('supervisor', 'GET');
			$data['all_supervisor'] = $all_supervisor->data;
			$all_pages = sizeof($data['all_supervisor']);
		}

		$pages = ($all_pages % $offset == 0 ? $all_pages / $offset : ($all_pages / $offset)+1 );
		$data['pages'] = (int)$pages;
		$data['currentPage'] = $page;

		$this->load->view('supervisor/data-search', $data);
	}

	public function add()
	{
		$data['title'] = "Add Supervisor";
		$data['menu_title'] = "Supervisor - Add Supervisor";

		$post = $this->input->post();
		if($post){
			$data_supervisor = array(
					'nama_supervisor'	=> $post['nama_supervisor']
				);
			$add_supervisor = curl_api('supervisor', 'POST', http_build_query($data_supervisor));
			if($add_supervisor->success == TRUE){
				$_SESSION['supervisor']['message_color'] = "green";
				$_SESSION['supervisor']['message'] = "Berhasil menambahkan Supervisor";
				redirect('supervisor');
			} else{
				$_SESSION['supervisor']['message_color'] = "red";
				$_SESSION['supervisor']['message'] = "Gagal menambahkan Supervisor. Silahkan coba kembali nanti.";
				redirect('supervisor');
			}
		}

		$this->load->view('supervisor/add', $data);
	}

	public function edit()
	{
		$data['title'] = "Edit Supervisor";
		$data['menu_title'] = "Supervisor - Edit Supervisor";

		$id = $this->input->get('id');
		$data['id'] = $id;

		$post = $this->input->post();
		if($post){
			$data_supervisor = array(
					'nama_supervisor'	=> $post['nama_supervisor']
				);
			$update_supervisor = curl_api('supervisor/'.base64_decode($id), 'PATCH', json_encode($data_supervisor));
			if($update_supervisor->success == TRUE){
				$_SESSION['supervisor']['message_color'] = "green";
				$_SESSION['supervisor']['message'] = "Berhasil edit data Supervisor";
				redirect('supervisor');
			} else{
				$_SESSION['supervisor']['message_color'] = "red";
				$_SESSION['supervisor']['message'] = "Gagal edit data Supervisor. Silahkan coba kembali nanti.";
				redirect('supervisor');
			}
		} else{
			$data['detail_supervisor'] = curl_api('supervisor/'.base64_decode($id), 'GET')->data;
		}

		$this->load->view('supervisor/edit', $data);
	}

	public function delete()
	{
		$id = $this->input->get('id');
		
		$delete_supervisor = curl_api('supervisor/'.base64_decode($id), 'DELETE');

		if($delete_supervisor->success == TRUE){
			$_SESSION['supervisor']['message_color'] = "green";
			$_SESSION['supervisor']['message'] = "Berhasil hapus data Supervisor";
			redirect('supervisor');
		} else{
			$_SESSION['supervisor']['message_color'] = "red";
			$_SESSION['supervisor']['message'] = "Gagal hapus data Supervisor. Silahkan coba kembali nanti.";
			redirect('supervisor');
		}
	}
}
