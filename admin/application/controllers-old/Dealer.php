<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dealer extends CI_Controller {

	function __construct() {
		parent::__construct();
        if ( !isset($_SESSION['login']) ) {
			redirect('login'); 
		}
		$this->load->vars(array('controller'=>'dealer'));
    }

	public function index()
	{
		$data['title'] = "Dealer";
		$data['menu_title'] = "Dealer - List Data";

		$this->load->view('dealer/data', $data);
	}

	public function data_search($page=0, $search='')
	{
		$search = urldecode($search);

		$offset = 10;

		if($page != 0){
			$limit = 0 + (($page - 1) * $offset);
		} else{
			$limit = 0;
		}

		if($search != ''){
			/*$data['all_dealer'] = $this->Dealer_model->data_dealer($limit, $offset, $search);
			$all_pages = $this->Dealer_model->count_all_dealer($search);*/
			$all_dealer = curl_api('dealer', 'GET');
			$data['all_dealer'] = $all_dealer->data;
			$all_pages = sizeof($data['all_dealer']);
		} else{
			/*$data['all_dealer'] = $this->Dealer_model->data_dealer($limit, $offset);
			$all_pages = $this->Dealer_model->count_all_dealer();*/
			$all_dealer = curl_api('dealer', 'GET');
			$data['all_dealer'] = $all_dealer->data;
			$all_pages = sizeof($data['all_dealer']);
		}

		$pages = ($all_pages % $offset == 0 ? $all_pages / $offset : ($all_pages / $offset)+1 );
		$data['pages'] = (int)$pages;
		$data['currentPage'] = $page;

		$this->load->view('dealer/data-search', $data);
	}

	public function add()
	{
		$data['title'] = "Add Dealer";
		$data['menu_title'] = "Dealer - Add Dealer";

		$post = $this->input->post();
		if($post){
			$data_dealer = array(
					'nama_dealer'	=> $post['nama_dealer'],
					'latitude_dealer'	=> $post['latitude_dealer'],
					'longitude_dealer'	=> $post['longitude_dealer']
				);
			$add_dealer = curl_api('dealer', 'POST', http_build_query($data_dealer));
			if($add_dealer->success == TRUE){
				$_SESSION['dealer']['message_color'] = "green";
				$_SESSION['dealer']['message'] = "Berhasil menambahkan Dealer";
				redirect('dealer');
			} else{
				$_SESSION['dealer']['message_color'] = "red";
				$_SESSION['dealer']['message'] = "Gagal menambahkan Dealer. Silahkan coba kembali nanti.";
				redirect('dealer');
			}
		}

		$this->load->view('dealer/add', $data);
	}

	public function edit()
	{
		$data['title'] = "Edit Dealer";
		$data['menu_title'] = "Dealer - Edit Dealer";

		$id = $this->input->get('id');
		$data['id'] = $id;

		$post = $this->input->post();
		if($post){
			$data_dealer = array(
					'nama_dealer'	=> $post['nama_dealer'],
					'latitude_dealer'	=> $post['latitude_dealer'],
					'longitude_dealer'	=> $post['longitude_dealer']
				);
			$update_dealer = curl_api('dealer/'.base64_decode($id), 'PATCH', json_encode($data_dealer));
			if($update_dealer->success == TRUE){
				$_SESSION['dealer']['message_color'] = "green";
				$_SESSION['dealer']['message'] = "Berhasil edit data Dealer";
				redirect('dealer');
			} else{
				$_SESSION['dealer']['message_color'] = "red";
				$_SESSION['dealer']['message'] = "Gagal edit data Dealer. Silahkan coba kembali nanti.";
				redirect('dealer');
			}
		} else{
			$data['detail_dealer'] = curl_api('dealer/'.base64_decode($id), 'GET')->data;
		}

		$this->load->view('dealer/edit', $data);
	}

	public function delete()
	{
		$id = $this->input->get('id');
		
		$delete_dealer = curl_api('dealer/'.base64_decode($id), 'DELETE');

		if($delete_dealer->success == TRUE){
			$_SESSION['dealer']['message_color'] = "green";
			$_SESSION['dealer']['message'] = "Berhasil hapus data Dealer";
			redirect('dealer');
		} else{
			$_SESSION['dealer']['message_color'] = "red";
			$_SESSION['dealer']['message'] = "Gagal hapus data Dealer. Silahkan coba kembali nanti.";
			redirect('dealer');
		}
	}
}
