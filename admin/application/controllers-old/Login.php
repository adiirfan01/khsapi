<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
        unset($_SESSION['login']);
    }

	public function index()
	{
		$data["msg"] = "";
		if ( isset($_SESSION['login']) ) { 
			redirect('home'); 
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('user', 'Uername', 'required');
		$this->form_validation->set_rules('pass', 'Password', 'required');
		if ( $this->form_validation->run() == TRUE ) {
			$data_post = array(
			  		'username'	=> $this->input->post('user'),
			  		'password'	=> $this->input->post('pass')
			  	);

			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => API_URL."login",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => http_build_query($data_post),
			  CURLOPT_HTTPHEADER => array(
			    "Cache-Control: no-cache"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			  echo "cURL Error #:" . $err;
			} else {
				$result = json_decode($response);
				$result = $result->data;
				if($result->login_status == FALSE){
					$data["msg"] = $result->message."<br/>";
				} else{
					if($result->id_user_category != 2){
						$data["msg"] = "You have no access for this site.";
					} else{
						$_SESSION['login']['id_user'] = $result->id_user;
						$_SESSION['login']['nama_user'] = $result->nama_user;
						$_SESSION['login']['token'] = $result->token;

						redirect('home');
					}
				}
			}
		}

		$this->load->view('login', $data);
	}

	public function logout()
	{
		print_r($_SESSION);die;
	}
}
