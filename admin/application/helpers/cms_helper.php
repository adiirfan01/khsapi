<?php
	//http_build_query -> for send post form data
	function curl_api($url, $type, $data=""){
		$curl = curl_init();

		if($type == "PATCH"){
			curl_setopt_array($curl, array(
			  CURLOPT_URL => API_URL.$url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => $type,
			  CURLOPT_POSTFIELDS => $data,
			  CURLOPT_HTTPHEADER => array(
			    "Cache-Control: no-cache",
			    "Content-Type: application/json",
			    "token: ".$_SESSION['login']['token']
			  ),
			));
		} else{
			curl_setopt_array($curl, array(
			  CURLOPT_URL => API_URL.$url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => $type,
			  CURLOPT_POSTFIELDS => $data,
			  CURLOPT_HTTPHEADER => array(
			    "Cache-Control: no-cache",
			    "token: ".$_SESSION['login']['token']
			  ),
			));
		}

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		return json_decode($response);
	}
?>