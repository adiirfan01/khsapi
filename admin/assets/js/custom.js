function lihat_detail(id)
{
	var check_order = $("#check_order_" + id).val();
	
	$.ajax({
        url: base_url + "history_transaction/view",
        type: "POST",
        cache: false,
        data: {check_order: check_order},
        success : function(data){
        	//alert(data);
        }
    });
}

function deleteData(delete_url){
    var answer = confirm("Apakah Anda yakin ingin menghapus data ini ?");
    
    if (answer!=0) { 
        window.parent.location = delete_url;
        $(this).closest('li').hide("slow"); 
    } 
    
    return false;
};