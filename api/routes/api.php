<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('login/destroytoken', 'API\LoginController@destroytoken');
Route::get('login/datapengawas', 'API\LoginController@datapengawas');
Route::resource('login', 'API\LoginController');

Route::post('absen/absenlist', 'API\AbsenController@absenlist');
Route::resource('absen', 'API\AbsenController');

Route::resource('supervisor', 'API\SupervisorController');
Route::resource('kontraktor', 'API\KontraktorController');
Route::resource('maindealer', 'API\MainDealerController');
Route::get('dealer/notinweeklyitem/{id_weekly_item}', 'API\DealerController@notinweeklyitem');
Route::resource('dealer', 'API\DealerController');

Route::get('user/categories', 'API\UserController@categories');
Route::get('user/supervisor', 'API\UserController@supervisor');
Route::get('user/kontraktor', 'API\UserController@kontraktor');
Route::get('user/maindealer', 'API\UserController@maindealer');
Route::get('user/dealer', 'API\UserController@dealer');
Route::get('user/dealer', 'API\UserController@dealer');
Route::resource('user', 'API\UserController');

Route::post('dailymom/approval/{id}', 'API\DailyMomController@approval');
Route::post('dailymom/simplelist', 'API\DailyMomController@simplelist');
Route::resource('dailymom', 'API\DailyMomController');

Route::resource('dailyproblem', 'API\DailyProblemController');
Route::post('dailyproblem/simplelist', 'API\DailyProblemController@simplelist');

Route::post('dailyreport/approval/{id}', 'API\DailyReportController@approval');
Route::post('dailyreport/simplelist', 'API\DailyReportController@simplelist');
Route::get('dailyreport/topdf/{id}', 'API\DailyReportController@topdf');
Route::resource('dailyreport', 'API\DailyReportController');

Route::post('monthlyreport/approval/{id}', 'API\MonthlyReportController@approval');
Route::post('monthlyreport/simplelist', 'API\MonthlyReportController@simplelist');
Route::get('monthlyreport/topdf/{id}', 'API\MonthlyReportController@topdf');
Route::get('monthlyreport/{id}', 'API\MonthlyReportController@getbyID');
Route::post('updatemonthlyreport/{id}', 'API\MonthlyReportController@updateExtSiang');
Route::resource('monthlyreport', 'API\MonthlyReportController');

Route::post('pages/historyreport', 'API\PagesController@historyreport');

Route::get('weeklyitem/listparent', 'API\WeeklyItemController@listparent');
Route::get('weeklyitem/showparent/{id}', 'API\WeeklyItemController@showparent');
Route::post('weeklyitem/addparent', 'API\WeeklyItemController@addparent');
Route::patch('weeklyitem/editparent/{id}', 'API\WeeklyItemController@editparent')->name('weeklyitem.editparent');
Route::get('weeklyitem/deleteparent/{id}', 'API\WeeklyItemController@deleteparent');
Route::post('weeklyitem/addparentdealer', 'API\WeeklyItemController@addparentdealer');
Route::get('weeklyitem/listparentdealer/{id_weekly_item}', 'API\WeeklyItemController@listparentdealer');
Route::get('weeklyitem/deleteparentdealer/{id}', 'API\WeeklyItemController@deleteparentdealer');
Route::get('weeklyitem/showparentdealer/{id}', 'API\WeeklyItemController@showparentdealer');

Route::get('weeklyitem/listchild/{id_parent}', 'API\WeeklyItemController@listchild');
Route::post('weeklyitem/addchild/{id_parent}', 'API\WeeklyItemController@addchild');

Route::post('weeklyreport/approval/{id}', 'API\WeeklyReportController@approval');
Route::get('weeklyreport/listitemdealer/{id_dealer}', 'API\WeeklyReportController@listitemdealer');
//Route::post('weeklyreport/addreport/{id_pengawas}', 'API\WeeklyReportController@addreport');
Route::post('weeklyreport/addreport', 'API\WeeklyReportController@addreport');
Route::post('weeklyreport/addreportdetail/{id_weekly_report}', 'API\WeeklyReportController@addreportdetail');
