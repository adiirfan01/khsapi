<style>
table, td, th {    
    border: 1px solid #7b7878;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: center;
    vertical-align: middle;
}

.item, .item td, .item th {    
    border: none;
    text-align: left;
}

.item {
    border-collapse: collapse;
    width: 100%;
}

.item th, .item td {
    text-align: center;
    vertical-align: top;
}

.inner-item, .inner-item td, .inner-item th {    
    border: 1px solid #7b7878;
    text-align: left;
}

.inner-item {
    border-collapse: collapse;
    width: 100%;
}

.inner-item th, .inner-item td {
    text-align: center;
    vertical-align: middle;
}

.inner-table {    
    border: 1px solid #7b7878;
    text-align: center;
    border-collapse: collapse;
    width: 100%;
}

.inner-table td {   
    border: none;
    text-align: center;
    vertical-align: middle;
}

.inner-table hr {
  margin: 0px !important;
}

.inner-td {
  padding-top: 3px;
  padding-bottom: 3px;
  border-bottom: 1px solid #7b7878 !important;
}

.noborder {
  border: 0px !important;
}
.images-td {
  border-bottom: 1px solid #7b7878 !important;
}
.images {
  padding-top: 15px;
  padding-bottom: 15px;
}
.images p {
  margin: 0 !important;
}
</style>

<div style="text-align: center;padding-top: 30px;padding-bottom: 170px;">
  <img src="<?php echo base_path().'/public/img/logo-ahm.png'; ?>" style="width: 45%;" />
</div>
<div>
  <p style="text-align: center;font-size: 23px;line-height: 35px;">SUPERVISI REDEVELOPMENT INTERIOR DAN EKSTERIOR DEALER SEPEDA MOTOR HONDA</p>
  <br/><br/>
  <p style="text-align: center;font-size: 25px;line-height: 35px;">LAPORAN HARIAN</p>
  <br/>
  <p style="text-align: center;font-size: 21px;line-height: 35px;">
    PT MARCO MOTOR INDONESIA
    <br/>
    BLITAR
  </p>
  <p style="text-align: center;font-size: 17px;line-height: 35px;"><?php echo $convert_date; ?></p>
</div>
<div style="text-align: center;padding-top: 120px;">
  <img src="<?php echo base_path().'/public/img/logo-khs.png'; ?>" style="width: 40%;" />
</div>

<div style="page-break-after: always;">

  <table>
    <tr>
      <td style="width: 25%;padding-bottom: 10px;">
        <p style="font-size: 10px;">PEMBERI TUGAS</p>
      </td>
      <td style="width: 50%;" rowspan="2">
        <br/><p style="margin-left: 10px;margin-right: 10px;font-size: 17px;line-height: 22px;">LAPORAN PROGRESS INSTALASI EKSTERIOR DAN INTERIOR</p><br/>
      </td>
      <td style="width: 25%;padding-bottom: 10px;">
        <p style="font-size: 10px;">KONSULTAN PENGAWAS</p>
      </td>
    </tr>
    <tr>
      <td style="width: 25%;">
        <img src="<?php echo base_path().'/public/img/logo-ahm.png'; ?>" style="width: 80%;" />
      </td>
      <td style="width: 25%;">
        <img src="<?php echo base_path().'/public/img/logo-khs.png'; ?>" style="width: 80%;" />
      </td>
    </tr>
  </table>

  <br/><br/>

  <table>
    <tr>
      <td style="width: 50%;padding-top: 10px;padding-bottom: 10px;">
        <table class="noborder" style="width: 50% !important;border-collapse: unset !important;">
          <tr>
            <td class="noborder" style="width: 100px;text-align: left;">
              KONTRAKTOR
            </td>
            <td class="noborder" style="width: 10px;">
              :
            </td>
            <td class="noborder" style="width: 180px;text-align: left;">
              <?php echo $pengawas->nama_kontraktor; ?>
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 50%;">
        <table class="noborder" style="width: 50% !important;border-collapse: unset !important;">
          <tr>
            <td class="noborder" style="width: 100px;text-align: left;">
              PENGAWAS
            </td>
            <td class="noborder" style="width: 10px;">
              :
            </td>
            <td class="noborder" style="width: 180px;text-align: left;">
              <?php echo $pengawas->nama_pengawas; ?>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="width: 50%;padding-top: 10px;padding-bottom: 10px;">
        <table class="noborder" style="width: 50% !important;border-collapse: unset !important;">
          <tr>
            <td class="noborder" style="width: 100px;text-align: left;">
              DEALER
            </td>
            <td class="noborder" style="width: 10px;">
              :
            </td>
            <td class="noborder" style="width: 180px;text-align: left;">
              <?php echo $pengawas->nama_dealer; ?>
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 50%;">
        <table class="noborder" style="width: 50% !important;border-collapse: unset !important;">
          <tr>
            <td class="noborder" style="width: 100px;text-align: left;">
              WAKTU KERJA
            </td>
            <td class="noborder" style="width: 10px;">
              :
            </td>
            <td class="noborder" style="width: 180px;text-align: left;">
              <?php echo date('H:i', strtotime($start_time))." - ".date('H:i', strtotime($end_time)) ?>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="width: 50%;padding-top: 10px;padding-bottom: 10px;">
        <table class="noborder" style="width: 50% !important;border-collapse: unset !important;">
          <tr>
            <td class="noborder" style="width: 100px;text-align: left;">
              MAIN DEALER
            </td>
            <td class="noborder" style="width: 10px;">
              :
            </td>
            <td class="noborder" style="width: 180px;text-align: left;">
              <?php echo $pengawas->nama_main_dealer; ?>
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 50%;">
        <table class="noborder" style="width: 50% !important;border-collapse: unset !important;">
          <tr>
            <td class="noborder" style="width: 100px;text-align: left;">
              TANGGAL
            </td>
            <td class="noborder" style="width: 10px;">
              :
            </td>
            <td class="noborder" style="width: 180px;text-align: left;">
              <?php echo $convert_date; ?>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <br/><br/>

  <table class="item">
    <tr>
      <?php if($exterior_img != NULL){ ?>
      <td style="width: 50%;">
        <table class="inner-item">
          <tr>
            <td class="inner-td" style="width: 100%;">
              <span>FOTO EKSTERIOR</span><br/>
            </td>
          </tr>
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$exterior_img; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo ($exterior_text == NULL ? "-" : $exterior_text); ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php } ?>
      <?php if($interior_img != NULL){ ?>
      <td style="width: 50%;">
        <table class="inner-item">
          <tr>
            <td class="inner-td" style="width: 100%;">
              <span>FOTO INTERIOR</span><br/>
            </td>
          </tr>
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$interior_img; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo ($interior_text == NULL ? "-" : $interior_text); ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php } ?>
    </tr>
  </table>

  <br/><br/>

  <table border="1" style="width: 100%;">
    <tr>
      <td colspan="2"><b>PHOTO PROGRESS PEKERJAAN</b></td>
    </tr>
    <tr>
      <?php 
        foreach($progress_work[0] as $progress){ 
      ?>
      <td style="width: 50%;">
        <table class="inner-table">
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$progress->progress_img; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo $progress->progress_text; ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php
        }
      ?>
    </tr>
  </table>

</div>

<?php
  for($loop_pw=1; $loop_pw<sizeof($progress_work); $loop_pw++){
?>
<div style="page-break-after: always;">

  <table>
    <tr>
      <td style="width: 25%;padding-bottom: 10px;">
        <p style="font-size: 10px;">PEMBERI TUGAS</p>
      </td>
      <td style="width: 50%;" rowspan="2">
        <br/><p style="margin-left: 10px;margin-right: 10px;font-size: 17px;line-height: 22px;">LAPORAN PROGRESS INSTALASI EKSTERIOR DAN INTERIOR</p><br/>
      </td>
      <td style="width: 25%;padding-bottom: 10px;">
        <p style="font-size: 10px;">KONSULTAN PENGAWAS</p>
      </td>
    </tr>
    <tr>
      <td style="width: 25%;">
        <img src="<?php echo base_path().'/public/img/logo-ahm.png'; ?>" style="width: 80%;" />
      </td>
      <td style="width: 25%;">
        <img src="<?php echo base_path().'/public/img/logo-khs.png'; ?>" style="width: 80%;" />
      </td>
    </tr>
  </table>

  <br/><br/>

  <table border="1" style="width: 100%;">
    <tr>
      <?php 
        foreach($progress_work[$loop_pw] as $progress){ 
      ?>
      <td style="width: 50%;">
        <table class="inner-table">
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$progress->progress_img; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo $progress->progress_text; ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php
        }
      ?>
    </tr>
  </table>

</div>
<?php
  }
?>

<!-- <div>

  <table>
    <tr>
      <td style="width: 25%;padding-bottom: 10px;">
        <p style="font-size: 10px;">PEMBERI TUGAS</p>
      </td>
      <td style="width: 50%;" rowspan="2">
        <br/><p style="margin-left: 10px;margin-right: 10px;font-size: 17px;line-height: 22px;">LAPORAN PROGRESS INSTALASI EKSTERIOR DAN INTERIOR</p><br/>
      </td>
      <td style="width: 25%;padding-bottom: 10px;">
        <p style="font-size: 10px;">KONSULTAN PENGAWAS</p>
      </td>
    </tr>
    <tr>
      <td style="width: 25%;">
        <img src="<?php echo base_path().'/public/img/logo-ahm.png'; ?>" style="width: 80%;" />
      </td>
      <td style="width: 25%;">
        <img src="<?php echo base_path().'/public/img/logo-khs.png'; ?>" style="width: 80%;" />
      </td>
    </tr>
  </table>

  <br/><br/>

  <p style="font-size: 16px; line-height: 25px;">
    Kondisi Umum: <?php //echo ($common_condition == NULL ? "-" : $common_condition); ?><br/>
    Masalah: <?php //echo ($problem == NULL ? "-" : $problem); ?><br/>
    Anggota: <?php //echo ($attendance == NULL ? "-" : $attendance); ?><br/>
    Laporan: <?php //echo ($report == NULL ? "-" : $report); ?><br/>
  </p>

</div> -->


<!-- <table border="1" style="width: 100%;">
    <tr>
      <td colspan="2"><b>PHOTO PROGRESS PEKERJAAN</b></td>
    </tr>
    <tr>
      <td style="width: 50%;">
        <span>EXTERIOR</span>
      </td>
      <td style="width: 50%;">
        <span>EXTERIOR</span>
      </td>
    </tr>
    <tr>
      <td class="images" style="width: 50%;">
        <img src="<?php echo base_path().'/public/uploads/dailyreport/20180206-exterior-1517952125.jpeg'; ?>" style="width: 80%;" />
      </td>
      <td class="images" style="width: 50%;">
        <img src="<?php echo base_path().'/public/uploads/dailyreport/20180206-interior-1517952296.jpeg'; ?>" style="width: 80%;" />
      </td>
    </tr>
    <tr>
      <td class="images" style="width: 50%;">
        <p>dgfhgfh</p>
      </td>
      <td class="images" style="width: 50%;">
        <p>hfh</p>
      </td>
    </tr>

    <tr>
      <td style="width: 50%;">
        <span>EXTERIOR</span>
      </td>
      <td style="width: 50%;">
        <span>EXTERIOR</span>
      </td>
    </tr>
    <tr>
      <td class="images" style="width: 50%;">
        <img src="<?php echo base_path().'/public/uploads/dailyreport/20180206-exterior-1517952125.jpeg'; ?>" style="width: 80%;" />
      </td>
      <td class="images" style="width: 50%;">
        <img src="<?php echo base_path().'/public/uploads/dailyreport/20180206-interior-1517952296.jpeg'; ?>" style="width: 80%;" />
      </td>
    </tr>
    <tr>
      <td class="images" style="width: 50%;">
        <p>dgfhgfh</p>
      </td>
      <td class="images" style="width: 50%;">
        <p>hfh</p>
      </td>
    </tr>
  </table> -->