<style>
table, td, th {    
    border: 1px solid #7b7878;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: center;
    vertical-align: middle;
}

.item, .item td, .item th {    
    border: none;
    text-align: left;
}

.item {
    border-collapse: collapse;
    width: 100%;
}

.item th, .item td {
    text-align: center;
    vertical-align: top;
}

.inner-item, .inner-item td, .inner-item th {    
    border: 1px solid #7b7878;
    text-align: left;
}

.inner-item {
    border-collapse: collapse;
    width: 100%;
}

.inner-item th, .inner-item td {
    text-align: center;
    vertical-align: middle;
}

.inner-table {    
    border: 1px solid #7b7878;
    text-align: center;
    border-collapse: collapse;
    width: 100%;
}

.inner-table td {   
    border: none;
    text-align: center;
    vertical-align: middle;
}

.inner-table hr {
  margin: 0px !important;
}

.inner-td {
  padding-top: 3px;
  padding-bottom: 3px;
  border-bottom: 1px solid #7b7878 !important;
}

.noborder {
  border: 0px !important;
}
.images-td {
  border-bottom: 1px solid #7b7878 !important;
}
.images {
  padding-top: 15px;
  padding-bottom: 15px;
}
.images p {
  margin: 0 !important;
}
</style>

<div style="text-align: center;padding-top: 30px;padding-bottom: 170px;">
  <img src="<?php echo base_path().'/public/img/logo-ahm.png'; ?>" style="width: 45%;" />
</div>
<div>
  <p style="text-align: center;font-size: 23px;line-height: 35px;">SUPERVISI REDEVELOPMENT INTERIOR DAN EKSTERIOR DEALER SEPEDA MOTOR HONDA</p>
  <br/><br/>
  <p style="text-align: center;font-size: 25px;line-height: 35px;">LAPORAN BULANAN</p>
  <br/>
  <p style="text-align: center;font-size: 21px;line-height: 35px;">
    PT MARCO MOTOR INDONESIA
    <br/>
    BLITAR
  </p>
  <p style="text-align: center;font-size: 17px;line-height: 35px;"><?php echo $convert_date; ?></p>
</div>
<div style="text-align: center;padding-top: 120px;">
  <img src="<?php echo base_path().'/public/img/logo-khs.png'; ?>" style="width: 40%;" />
</div>

<div style="page-break-after: always;">

  <table>
    <tr>
      <td style="width: 25%;padding-bottom: 10px;">
        <p style="font-size: 10px;">PEMBERI TUGAS</p>
      </td>
      <td style="width: 50%;" rowspan="2">
        <br/><p style="margin-left: 10px;margin-right: 10px;font-size: 17px;line-height: 22px;">LAPORAN PROGRESS INSTALASI EKSTERIOR DAN INTERIOR</p><br/>
      </td>
      <td style="width: 25%;padding-bottom: 10px;">
        <p style="font-size: 10px;">KONSULTAN PENGAWAS</p>
      </td>
    </tr>
    <tr>
      <td style="width: 25%;">
        <img src="<?php echo base_path().'/public/img/logo-ahm.png'; ?>" style="width: 80%;" />
      </td>
      <td style="width: 25%;">
        <img src="<?php echo base_path().'/public/img/logo-khs.png'; ?>" style="width: 80%;" />
      </td>
    </tr>
  </table>

  <br/><br/>

  <table>
    <tr>
      <td style="width: 50%;padding-top: 10px;padding-bottom: 10px;">
        <table class="noborder" style="width: 50% !important;border-collapse: unset !important;">
          <tr>
            <td class="noborder" style="width: 100px;text-align: left;">
              KONTRAKTOR
            </td>
            <td class="noborder" style="width: 10px;">
              :
            </td>
            <td class="noborder" style="width: 180px;text-align: left;">
              <?php echo $pengawas->nama_kontraktor; ?>
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 50%;">
        <table class="noborder" style="width: 50% !important;border-collapse: unset !important;">
          <tr>
            <td class="noborder" style="width: 100px;text-align: left;">
              PENGAWAS
            </td>
            <td class="noborder" style="width: 10px;">
              :
            </td>
            <td class="noborder" style="width: 180px;text-align: left;">
              <?php echo $pengawas->nama_pengawas; ?>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="width: 50%;padding-top: 10px;padding-bottom: 10px;">
        <table class="noborder" style="width: 50% !important;border-collapse: unset !important;">
          <tr>
            <td class="noborder" style="width: 100px;text-align: left;">
              DEALER
            </td>
            <td class="noborder" style="width: 10px;">
              :
            </td>
            <td class="noborder" style="width: 180px;text-align: left;">
              <?php echo $pengawas->nama_dealer; ?>
            </td>
          </tr>
        </table>
      </td>
      <td style="width: 50%;">
        <table class="noborder" style="width: 50% !important;border-collapse: unset !important;">
          <tr>
            <td class="noborder" style="width: 100px;text-align: left;">
              MAIN DEALER
            </td>
            <td class="noborder" style="width: 10px;">
              :
            </td>
            <td class="noborder" style="width: 180px;text-align: left;">
              <?php echo $pengawas->nama_main_dealer; ?>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <br/><br/>

  <table class="item">
    <tr>
      <?php if(isset($images[0][0])){ ?>
      <td style="width: 50%;">
        <table class="inner-item">
          <tr>
            <td class="inner-td" style="width: 100%;">
              <span><?php echo $images[0][0]['title']; ?></span><br/>
            </td>
          </tr>
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$images[0][0]['image']; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo $images[0][0]['caption']; ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php } ?>
      <?php if(isset($images[0][1])){ ?>
      <td style="width: 50%;">
        <table class="inner-item">
          <tr>
            <td class="inner-td" style="width: 100%;">
              <span><?php echo $images[0][1]['title']; ?></span><br/>
            </td>
          </tr>
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$images[0][1]['image']; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo $images[0][1]['caption']; ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php } ?>
    </tr>
    <tr>
      <?php if(isset($images[0][2])){ ?>
      <td style="width: 50%;">
        <table class="inner-item">
          <tr>
            <td class="inner-td" style="width: 100%;">
              <span><?php echo $images[0][2]['title']; ?></span><br/>
            </td>
          </tr>
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$images[0][2]['image']; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo $images[0][2]['caption']; ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php } ?>
      <?php if(isset($images[0][3])){ ?>
      <td style="width: 50%;">
        <table class="inner-item">
          <tr>
            <td class="inner-td" style="width: 100%;">
              <span><?php echo $images[0][3]['title']; ?></span><br/>
            </td>
          </tr>
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$images[0][3]['image']; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo $images[0][3]['caption']; ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php } ?>
    </tr>
  </table>

</div>

<?php
  for($loop=1; $loop<sizeof($images)-1; $loop++){
?>
<div style="page-break-after: always;">

  <table>
    <tr>
      <td style="width: 25%;padding-bottom: 10px;">
        <p style="font-size: 10px;">PEMBERI TUGAS</p>
      </td>
      <td style="width: 50%;" rowspan="2">
        <br/><p style="margin-left: 10px;margin-right: 10px;font-size: 17px;line-height: 22px;">LAPORAN PROGRESS INSTALASI EKSTERIOR DAN INTERIOR</p><br/>
      </td>
      <td style="width: 25%;padding-bottom: 10px;">
        <p style="font-size: 10px;">KONSULTAN PENGAWAS</p>
      </td>
    </tr>
    <tr>
      <td style="width: 25%;">
        <img src="<?php echo base_path().'/public/img/logo-ahm.png'; ?>" style="width: 80%;" />
      </td>
      <td style="width: 25%;">
        <img src="<?php echo base_path().'/public/img/logo-khs.png'; ?>" style="width: 80%;" />
      </td>
    </tr>
  </table>

  <br/><br/>

  <table class="item">
    <tr>
      <?php if(isset($images[$loop][0])){ ?>
      <td style="width: 50%;">
        <table class="inner-item">
          <tr>
            <td class="inner-td" style="width: 100%;">
              <span><?php echo $images[$loop][0]['title']; ?></span><br/>
            </td>
          </tr>
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$images[$loop][0]['image']; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo $images[$loop][0]['caption']; ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php } ?>
      <?php if(isset($images[$loop][1])){ ?>
      <td style="width: 50%;">
        <table class="inner-item">
          <tr>
            <td class="inner-td" style="width: 100%;">
              <span><?php echo $images[$loop][1]['title']; ?></span><br/>
            </td>
          </tr>
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$images[$loop][1]['image']; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo $images[$loop][1]['caption']; ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php } ?>
    </tr>
    <tr>
      <?php if(isset($images[$loop][2])){ ?>
      <td style="width: 50%;">
        <table class="inner-item">
          <tr>
            <td class="inner-td" style="width: 100%;">
              <span><?php echo $images[$loop][2]['title']; ?></span><br/>
            </td>
          </tr>
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$images[$loop][2]['image']; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo $images[$loop][2]['caption']; ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php } ?>
      <?php if(isset($images[$loop][3])){ ?>
      <td style="width: 50%;">
        <table class="inner-item">
          <tr>
            <td class="inner-td" style="width: 100%;">
              <span><?php echo $images[$loop][3]['title']; ?></span><br/>
            </td>
          </tr>
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$images[$loop][3]['image']; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo $images[$loop][3]['caption']; ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php } ?>
    </tr>
  </table>

</div>
<?php
  }
?>

<div>

  <table>
    <tr>
      <td style="width: 25%;padding-bottom: 10px;">
        <p style="font-size: 10px;">PEMBERI TUGAS</p>
      </td>
      <td style="width: 50%;" rowspan="2">
        <br/><p style="margin-left: 10px;margin-right: 10px;font-size: 17px;line-height: 22px;">LAPORAN PROGRESS INSTALASI EKSTERIOR DAN INTERIOR</p><br/>
      </td>
      <td style="width: 25%;padding-bottom: 10px;">
        <p style="font-size: 10px;">KONSULTAN PENGAWAS</p>
      </td>
    </tr>
    <tr>
      <td style="width: 25%;">
        <img src="<?php echo base_path().'/public/img/logo-ahm.png'; ?>" style="width: 80%;" />
      </td>
      <td style="width: 25%;">
        <img src="<?php echo base_path().'/public/img/logo-khs.png'; ?>" style="width: 80%;" />
      </td>
    </tr>
  </table>

  <br/><br/>

  <?php $last = sizeof($images)-1; ?>
  <table class="item">
    <tr>
      <?php if(isset($images[$last][0])){ ?>
      <td style="width: 50%;">
        <table class="inner-item">
          <tr>
            <td class="inner-td" style="width: 100%;">
              <span><?php echo $images[$last][0]['title']; ?></span><br/>
            </td>
          </tr>
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$images[$last][0]['image']; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo $images[$last][0]['caption']; ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php } ?>
      <?php if(isset($images[$last][1])){ ?>
      <td style="width: 50%;">
        <table class="inner-item">
          <tr>
            <td class="inner-td" style="width: 100%;">
              <span><?php echo $images[$last][1]['title']; ?></span><br/>
            </td>
          </tr>
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$images[$last][1]['image']; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo $images[$last][1]['caption']; ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php } ?>
    </tr>
    <tr>
      <?php if(isset($images[$last][2])){ ?>
      <td style="width: 50%;">
        <table class="inner-item">
          <tr>
            <td class="inner-td" style="width: 100%;">
              <span><?php echo $images[$last][2]['title']; ?></span><br/>
            </td>
          </tr>
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$images[$last][2]['image']; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo $images[$last][2]['caption']; ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php } ?>
      <?php if(isset($images[$last][3])){ ?>
      <td style="width: 50%;">
        <table class="inner-item">
          <tr>
            <td class="inner-td" style="width: 100%;">
              <span><?php echo $images[$last][3]['title']; ?></span><br/>
            </td>
          </tr>
          <tr>
            <td class="images-td images" style="width: 100%;">
              <img src="<?php echo base_path().'/public/'.$images[$last][3]['image']; ?>" style="width: 80%;" />
            </td>
          </tr>
          <tr>
            <td class="images" style="width: 100%;">
              <p><?php echo $images[$last][3]['caption']; ?></p>
            </td>
          </tr>
        </table>
      </td>
      <?php } ?>
    </tr>
  </table>

</div>