<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>KHS API Documentation</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .links > a:hover {
                color: black;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .item {
                text-align: left;
                margin: 10px 10px 10px 100px;
            }

            .item p {
                color: #b5b5b5;
                font-weight: bold;
            }

            .item-url {
                color: #ffffff;
                font-weight: bold;
                background-color: #565555;
                padding: 13px;
            }

            table {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 70%;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            table tr:nth-child(even){background-color: #f2f2f2;}

            table tr:hover {background-color: #ddd;}

            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #901c1c;
                color: white;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <!-- <div class="flex-center position-ref full-height"> -->
        <div class="position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <br/>
                    KHS API Documentation
                </div>

                <div class="links">
                    <a href="#login">Login</a>
                    <a href="#logout">Logout</a>
                    <br/><br/>

                    <a href="#absen_pagi">Absen Pagi</a>
                    <a href="#absen_sore">Absen Sore</a>
                    <a href="#absen_list">List Absen</a>
                    <br/><br/>

                    <a href="#daily_report">Create Daily Report</a>
                    <a href="#daily_report_list">Daily Report List</a>
                    <a href="#daily_report_detail">Lihat Daily Report</a>
                    <br/><br/>

                    <a href="#monthly_report">Create Monthly Report</a>
                    <a href="#monthly_report_list">Monthly Report List</a>
                    <br/><br/>

                    <a href="#history_report">History All Report</a>
                    <br/><br/>

                    <!-- <a href="#supervisor">Data Supervisor</a>
                    <a href="#kontraktor">Data Kontraktor</a>
                    <a href="#main_dealer">Data Main Dealer</a>
                    <a href="#dealer">Data Dealer</a><br/><br/> -->

                    <a href="#list_supervisor">List Supervisor</a>
                    <a href="#view_supervisor">Lihat Supervisor</a>
                    <a href="#add_supervisor">Tambah Supervisor</a>
                    <a href="#edit_supervisor">Edit Supervisor</a>
                    <a href="#delete_supervisor">Delete Supervisor</a>
                    <br/><br/>

                    <a href="#list_kontraktor">List Kontraktor</a>
                    <a href="#view_kontraktor">Lihat Kontraktor</a>
                    <a href="#add_kontraktor">Tambah Kontraktor</a>
                    <a href="#edit_kontraktor">Edit Kontraktor</a>
                    <a href="#delete_kontraktor">Delete Kontraktor</a>
                    <br/><br/>

                    <a href="#list_maindealer">List Main Dealer</a>
                    <a href="#view_maindealer">Lihat Main Dealer</a>
                    <a href="#add_maindealer">Tambah Main Dealer</a>
                    <a href="#edit_maindealer">Edit Main Dealer</a>
                    <a href="#delete_maindealer">Delete Main Dealer</a>
                    <br/><br/>

                    <a href="#list_dealer">List Dealer</a>
                    <a href="#view_dealer">Lihat Dealer</a>
                    <a href="#add_dealer">Tambah Dealer</a>
                    <a href="#edit_dealer">Edit Dealer</a>
                    <a href="#delete_dealer">Delete Dealer</a>
                    <br/><br/>

                    <a href="#user_category">Kategori User</a>
                    <a href="#add_user">Tambah User</a>
                    <a href="#edit_user">Edit User</a>
                    <a href="#delete_user">Delete User</a>
                </div>

                <br/><br/><br/><br/><br/>

                <!-- Start Line Login -->
                <br/><br/><br/>
                <div class="item" id="login">
                    <h2>Login</h2>
                    <p>
                        Berfungsi mendapat token yang digunakan untuk mengakses ke semua halaman yang ada di dalam API.<br/>
                        User yang dapat login adalah user yang sudah terdaftar pada sistem.<br/><br/>
                        Token dikirimkan sebagai header saat melakukan request ke API.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: POST
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/login
                    </span>

                    <br/><br/><br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>username</td>
                            <td>Input username dari user</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>password</td>
                            <td>Input password dari user</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "id_user": 3,
                                        "id_user_category": 3,
                                        "nama_user": "User Pengawas",
                                        "username": "user3",
                                        "password": "92877af70a45fd6a2ed7fe81e1236b78",
                                        "created_at": "2018-01-30 00:00:00",
                                        "updated_at": "2018-01-30 00:00:00",
                                        "login_status": true,
                                        "token": "897d973ca51b3c0c4105c8b37a3768ce",
                                        "message": "Login Success."
                                    },
                                    "message": "Login retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="logout">
                    <h2>Logout</h2>
                    <p>
                        Menghapus akses dari token user yang telah login.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/login/destroytoken
                    </span>

                    <br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "logout_status": true,
                                        "message": "Logout Success."
                                    },
                                    "message": "Logout successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- End Line Login -->

                <!-- Start Line Absen -->
                <br/><br/><br/>
                <div class="item" id="absen_pagi">
                    <h2>Absen Pagi</h2>
                    <p>
                        Untuk melakukan absen pagi, mulai dari pukul 08:00 - 09:00.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: POST
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/absen
                    </span>

                    <br/><br/><br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>tgl_absen</td>
                            <td>Tanggal user absen ( harus sesuai dengan hari ia klik absen )</td>
                            <td>DATE ( Y-m-d )</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>start_absen</td>
                            <td>Waktu absen pagi</td>
                            <td>TIME ( H:i )</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>start_latitude</td>
                            <td>Lokasi latitude user pada maps/gps</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>start_longitude</td>
                            <td>Lokasi longitude user pada maps/gps</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "tgl_absen": "2018-02-03",
                                        "start_absen": "08:00",
                                        "start_latitude": "-6.242423",
                                        "start_longitude": "106.844445",
                                        "id_pengawas": 1,
                                        "updated_at": "2018-02-02 23:44:36",
                                        "created_at": "2018-02-02 23:44:36",
                                        "id_absen": 4
                                    },
                                    "message": "Insert absence successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="absen_sore">
                    <h2>Absen Sore</h2>
                    <p>
                        Untuk melakukan absen sore, mulai dari pukul 17:00 - 23:59.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: POST
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/absen
                    </span>

                    <br/><br/><br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>tgl_absen</td>
                            <td>Tanggal user absen ( harus sesuai dengan hari ia klik absen )</td>
                            <td>DATE ( Y-m-d )</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>end_absen</td>
                            <td>Waktu absen sore</td>
                            <td>TIME ( H:i )</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>end_latitude</td>
                            <td>Lokasi latitude user pada maps/gps</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>end_longitude</td>
                            <td>Lokasi longitude user pada maps/gps</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "id_absen": 4,
                                        "id_pengawas": 1,
                                        "tgl_absen": "2018-02-03",
                                        "start_absen": "08:00:00",
                                        "start_latitude": "-6.242423",
                                        "start_longitude": "106.844445",
                                        "end_absen": "17:30",
                                        "end_latitude": "-6.242507",
                                        "end_longitude": "106.844353",
                                        "created_at": "2018-02-02 23:44:36",
                                        "updated_at": "2018-02-02 23:48:32"
                                    },
                                    "message": "Absence updated successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="absen_list">
                    <h2>List Absen</h2>
                    <p>
                        Untuk melihat list absen.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: POST
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/absen/absenlist
                    </span>

                    <br/><br/><br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>id_pengawas</td>
                            <td>ID Pengawas, parameter ini digunakan jika ingin melihat absen dari salah satu pengawas</td>
                            <td>INT</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>id_dealer</td>
                            <td>ID Dealer, parameter ini digunakan jika ingin melihat absen pegawai dari salah satu dealer</td>
                            <td>INT</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>tgl_absen</td>
                            <td>Tanggal Absen, parameter ini digunakan jika ingin melihat absen pada tanggal tertentu</td>
                            <td>DATE ( Y-m-d )</td>
                            <td>FALSE</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": [
                                        {
                                            "id_absen": 1,
                                            "id_pengawas": 1,
                                            "tgl_absen": "2018-02-02",
                                            "start_absen": "08:00:00",
                                            "start_latitude": "-6.242423",
                                            "start_longitude": "106.844445",
                                            "end_absen": "17:30:00",
                                            "end_latitude": "-6.242423",
                                            "end_longitude": "106.844445",
                                            "created_at": "2018-02-02 21:35:03",
                                            "updated_at": "2018-02-02 21:44:52"
                                        }
                                    ],
                                    "message": "Absences retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- End Line Login -->

                <!-- Start Line Daily Report -->
                <br/><br/><br/>
                <div class="item" id="daily_report">
                    <h2>Create Daily Report</h2>
                    <p>
                        Untuk membuat daily report ( hanya dapat dilakukan oleh user petugas ).
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: POST
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/dailyreport
                    </span>

                    <br/><br/><br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>date_report</td>
                            <td>Tanggal user membuat daily report ( harus sesuai dengan hari ia membuat daily report )</td>
                            <td>DATE ( Y-m-d )</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>start_time</td>
                            <td>Waktu mulai pekerjaan</td>
                            <td>TIME ( H:i )</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>end_time</td>
                            <td>Waktu selesai pekerjaan</td>
                            <td>TIME ( H:i )</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>exterior_img</td>
                            <td>Gambar Exterior</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>exterior_text</td>
                            <td>Keterangan dari gambar Exterior</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>interior_img</td>
                            <td>Gambar Interior</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>interior_text</td>
                            <td>Keterangan dari gambar Interior</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>common_condition</td>
                            <td>Kondisi umum proyek</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>problem</td>
                            <td>Masalah yang terjadi pada proyek</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>attendance</td>
                            <td></td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>report</td>
                            <td></td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>progress_work</td>
                            <td>Total dari gambar dari progress work yang diupload</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>progress_img_1</td>
                            <td>Gambar untuk progress work. Format kolomnya: progress_img_{item}.</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>progress_text_1</td>
                            <td>Keterangan dari gambar untuk progress work. Format kolomnya: progress_text_{item}.</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "date_report": "2018-02-06",
                                        "start_time": "08:22",
                                        "end_time": "19:22",
                                        "exterior_img": "uploads/dailyreport/20180206-exterior-1517952296.jpeg",
                                        "interior_img": "uploads/dailyreport/20180206-interior-1517952296.jpeg",
                                        "id_pengawas": 1,
                                        "updated_at": "2018-02-06 21:24:56",
                                        "created_at": "2018-02-06 21:24:56",
                                        "id_daily_report": 10
                                    },
                                    "message": "Insert daily report successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="daily_report_list">
                    <h2>List Daily Report</h2>
                    <p>
                        List Daily Report.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/dailyreport
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": [
                                        {
                                            "id_daily_report": 10,
                                            "id_pengawas": 1,
                                            "date_report": "2018-02-06",
                                            "start_time": "08:22:00",
                                            "end_time": "19:22:00",
                                            "exterior_img": "uploads/dailyreport/20180206-exterior-1517952296.jpeg",
                                            "exterior_text": null,
                                            "interior_img": "uploads/dailyreport/20180206-interior-1517952296.jpeg",
                                            "interior_text": null,
                                            "common_condition": null,
                                            "problem": null,
                                            "attendance": null,
                                            "report": null,
                                            "created_at": "2018-02-06 21:24:56",
                                            "updated_at": "2018-02-06 21:24:56",
                                            "daily_report_progress": [
                                                {
                                                    "id_daily_report_progress": 18,
                                                    "id_daily_report": 10,
                                                    "progress_img": "uploads/dailyreport_progress/20180206-1-1517952296.jpeg",
                                                    "progress_text": "tes ya",
                                                    "created_at": "2018-02-06 21:24:57",
                                                    "updated_at": "2018-02-06 21:24:57"
                                                },
                                                {
                                                    "id_daily_report_progress": 19,
                                                    "id_daily_report": 10,
                                                    "progress_img": "uploads/dailyreport_progress/20180206-2-1517952297.jpeg",
                                                    "progress_text": null,
                                                    "created_at": "2018-02-06 21:24:57",
                                                    "updated_at": "2018-02-06 21:24:57"
                                                }
                                            ]
                                        }
                                    ],
                                    "message": "List daily report retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="daily_report_detail">
                    <h2>Lihat Daily Report</h2>
                    <p>
                        Melihat data Daily Report.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/dailyreport/{id_daily_report}
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "id_daily_report": 11,
                                        "id_pengawas": 1,
                                        "date_report": "2018-02-07",
                                        "start_time": "08:22:00",
                                        "end_time": "19:22:00",
                                        "exterior_img": "uploads/dailyreport/20180208-exterior-1518124416.jpeg",
                                        "exterior_text": null,
                                        "interior_img": "uploads/dailyreport/20180208-interior-1518124416.jpeg",
                                        "interior_text": null,
                                        "common_condition": null,
                                        "problem": null,
                                        "attendance": null,
                                        "report": null,
                                        "created_at": "2018-02-08 21:13:36",
                                        "updated_at": "2018-02-08 21:13:36",
                                        "daily_report_progress": [
                                            {
                                                "id_daily_report_progress": 21,
                                                "id_daily_report": 11,
                                                "progress_img": "uploads/dailyreport_progress/20180208-2-1518124416.jpeg",
                                                "progress_text": null,
                                                "created_at": "2018-02-08 21:13:36",
                                                "updated_at": "2018-02-08 21:13:36"
                                            }
                                        ]
                                    },
                                    "message": "Login retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- End Line Daily Report -->

                <!-- Start Line Monthly Report -->
                <br/><br/><br/>
                <div class="item" id="monthly_report">
                    <h2>Create Monthly Report</h2>
                    <p>
                        Untuk membuat monthly report ( hanya dapat dilakukan oleh user petugas ).
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: POST
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/monthlyreport
                    </span>

                    <br/><br/><br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>date_report</td>
                            <td>Tanggal user membuat monthly report ( harus sesuai dengan hari ia membuat monthly report )</td>
                            <td>DATE ( Y-m-d )</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>exterior_siang</td>
                            <td>Gambar Exterior Siang</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>exterior_siang_text</td>
                            <td>Keterangan dari gambar Exterior Siang</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>exterior_malam</td>
                            <td>Gambar Exterior Malam</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>exterior_malam_text</td>
                            <td>Keterangan dari gambar Exterior Malam</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>sales_finance_front</td>
                            <td>Gambar Sales Finance Front</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>sales_finance_front_text</td>
                            <td>Keterangan dari gambar Sales Finance Front</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>parts_front</td>
                            <td>Gambar Parts Front</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>parts_front_text</td>
                            <td>Keterangan dari gambar Parts Front</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>service_front</td>
                            <td>Gambar Service Front</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>service_front_text</td>
                            <td>Keterangan dari gambar Service Front</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>advisor_desk</td>
                            <td>Gambar Advisor Desk</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>advisor_desk_text</td>
                            <td>Keterangan dari gambar Advisor Desk</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>hero_stage</td>
                            <td>Gambar Hero Stage</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>hero_stage_text</td>
                            <td>Keterangan dari gambar Hero Stage</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>isolate_stage_1</td>
                            <td>Gambar Isolate Stage 1</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>isolate_stage_1_text</td>
                            <td>Keterangan dari gambar Isolate Stage 1</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>isolate_stage_2</td>
                            <td>Gambar Isolate Stage 1</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>isolate_stage_2_text</td>
                            <td>Keterangan dari gambar Isolate Stage 2</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>wall_system_display</td>
                            <td>Gambar Wall System Display</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>wall_system_display_text</td>
                            <td>Keterangan dari gambar Wall System Display</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>glass_showcase_1</td>
                            <td>Gambar Glass Showcase 1</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>glass_showcase_1_text</td>
                            <td>Keterangan dari gambar Glass Showcase 1</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>glass_showcase_2</td>
                            <td>Gambar Glass Showcase 2</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>glass_showcase_2_text</td>
                            <td>Keterangan dari gambar Glass Showcase 2</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>spec_stand_1</td>
                            <td>Gambar Spec Stand 1</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>spec_stand_1_text</td>
                            <td>Keterangan dari gambar Spec Stand 1</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>spec_stand_2</td>
                            <td>Gambar Spec Stand 2</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>spec_stand_2_text</td>
                            <td>Keterangan dari gambar Spec Stand 2</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>spec_stand_3</td>
                            <td>Gambar Spec Stand 3</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>spec_stand_3_text</td>
                            <td>Keterangan dari gambar Spec Stand 3</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>spec_stand_4</td>
                            <td>Gambar Spec Stand 4</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>spec_stand_4_text</td>
                            <td>Keterangan dari gambar Spec Stand 4</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>spec_stand_5</td>
                            <td>Gambar Spec Stand 5</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>spec_stand_5_text</td>
                            <td>Keterangan dari gambar Spec Stand 5</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>brochure_rack</td>
                            <td>Gambar Brochure Rack</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>brochure_rack_text</td>
                            <td>Keterangan dari gambar Brochure Rack</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>internet_counter</td>
                            <td>Gambar Internet Counter</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>internet_counter_text</td>
                            <td>Keterangan dari gambar Internet Counter</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>ci_logo</td>
                            <td>Gambar CI Logo</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>ci_logo_text</td>
                            <td>Keterangan dari gambar CI Logo</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_bengkel</td>
                            <td>Gambar Direction Bengkel</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_bengkel_text</td>
                            <td>Keterangan dari gambar Direction Bengkel</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_penjualan</td>
                            <td>Gambar Direction Penjualan</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_penjualan_text</td>
                            <td>Keterangan dari gambar Direction Penjualan</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_ruang_tunggu</td>
                            <td>Gambar Direction Ruang Tunggu</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_ruang_tunggu_text</td>
                            <td>Keterangan dari gambar Direction Ruang Tunggu</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_toilet</td>
                            <td>Gambar Direction Toilet</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_toilet_text</td>
                            <td>Keterangan dari gambar Direction Toilet</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_pit</td>
                            <td>Gambar Direction Pit</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_pit_text</td>
                            <td>Keterangan dari gambar Direction Pit</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_kasir</td>
                            <td>Gambar Direction Kasir</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_kasir_text</td>
                            <td>Keterangan dari gambar Direction Kasir</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_toilet_wall</td>
                            <td>Gambar Direction Toilet Wall</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_toilet_wall_text</td>
                            <td>Keterangan dari gambar Direction Toilet Wall</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_smoking</td>
                            <td>Gambar Direction Smoking</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>direction_smoking_text</td>
                            <td>Keterangan dari gambar Direction Smoking</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>panel_poster_display</td>
                            <td>Gambar Panel Poster Display</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>panel_poster_display_text</td>
                            <td>Keterangan dari gambar Panel Poster Display</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>poster_display</td>
                            <td>Gambar Poster Display</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>poster_display_text</td>
                            <td>Keterangan dari gambar Poster Display</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>spareparts_display</td>
                            <td>Gambar Spareparts Display</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>spareparts_display_text</td>
                            <td>Keterangan dari gambar Spareparts Display</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>helmet_rack</td>
                            <td>Gambar Helmet Rack</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>helmet_rack_text</td>
                            <td>Keterangan dari gambar Helmet Rack</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>magazine_rack</td>
                            <td>Gambar Magazine Rack</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>magazine_rack_text</td>
                            <td>Keterangan dari gambar Magazine Rack</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>banner_stand</td>
                            <td>Gambar Banner Stand</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>banner_stand_text</td>
                            <td>Keterangan dari gambar Banner Stand</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>island_display</td>
                            <td>Gambar Island Display</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>island_display_text</td>
                            <td>Keterangan dari gambar Island Display</td>
                            <td>VARCHAR</td>
                            <td>FALSE</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "date_report": "2018-02-05",
                                        "exterior_siang": "uploads/monthlyreport/20180214-exterior_siang-1518591305.jpeg",
                                        "id_pengawas": 1,
                                        "updated_at": "2018-02-14 06:55:05",
                                        "created_at": "2018-02-14 06:55:05",
                                        "id_monthly_report": 1
                                    },
                                    "message": "Insert monthly report successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="daily_report_list">
                    <h2>List Daily Report</h2>
                    <p>
                        List Daily Report.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/dailyreport
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": [
                                        {
                                            "id_daily_report": 10,
                                            "id_pengawas": 1,
                                            "date_report": "2018-02-06",
                                            "start_time": "08:22:00",
                                            "end_time": "19:22:00",
                                            "exterior_img": "uploads/dailyreport/20180206-exterior-1517952296.jpeg",
                                            "exterior_text": null,
                                            "interior_img": "uploads/dailyreport/20180206-interior-1517952296.jpeg",
                                            "interior_text": null,
                                            "common_condition": null,
                                            "problem": null,
                                            "attendance": null,
                                            "report": null,
                                            "created_at": "2018-02-06 21:24:56",
                                            "updated_at": "2018-02-06 21:24:56",
                                            "daily_report_progress": [
                                                {
                                                    "id_daily_report_progress": 18,
                                                    "id_daily_report": 10,
                                                    "progress_img": "uploads/dailyreport_progress/20180206-1-1517952296.jpeg",
                                                    "progress_text": "tes ya",
                                                    "created_at": "2018-02-06 21:24:57",
                                                    "updated_at": "2018-02-06 21:24:57"
                                                },
                                                {
                                                    "id_daily_report_progress": 19,
                                                    "id_daily_report": 10,
                                                    "progress_img": "uploads/dailyreport_progress/20180206-2-1517952297.jpeg",
                                                    "progress_text": null,
                                                    "created_at": "2018-02-06 21:24:57",
                                                    "updated_at": "2018-02-06 21:24:57"
                                                }
                                            ]
                                        }
                                    ],
                                    "message": "List daily report retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- End Line Monthly Report -->

                <!-- Start Line History Report -->
                <br/><br/><br/>
                <div class="item" id="history_report">
                    <h2>History All Report</h2>
                    <p>
                        Untuk melihat list history dari semua report.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: POST
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/pages/historyreport
                    </span>

                    <br/><br/><br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>id_pengawas</td>
                            <td>ID Pengawas, parameter ini digunakan jika ingin melihat history report dari salah satu pengawas</td>
                            <td>INT</td>
                            <td>FALSE</td>
                        </tr>
                        <tr>
                            <td>date_report</td>
                            <td>Date Report, parameter ini digunakan jika ingin melihat history report pada tanggal tertentu</td>
                            <td>DATE ( Y-m-d )</td>
                            <td>FALSE</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": [
                                        {
                                            "id_report": 1,
                                            "report_type": 3,
                                            "id_pengawas": 1,
                                            "date_report": "2018-02-05",
                                            "nama_dealer": "Dealer 1"
                                        },
                                        {
                                            "id_report": 10,
                                            "report_type": 1,
                                            "id_pengawas": 1,
                                            "date_report": "2018-02-06",
                                            "nama_dealer": "Dealer 1"
                                        },
                                        {
                                            "id_report": 11,
                                            "report_type": 1,
                                            "id_pengawas": 1,
                                            "date_report": "2018-02-07",
                                            "nama_dealer": "Dealer 1"
                                        }
                                    ],
                                    "message": "List history report retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- End Line History Report -->

                <!-- <br/><br/><br/>
                <div class="item" id="supervisor">
                    <h2>Data Supervisor</h2>
                    <p>
                        List Supervisor untuk Pengawas.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/user/supervisor
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": [
                                        {
                                            "id_supervisor": 1,
                                            "nama_supervisor": "Supervisor 1"
                                        }
                                    ],
                                    "message": "Supervisor retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="kontraktor">
                    <h2>Data Kontraktor</h2>
                    <p>
                        List Kontraktor untuk Pengawas.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/user/kontraktor
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": [
                                        {
                                            "id_kontraktor": 1,
                                            "nama_kontraktor": "Kontraktor 1"
                                        }
                                    ],
                                    "message": "Kontraktor retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="main_dealer">
                    <h2>Data Main Dealer</h2>
                    <p>
                        List Main Dealer untuk Pengawas.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/user/maindealer
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": [
                                        {
                                            "id_main_dealer": 1,
                                            "nama_main_dealer": "Main Dealer 1"
                                        }
                                    ],
                                    "message": "Main Dealer retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="dealer">
                    <h2>Data Dealer</h2>
                    <p>
                        List Dealer untuk Pengawas.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/user/dealer
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": [
                                        {
                                            "id_dealer": 1,
                                            "nama_dealer": "Dealer 1",
                                            "latitude_dealer": "-6.242423",
                                            "longitude_dealer": "106.844445"
                                        }
                                    ],
                                    "message": "Dealer retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div> -->

                <!-- Start Line Supervisor -->
                <br/><br/><br/>
                <div class="item" id="list_supervisor">
                    <h2>List Supervisor</h2>
                    <p>
                        List Supervisor.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/supervisor
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": [
                                        {
                                            "id_supervisor": 1,
                                            "nama_supervisor": "Supervisor 1",
                                            "created_at": "2018-02-01 00:00:00",
                                            "updated_at": "2018-02-01 00:00:00"
                                        }
                                    ],
                                    "message": "List supervisor retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="view_supervisor">
                    <h2>Lihat Supervisor</h2>
                    <p>
                        Melihat detail Supervisor.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/supervisor/{id_supervisor}
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "id_supervisor": 1,
                                        "nama_supervisor": "Supervisor 1",
                                        "created_at": "2018-02-01 00:00:00",
                                        "updated_at": "2018-02-01 00:00:00"
                                    },
                                    "message": "Supervisor retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="add_supervisor">
                    <h2>Tambah Supervisor</h2>
                    <p>
                        Menambahkan data Supervisor.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: POST
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/supervisor
                    </span>

                    <br/><br/><br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>nama_supervisor</td>
                            <td>Nama Supervisor</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "nama_supervisor": "Supervisor 3",
                                        "updated_at": "2018-02-08 17:01:25",
                                        "created_at": "2018-02-08 17:01:25",
                                        "id_supervisor": 3
                                    },
                                    "message": "Supervisor created successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="edit_supervisor">
                    <h2>Edit Supervisor</h2>
                    <p>
                        Mengubah data Supervisor.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: PATCH
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/supervisor/{id_supervisor}
                    </span>

                    <br/><br/><br/>
                    <h4>Additional Header</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Value</th>
                        </tr>
                        <tr>
                            <td>Content-Type</td>
                            <td>application/json</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>nama_supervisor</td>
                            <td>Nama Supervisor</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "id_supervisor": 4,
                                        "nama_supervisor": "Supervisor 4",
                                        "created_at": "2018-02-08 17:02:26",
                                        "updated_at": "2018-02-08 17:16:48"
                                    },
                                    "message": "Supervisor updated successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="edit_supervisor">
                    <h2>Delete Supervisor</h2>
                    <p>
                        Menghapus data Supervisor.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: DELETE
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/supervisor/{id_supervisor}
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": "4",
                                    "message": "Supervisor deleted successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- End Line Supervisor -->

                <!-- Start Line Kontraktor -->
                <br/><br/><br/>
                <div class="item" id="list_kontraktor">
                    <h2>List Kontraktor</h2>
                    <p>
                        List Kontraktor.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/kontraktor
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": [
                                        {
                                            "id_kontraktor": 1,
                                            "nama_kontraktor": "Kontraktor 1",
                                            "created_at": "2018-02-01 00:00:00",
                                            "updated_at": "2018-02-01 00:00:00"
                                        }
                                    ],
                                    "message": "List kontraktor retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="view_kontraktor">
                    <h2>Lihat Kontraktor</h2>
                    <p>
                        Melihat detail Kontraktor.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/kontraktor/{id_kontraktor}
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "id_kontraktor": 1,
                                        "nama_kontraktor": "Kontraktor 1",
                                        "created_at": "2018-02-01 00:00:00",
                                        "updated_at": "2018-02-01 00:00:00"
                                    },
                                    "message": "Kontraktor retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="add_kontraktor">
                    <h2>Tambah Kontraktor</h2>
                    <p>
                        Menambahkan data Kontraktor.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: POST
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/kontraktor
                    </span>

                    <br/><br/><br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>nama_kontraktor</td>
                            <td>Nama Kontraktor</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "nama_kontraktor": "Kontraktor 2",
                                        "updated_at": "2018-02-08 17:30:22",
                                        "created_at": "2018-02-08 17:30:22",
                                        "id_kontraktor": 2
                                    },
                                    "message": "Kontraktor created successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="edit_kontraktor">
                    <h2>Edit Kontraktor</h2>
                    <p>
                        Mengubah data Kontraktor.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: PATCH
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/kontraktor/{id_kontraktor}
                    </span>

                    <br/><br/><br/>
                    <h4>Additional Header</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Value</th>
                        </tr>
                        <tr>
                            <td>Content-Type</td>
                            <td>application/json</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>nama_kontraktor</td>
                            <td>Nama Kontraktor</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "id_kontraktor": 2,
                                        "nama_kontraktor": "Kontraktor 3",
                                        "created_at": "2018-02-08 17:30:22",
                                        "updated_at": "2018-02-08 17:32:09"
                                    },
                                    "message": "Kontraktor updated successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="edit_kontraktor">
                    <h2>Delete Kontraktor</h2>
                    <p>
                        Menghapus data Kontraktor.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: DELETE
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/kontraktor/{id_kontraktor}
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": "2",
                                    "message": "Kontraktor deleted successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- End Line Kontraktor -->

                <!-- Start Line Main Dealer -->
                <br/><br/><br/>
                <div class="item" id="list_maindealer">
                    <h2>List Main Dealer</h2>
                    <p>
                        List Main Dealer.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/maindealer
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": [
                                        {
                                            "id_main_dealer": 1,
                                            "nama_main_dealer": "Main Dealer 1",
                                            "created_at": "2018-02-01 00:00:00",
                                            "updated_at": "2018-02-01 00:00:00"
                                        }
                                    ],
                                    "message": "List main dealer retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="view_maindealer">
                    <h2>Lihat Main Dealer</h2>
                    <p>
                        Melihat detail Main Dealer.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/maindealer/{id_main_dealer}
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "id_main_dealer": 1,
                                        "nama_main_dealer": "Main Dealer 1",
                                        "created_at": "2018-02-01 00:00:00",
                                        "updated_at": "2018-02-01 00:00:00"
                                    },
                                    "message": "Main Dealer retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="add_maindealer">
                    <h2>Tambah Main Dealer</h2>
                    <p>
                        Menambahkan data Main Dealer.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: POST
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/maindealer
                    </span>

                    <br/><br/><br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>nama_main_dealer</td>
                            <td>Nama Main Dealer</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "nama_main_dealer": "Main Dealer 2",
                                        "updated_at": "2018-02-08 20:35:33",
                                        "created_at": "2018-02-08 20:35:33",
                                        "id_main_dealer": 2
                                    },
                                    "message": "Main Dealer created successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="edit_maindealer">
                    <h2>Edit Main Dealer</h2>
                    <p>
                        Mengubah data Main Dealer.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: PATCH
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/maindealer/{id_main_dealer}
                    </span>

                    <br/><br/><br/>
                    <h4>Additional Header</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Value</th>
                        </tr>
                        <tr>
                            <td>Content-Type</td>
                            <td>application/json</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>nama_main_dealer</td>
                            <td>Nama Main Dealer</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "id_main_dealer": 2,
                                        "nama_main_dealer": "Main Dealer 3",
                                        "created_at": "2018-02-08 20:35:33",
                                        "updated_at": "2018-02-08 20:36:31"
                                    },
                                    "message": "Main Dealer updated successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="edit_maindealer">
                    <h2>Delete Main Dealer</h2>
                    <p>
                        Menghapus data Main Dealer.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: DELETE
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/maindealer/{id_main_dealer}
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": "2",
                                    "message": "Main Dealer deleted successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- End Line Main Dealer -->

                <!-- Start Line Dealer -->
                <br/><br/><br/>
                <div class="item" id="list_dealer">
                    <h2>List Dealer</h2>
                    <p>
                        List Dealer.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/dealer
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": [
                                        {
                                            "id_dealer": 1,
                                            "nama_dealer": "Dealer 1",
                                            "latitude_dealer": "-6.242423",
                                            "longitude_dealer": "106.844445",
                                            "created_at": "2018-02-01 00:00:00",
                                            "updated_at": "2018-02-01 00:00:00"
                                        }
                                    ],
                                    "message": "List dealer retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="view_dealer">
                    <h2>Lihat Dealer</h2>
                    <p>
                        Melihat detail Dealer.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/dealer/{id_dealer}
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "id_dealer": 1,
                                        "nama_dealer": "Dealer 1",
                                        "latitude_dealer": "-6.242423",
                                        "longitude_dealer": "106.844445",
                                        "created_at": "2018-02-01 00:00:00",
                                        "updated_at": "2018-02-01 00:00:00"
                                    },
                                    "message": "Dealer retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="add_dealer">
                    <h2>Tambah Dealer</h2>
                    <p>
                        Menambahkan data Dealer.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: POST
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/dealer
                    </span>

                    <br/><br/><br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>nama_dealer</td>
                            <td>Nama Dealer</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>latitude_dealer</td>
                            <td>Latitude dari lokasi Dealer</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>longitude_dealer</td>
                            <td>Longitude dari lokasi Dealer</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "nama_dealer": "Dealer 2",
                                        "latitude_dealer": "1111",
                                        "longitude_dealer": "3333",
                                        "updated_at": "2018-02-08 20:42:33",
                                        "created_at": "2018-02-08 20:42:33",
                                        "id_dealer": 2
                                    },
                                    "message": "Dealer created successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="edit_dealer">
                    <h2>Edit Dealer</h2>
                    <p>
                        Mengubah data Dealer.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: PATCH
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/dealer/{id_dealer}
                    </span>

                    <br/><br/><br/>
                    <h4>Additional Header</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Value</th>
                        </tr>
                        <tr>
                            <td>Content-Type</td>
                            <td>application/json</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>nama_dealer</td>
                            <td>Nama Dealer</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>latitude_dealer</td>
                            <td>Latitude dari lokasi Dealer</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>longitude_dealer</td>
                            <td>Longitude dari lokasi Dealer</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "id_dealer": 2,
                                        "nama_dealer": "Main Dealer 3",
                                        "latitude_dealer": "5675675",
                                        "longitude_dealer": "3873783683",
                                        "created_at": "2018-02-08 20:42:33",
                                        "updated_at": "2018-02-08 20:44:28"
                                    },
                                    "message": "Dealer updated successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="edit_dealer">
                    <h2>Delete Dealer</h2>
                    <p>
                        Menghapus data Dealer.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: DELETE
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/dealer/{id_dealer}
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": "2",
                                    "message": "Dealer deleted successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- End Line Dealer -->

                <!-- Start Line User -->
                <br/><br/><br/>
                <div class="item" id="user_category">
                    <h2>Kategori User</h2>
                    <p>
                        List kategori user.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: GET
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/user/categories
                    </span>

                    <br/><br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": [
                                        {
                                            "id_user_category": 1,
                                            "nama_user_category": "AHM"
                                        },
                                        {
                                            "id_user_category": 2,
                                            "nama_user_category": "KHS"
                                        },
                                        {
                                            "id_user_category": 3,
                                            "nama_user_category": "Pengawas"
                                        }
                                    ],
                                    "message": "Categories retrieved successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="add_user">
                    <h2>Tambah User</h2>
                    <p>
                        Untuk menambahkan user dengan kagetori AHM, KHS dan Pengawas.<br/>
                        Khusus user Pengawas memiliki tambahan mandatory paramater.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: POST
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/user
                    </span>

                    <br/><br/><br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>id_user_category</td>
                            <td>Id User Category diambil dari method <a href="#user_category">Kategori User</a></td>
                            <td>INT</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>nama_user</td>
                            <td>Nama User yang ingin ditambahkan</td>
                            <td>TIME ( H:i )</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>username</td>
                            <td>Username yang digunakan untuk login</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>password</td>
                            <td>Password yang digunakan untuk login</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>POST Parameters ( Pengawas Only )</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>id_supervisor</td>
                            <td>Id Supervisor, diambil dari <a href="#supervisor">Data Supervisor</a></td>
                            <td>INT</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>id_kontraktor</td>
                            <td>Id Kontraktor, diambil dari <a href="#kontraktor">Data Kontraktor</a></td>
                            <td>INT</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>id_main_dealer</td>
                            <td>Id Main Dealer, diambil dari <a href="#main_dealer">Data Main Dealer</a></td>
                            <td>INT</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>id_dealer</td>
                            <td>Id Dealer, diambil dari <a href="#dealer">Data Dealer</a></td>
                            <td>INT</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "id_user_category": "1",
                                        "nama_user": "AHM 2",
                                        "username": "user2a",
                                        "password": "f37f00a70d686ad548b9e7a009af2ccd",
                                        "updated_at": "2018-02-05 10:08:44",
                                        "created_at": "2018-02-05 10:08:44",
                                        "id_user": 4
                                    },
                                    "message": "Insert user successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="edit_user">
                    <h2>Edit User</h2>
                    <p>
                        Untuk mengubah data user yang telah terdaftar.<br/>
                        Khusus user Pengawas memiliki tambahan mandatory paramater.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: PATCH
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/user/{id_user}
                    </span>

                    <br/><br/><br/>
                    <h4>Additional Header</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Value</th>
                        </tr>
                        <tr>
                            <td>Content-Type</td>
                            <td>application/json</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>POST Parameters</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>id_user_category</td>
                            <td>Id User Category diambil dari method <a href="#user_category">Kategori User</a></td>
                            <td>INT</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>nama_user</td>
                            <td>Nama User yang ingin ditambahkan</td>
                            <td>TIME ( H:i )</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>username</td>
                            <td>Username yang digunakan untuk login</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>password</td>
                            <td>Password yang digunakan untuk login</td>
                            <td>VARCHAR</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>POST Parameters ( Pengawas Only )</h4>
                    <table>
                        <tr>
                            <th>Field</th>
                            <th>Keterangan</th>
                            <th>Format</th>
                            <th>Mandatory</th>
                        </tr>
                        <tr>
                            <td>id_supervisor</td>
                            <td>Id Supervisor, diambil dari <a href="#supervisor">Data Supervisor</a></td>
                            <td>INT</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>id_kontraktor</td>
                            <td>Id Kontraktor, diambil dari <a href="#kontraktor">Data Kontraktor</a></td>
                            <td>INT</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>id_main_dealer</td>
                            <td>Id Main Dealer, diambil dari <a href="#main_dealer">Data Main Dealer</a></td>
                            <td>INT</td>
                            <td>TRUE</td>
                        </tr>
                        <tr>
                            <td>id_dealer</td>
                            <td>Id Dealer, diambil dari <a href="#dealer">Data Dealer</a></td>
                            <td>INT</td>
                            <td>TRUE</td>
                        </tr>
                    </table>

                    <br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": {
                                        "id_user": 6,
                                        "id_user_category": 3,
                                        "nama_user": "Pengawas 2",
                                        "username": "user4a",
                                        "created_at": "2018-02-05 10:32:33",
                                        "updated_at": "2018-02-05 10:32:33",
                                        "id_pengawas": 2,
                                        "id_supervisor": 1,
                                        "id_kontraktor": 1,
                                        "id_main_dealer": 1,
                                        "id_dealer": 1
                                    },
                                    "message": "Update user successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>

                <br/><br/><br/>
                <div class="item" id="delete_user">
                    <h2>Delete User</h2>
                    <p>
                        Untuk menghapus data user yang telah terdaftar.
                    </p>
                    <br/>
                    <span class="item-url">
                        Request Type: DELETE
                    </span>
                    <br/><br/><br/>
                    <span class="item-url">
                        URL: http://khsapi.permata-media.com/public/api/user/{id_user}
                    </span>

                    <br/>
                    <h4>Output Example ( JSON Format )</h4>
                    <table>
                        <tr>
                            <td>
                                {
                                    "success": true,
                                    "data": "4",
                                    "message": "User deleted successfully."
                                }
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- End Line User -->

            </div>
        </div>
    </body>
</html>
