<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\ApiModel\LoginModel;
use App\ApiModel\SupervisorModel;
use Validator;

class SupervisorController extends APIBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $supervisor = SupervisorModel::all();
        return $this->sendResponse($supervisor->toArray(), 'List supervisor retrieved successfully.');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $validator = Validator::make($input, [
            'nama_supervisor' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $supervisor = SupervisorModel::create($input);
        return $this->sendResponse($supervisor->toArray(), 'Supervisor created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $supervisor = SupervisorModel::find($id);

        if (is_null($supervisor)) {
            return $this->sendError('Supervisor not found.');
        }

        return $this->sendResponse($supervisor->toArray(), 'Supervisor retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $validator = Validator::make($input, [
            'nama_supervisor' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $supervisor = SupervisorModel::find($id);
        if (is_null($supervisor)) {
            return $this->sendError('Supervisor not found.');
        }

        $supervisor->nama_supervisor = $input['nama_supervisor'];
        $supervisor->save();

        return $this->sendResponse($supervisor->toArray(), 'Supervisor updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $supervisor = SupervisorModel::find($id);

        if (is_null($supervisor)) {
            return $this->sendError('Supervisor not found.');
        }

        $supervisor->delete();

        return $this->sendResponse($id, 'Supervisor deleted successfully.');
    }
}