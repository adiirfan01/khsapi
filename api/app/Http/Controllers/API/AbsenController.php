<?php



namespace App\Http\Controllers\API;



use Illuminate\Http\Request;

use App\Http\Controllers\API\APIBaseController as APIBaseController;

use App\ApiModel\LoginModel;

use App\ApiModel\AbsenModel;

use Validator;



class AbsenController extends APIBaseController

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        return $this->sendError('Page not found.');

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {



        $input = $request->all();



        $validator = Validator::make($request->header(), [

            'token' => 'required'

        ]);



        if($validator->fails()){

            return $this->sendError('Validation Error.', $validator->errors());       

        }



        $token = $request->header('token');

        $is_token_active = LoginModel::is_token_active($token);



        if($is_token_active == false){

            return $this->sendError('Token not found or inactive.');

        }



        $id_pengawas = LoginModel::get_id_pengawas($token);



        if($id_pengawas == 0){

            return $this->sendError('User have no access.');

        }



        $currentTime = $this->currentTime();

        //$currentTime = $input['current_time'];



        if($currentTime >= date('H:i', strtotime("08:00")) && $currentTime <= date('H:i', strtotime("09:00"))){

            $validator = Validator::make($input, [

                'tgl_absen' => 'date_format:"Y-m-d"|required',

                //'start_absen' => 'date_format:"H:i"|required',

                'start_latitude' => 'required',

                'start_longitude' => 'required'

            ]);



            if($validator->fails()){

                return $this->sendError('Validation Error.', $validator->errors());       

            }



            $input['start_absen'] = $currentTime;

            $current_absence = AbsenModel::get_current_absence($input['tgl_absen']);



            if($current_absence != 0){

                return $this->sendError('You already absence.');

            }



            $dealer_location = AbsenModel::get_dealer_location($token);

            $distance = $this->getDistanceBetween($input['start_latitude'], $input['start_longitude'], $dealer_location->latitude_dealer, $dealer_location->longitude_dealer, 'm');



            if($distance > 20){

                return $this->sendError('Distance must be under 20 meter.');

            }



            $input['id_pengawas'] = $id_pengawas;

            $insert = AbsenModel::create($input);



            return $this->sendResponse($insert->toArray(), 'Insert absence successfully.');



        } else if($currentTime >= date('H:i', strtotime("17:00")) && $currentTime <= date('H:i', strtotime("23:59"))){

            $validator = Validator::make($input, [

                'tgl_absen' => 'date_format:"Y-m-d"|required',

                //'end_absen' => 'date_format:"H:i"|required',

                'end_latitude' => 'required',

                'end_longitude' => 'required'

            ]);



            if($validator->fails()){

                return $this->sendError('Validation Error.', $validator->errors());       

            }


            $input['start_absen'] = $currentTime;
            $current_absence = AbsenModel::get_current_absence($input['tgl_absen']);



            



            $dealer_location = AbsenModel::get_dealer_location($token);

            $distance = $this->getDistanceBetween($input['end_latitude'], $input['end_longitude'], $dealer_location->latitude_dealer, $dealer_location->longitude_dealer, 'm');



            if($distance > 20){

                return $this->sendError('Distance must be under 20 meter.');

            }

            if($current_absence == 0){

               $input['id_pengawas'] = $id_pengawas;

                $insert = AbsenModel::create($input);
                return $this->sendResponse($insert->toArray(), 'Insert absence successfully.');

            }
            else
            {
                $update = AbsenModel::find($current_absence);

            if (is_null($update)) {

                return $this->sendError('Data not found.');

            }



            $update->end_absen = $currentTime;

            $update->end_latitude = $input['end_latitude'];

            $update->end_longitude = $input['end_longitude'];

            $update->save();



            return $this->sendResponse($update->toArray(), 'Absence updated successfully.');
            }
            



        } else{

            $validator = Validator::make($input, [

                'tgl_absen' => 'date_format:"Y-m-d"|required',

                'note' => 'required'

            ]);



            if($validator->fails()){

                return $this->sendError('Validation Error.', $validator->errors());       

            }



            $input['id_pengawas'] = $id_pengawas;



            $current_absence = AbsenModel::get_current_absence($input['tgl_absen']);



            if($current_absence != 0){

                $validator = Validator::make($input, [

                    'end_latitude' => 'required',

                    'end_longitude' => 'required'

                ]);



                if($validator->fails()){

                    return $this->sendError('Validation Error.', $validator->errors());       

                }



                $update = AbsenModel::find($current_absence);

                if (is_null($update)) {

                    return $this->sendError('Data not found.');

                }



                $update->end_absen = $currentTime;

                $update->end_latitude = $input['end_latitude'];

                $update->end_longitude = $input['end_longitude'];

                $update->save();



                return $this->sendResponse($update->toArray(), 'Absence updated successfully.');

            } else{

                $validator = Validator::make($input, [

                    'start_latitude' => 'required',

                    'start_longitude' => 'required'

                ]);



                if($validator->fails()){

                    return $this->sendError('Validation Error.', $validator->errors());       

                }



                $input['start_absen'] = $currentTime;

                $insert = AbsenModel::create($input);



                return $this->sendResponse($insert->toArray(), 'Insert absence successfully.');

            }

        }



        //$check_user_login = LoginModel::check_user_login($input['username'], $input['password']);

        //return $this->sendResponse($check_user_login, 'Login retrieved successfully.');

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        /*$post = Login::find($id);



        if (is_null($post)) {

            return $this->sendError('Post not found.');

        }



        return $this->sendResponse($post->toArray(), 'Login retrieved successfully.');*/



        return $this->sendError('Page not found.');

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        return $this->sendError('Page not found.');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        return $this->sendError('Page not found.');

    }



    function getDistanceBetween($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Mi') 

    { 

        $theta = $longitude1 - $longitude2; 

        $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2)))  + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta))); 

        $distance = acos($distance); 

        $distance = rad2deg($distance); 

        $distance = $distance * 60 * 1.1515; 

        switch($unit) 

        { 

            case 'Mi': break; 

            case 'Km' : $distance = $distance * 1.609344; 

            case 'm' : $distance = $distance * 1.609344 * 1000; 

        } 

        return (round($distance,2)); 

    }



    public function absenlist(Request $request)

    {

        $validator = Validator::make($request->header(), [

            'token' => 'required'

        ]);



        if($validator->fails()){

            return $this->sendError('Validation Error.', $validator->errors());       

        }



        $token = $request->header('token');

        $is_token_active = LoginModel::is_token_active($token);



        if($is_token_active == 0){

            return $this->sendError('Token not found or inactive.');

        }



        $input = $request->all();



        $id_pengawas = (isset($input['id_pengawas']) ? $input['id_pengawas'] : NULL);

        $id_dealer = (isset($input['id_dealer']) ? $input['id_dealer'] : NULL);

        $tgl_absen = (isset($input['tgl_absen']) ? $input['tgl_absen'] : NULL);



        $absences = AbsenModel::get_absence_list($id_pengawas, $id_dealer, $tgl_absen);

        //print_r($absences);die;

        return $this->sendResponse($absences->toArray(), 'Absences retrieved successfully.');

    }



    /*publc function absenNote(Request $request)

    {

        $input = $request->all();



        $validator = Validator::make($request->header(), [

            'token' => 'required'

        ]);



        if($validator->fails()){

            return $this->sendError('Validation Error.', $validator->errors());       

        }



        $token = $request->header('token');

        $is_token_active = LoginModel::is_token_active($token);



        if($is_token_active == false){

            return $this->sendError('Token not found or inactive.');

        }



        $id_pengawas = LoginModel::get_id_pengawas($token);



        if($id_pengawas == 0){

            return $this->sendError('User have no access.');

        }



        $currentTime = $this->currentTime();

        if(($currentTime < date('H:i', strtotime("08:00")) && $currentTime > date('H:i', strtotime("09:00"))) || $currentTime < date('H:i', strtotime("17:00")) && $currentTime > date('H:i', strtotime("23:59"))){



        } else{

            return $this->sendError('User have no access.');

        }

    }*/

}