<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\ApiModel\LoginModel;
use App\ApiModel\DailyMomModel;
use Validator;

class DailyMomController extends APIBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        /*$id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }*/

        $dailymom = DailyMomModel::all();
        return $this->sendResponse($dailymom->toArray(), 'List daily MOM retrieved successfully.');
        
    }

    public function simplelist(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_pengawas = NULL;
        $id_dealer = NULL;
        $date_mom = NULL;
        if(isset($input['id_pengawas'])){
            $id_pengawas = $input['id_pengawas'];
        }
        if(isset($input['id_dealer'])){
            $id_dealer = $input['id_dealer'];
        }
        if(isset($input['date_mom'])){
            $date_mom = $input['date_mom'];
        }
        $id_user_category = LoginModel::get_user_category($token);
        $is_approved = NULL;
        if($id_user_category == 1){
            $is_approved = 1;
        }
        
        $dailymom = DailyMomModel::simple_daily_mom_list($id_pengawas, $id_dealer, $date_mom, $is_approved);

        //$result = $dailymom->toArray();
        
        /*foreach ($result as $key => $value) {
            $result[$key]['daily_report_progress'] = dailymomProgressModel::daily_report_progress($value['id_daily_report']);
        }*/

        return $this->sendResponse($dailymom, 'List daily MOM retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 3){
            return $this->sendError('You have no access for this page.');
        }

        $validator = Validator::make($input, [
            'date_mom' => 'date_format:"Y-m-d"|required',
            'common_condition' => 'required',
            'problem' => 'required',
            'attendance' => 'required',
            'report' => 'required',
            'to_do_list' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $id_pengawas = LoginModel::get_id_pengawas($token);
        $input['id_pengawas'] = $id_pengawas;

        $dailymom = DailyMomModel::create($input);
        return $this->sendResponse($dailymom->toArray(), 'Daily MOM created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $dailymom = DailyMomModel::find($id);

        if (is_null($dailymom)) {
            return $this->sendError('Daily MOM not found.');
        }

        return $this->sendResponse($dailymom->toArray(), 'Daily MOM retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $validator = Validator::make($input, [
            'date_mom' => 'date_format:"Y-m-d"|required',
            'common_condition' => 'required',
            'problem' => 'required',
            'attendance' => 'required',
            'report' => 'required',
            'to_do_list' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $dailymom = DailyMomModel::find($id);
        if (is_null($dailymom)) {
            return $this->sendError('Daily MOM not found.');
        }

        $dailymom->date_mom = $input['date_mom'];
        $dailymom->common_condition = $input['common_condition'];
        $dailymom->problem = $input['problem'];
        $dailymom->attendance = $input['attendance'];
        $dailymom->report = $input['report'];
        $dailymom->to_do_list = $input['to_do_list'];
        $dailymom->save();

        return $this->sendResponse($dailymom->toArray(), 'Daily MOM updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $dailymom = DailyMomModel::find($id);

        if (is_null($dailymom)) {
            return $this->sendError('Daily MOM not found.');
        }

        $dailymom->delete();

        return $this->sendResponse($id, 'Daily MOM deleted successfully.');
    }

    public function approval(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $dailymom = DailyMomModel::find($id);
        if (is_null($dailymom)) {
            return $this->sendError('Dealer not found.');
        }

        if(isset($input['approved'])){
            $dailymom->approved = $input['approved'];
        }
        if(isset($input['approval_note'])){
            $dailymom->approval_note = $input['approval_note'];
        }
        $dailymom->approved_by = LoginModel::get_id_user($token);
        $dailymom->save();

        $result = $dailymom->toArray();
        unset($result['common_condition'], $result['problem'], $result['attendance'], $result['report'], $result['to_do_list'], $result['created_at'], $result['updated_at']);

        return $this->sendResponse($result, 'Daily MOM Approval successfully.');
    }

}