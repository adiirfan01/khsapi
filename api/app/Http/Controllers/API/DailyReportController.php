<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\ApiModel\LoginModel;
use App\ApiModel\DailyReportModel;
use App\ApiModel\DailyReportProgressModel;
use App\ApiModel\PengawasModel;
use Validator;
use App\Http\Requests;
use App\Image;
use Illuminate\Http\File;
use Spipu\Html2Pdf\Html2Pdf;

class DailyReportController extends APIBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_pengawas = NULL;
        $id_dealer = NULL;
        $date_report = NULL;
        if(isset($input['id_pengawas'])){
            $id_pengawas = $input['id_pengawas'];
        }
        if(isset($input['id_dealer'])){
            $id_dealer = $input['id_dealer'];
        }
        if(isset($input['date_report'])){
            $date_report = $input['date_report'];
        }
        
        $dailyreport = DailyReportModel::simple_daily_report_list($id_pengawas, $id_dealer, $date_report);

        //$result = $dailyreport->toArray();
        
        /*foreach ($result as $key => $value) {
            $result[$key]['daily_report_progress'] = DailyReportProgressModel::daily_report_progress($value['id_daily_report']);
        }*/

        return $this->sendResponse($dailyreport, 'List daily report retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 3){
            return $this->sendError('You have no access for this page.');
        }

        $validator = Validator::make($input, [
            'date_report' => 'date_format:"Y-m-d"|required',
            'start_time' => 'date_format:"H:i"|required',
            'end_time' => 'date_format:"H:i"|required',
            'exterior_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'interior_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'progress_work' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        if(!is_numeric($input['progress_work'])){
            return $this->sendError('Progress Work field must be total of progress work images.');
        }

        if($input['progress_work'] != 0){
            for($i=1; $i<=$input['progress_work']; $i++){
                $validator = Validator::make($input, [
                    'progress_img_'.$i => 'required'
                ]);

                if($validator->fails()){
                    return $this->sendError('Validation Error.', $validator->errors());  
                }
            }
        }

        $id_pengawas = LoginModel::get_id_pengawas($token);
        $input['id_pengawas'] = $id_pengawas;

        $exterior_extension = $request->file('exterior_img')->guessExtension();
        $exterior_name = date('Ymd').'-exterior-'.time().'.'.$exterior_extension;
        $exterior_destination = base_path() . '/public/uploads/dailyreport';
        $request->file('exterior_img')->move($exterior_destination, $exterior_name);

        $interior_extension = $request->file('interior_img')->guessExtension();
        $interior_name = date('Ymd').'-interior-'.time().'.'.$interior_extension;
        $interior_destination = base_path() . '/public/uploads/dailyreport';
        $request->file('interior_img')->move($interior_destination, $interior_name);

        $input['exterior_img'] = 'uploads/dailyreport/'.$exterior_name;
        $input['interior_img'] = 'uploads/dailyreport/'.$interior_name;
        $insert = DailyReportModel::create($input);

        if($input['progress_work'] != 0){
            $input_progress['id_daily_report'] = $insert->id_daily_report;
            for($i=1; $i<=$input['progress_work']; $i++){
                if($request->file('progress_img_'.$i)){
                    $progress_extension = $request->file('progress_img_'.$i)->guessExtension();
                    $progress_name = date('Ymd').'-'.$i.'-'.time().'.'.$progress_extension;
                    $progress_destination = base_path() . '/public/uploads/dailyreport_progress';
                    $request->file('progress_img_'.$i)->move($progress_destination, $progress_name);

                    $input_progress['progress_img'] = 'uploads/dailyreport_progress/'.$progress_name;
                    if(isset($input['progress_text_'.$i])){
                        $input_progress['progress_text'] = $input['progress_text_'.$i];
                    } else{
                        $input_progress['progress_text'] = NULL;
                    }
                    $insert_progress = DailyReportProgressModel::create($input_progress);
                } else{
                    return $this->sendError('Upload image first.');
                }
            }
        }

        return $this->sendResponse($insert->toArray(), 'Insert daily report successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {

        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 3){
            return $this->sendError('You have no access for this page.');
        }

        $dailyreport = DailyReportModel::find($id);

        if (is_null($dailyreport)) {
            return $this->sendError('Daily Report not found.');
        }

        $result = $dailyreport->toArray();

        $result['daily_report_progress'] = DailyReportProgressModel::daily_report_progress($dailyreport['id_daily_report']);

        return $this->sendResponse($result, 'Login retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 3){
            return $this->sendError('You have no access for this page.');
        }

        $dailyreport = DailyReportModel::find($id);
        if (is_null($dailyreport)) {
            return $this->sendError('Daily Report not found.');
        }

        $id_pengawas = LoginModel::get_id_pengawas($token);
        $input['id_pengawas'] = $id_pengawas;

        if(isset($input['exterior_img'])){
            $exterior_extension = $request->file('exterior_img')->guessExtension();
            $exterior_name = date('Ymd').'-exterior-'.time().'.'.$exterior_extension;
            $exterior_destination = base_path() . '/public/uploads/dailyreport';
            $request->file('exterior_img')->move($exterior_destination, $exterior_name);

            $input['exterior_img'] = 'uploads/dailyreport/'.$exterior_name;
        }

        if(isset($input['interior_img'])){
            $interior_extension = $request->file('interior_img')->guessExtension();
            $interior_name = date('Ymd').'-interior-'.time().'.'.$interior_extension;
            $interior_destination = base_path() . '/public/uploads/dailyreport';
            $request->file('interior_img')->move($interior_destination, $interior_name);

            $input['interior_img'] = 'uploads/dailyreport/'.$interior_name;
        }

        $input_keys = array_keys($input);
 
        for($i=0; $i<sizeof($input_keys); $i++){
            $dailyreport->$input_keys[$i] = $input[$input_keys[$i]];
        }
        
        $dailyreport->save();

        if(isset($input['progress_work'])){
            $input_progress['id_daily_report'] = $insert->id_daily_report;
            for($i=1; $i<=$input['progress_work']; $i++){
                if($request->file('progress_img_'.$i)){
                    $progress_extension = $request->file('progress_img_'.$i)->guessExtension();
                    $progress_name = date('Ymd').'-'.$i.'-'.time().'.'.$progress_extension;
                    $progress_destination = base_path() . '/public/uploads/dailyreport_progress';
                    $request->file('progress_img_'.$i)->move($progress_destination, $progress_name);

                    $input_progress['progress_img'] = 'uploads/dailyreport_progress/'.$progress_name;
                    if(isset($input['progress_text_'.$i])){
                        $input_progress['progress_text'] = $input['progress_text_'.$i];
                    } else{
                        $input_progress['progress_text'] = NULL;
                    }
                    $insert_progress = DailyReportProgressModel::create($input_progress);
                } else{
                    return $this->sendError('Upload image first.');
                }
            }
        }

        return $this->sendResponse($result, 'Update user successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post_user = UserModel::find($id);

        if (is_null($post_user)) {
            return $this->sendError('User not found.');
        }

        $post_user->delete();

        $id_pengawas = UserModel::get_pengawas_id($id);
        if($id_pengawas != 0){
            $post_pengawas = PengawasModel::find($id_pengawas);

            if (is_null($post_pengawas)) {
                return $this->sendError('Pengawas not found.');
            }

            $post_pengawas->delete();
        }

        return $this->sendResponse($id, 'User deleted successfully.');
    }

    public function categories(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $categories = UserModel::get_all_category();
        return $this->sendResponse($categories->toArray(), 'Categories retrieved successfully.');
    }

    public function supervisor(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $supervisor = UserModel::get_all_supervisor();
        return $this->sendResponse($supervisor->toArray(), 'Supervisor retrieved successfully.');
    }

    public function kontraktor(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $kontraktor = UserModel::get_all_kontraktor();
        return $this->sendResponse($kontraktor->toArray(), 'Kontraktor retrieved successfully.');
    }

    public function maindealer(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $main_dealer = UserModel::get_all_main_dealer();
        return $this->sendResponse($main_dealer->toArray(), 'Main Dealer retrieved successfully.');
    }

    public function dealer(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $dealer = UserModel::get_all_dealer();
        return $this->sendResponse($dealer->toArray(), 'Dealer retrieved successfully.');
    }

    public function approval(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $dailyreport = DailyReportModel::find($id);
        if (is_null($dailyreport)) {
            return $this->sendError('Dealer not found.');
        }

        if(isset($input['approved'])){
            $dailyreport->approved = $input['approved'];
        }
        if(isset($input['approval_note'])){
            $dailyreport->approval_note = $input['approval_note'];
        }
        $dailyreport->approved_by = LoginModel::get_id_user($token);
        $dailyreport->save();

        $result = $dailyreport->toArray();
        unset($result['date_report'], $result['start_time'], $result['end_time'], $result['exterior_img'], $result['exterior_text'], $result['interior_img'], $result['interior_text'], $result['common_condition'], $result['problem'], $result['attendance'], $result['report'], $result['created_at'], $result['updated_at']);

        return $this->sendResponse($result, 'Daily Report Approval successfully.');
    }

    public function simplelist(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_pengawas = NULL;
        $id_dealer = NULL;
        $date_report = NULL;
        if(isset($input['id_pengawas'])){
            $id_pengawas = $input['id_pengawas'];
        }
        if(isset($input['id_dealer'])){
            $id_dealer = $input['id_dealer'];
        }
        if(isset($input['date_report'])){
            $date_report = $input['date_report'];
        }
        $id_user_category = LoginModel::get_user_category($token);
        $is_approved = NULL;
        if($id_user_category == 1){
            $is_approved = 1;
        }
        
        $dailyreport = DailyReportModel::simple_daily_report_list($id_pengawas, $id_dealer, $date_report, $is_approved);

        //$result = $dailyreport->toArray();
        
        /*foreach ($result as $key => $value) {
            $result[$key]['daily_report_progress'] = DailyReportProgressModel::daily_report_progress($value['id_daily_report']);
        }*/

        return $this->sendResponse($dailyreport, 'List daily report retrieved successfully.');
    }

    public function topdf(Request $request, $id)
    {        
        /*$i = 0;
        foreach ($dailyreport->toArray() as $key => $value) {
            //$result[$key]['daily_report_progress'] = DailyReportProgressModel::daily_report_progress($value['id_daily_report']);

            if($value['exterior_img'] != NULL){
                $result[$i] = array(
                            'title'     => "FOTO EKSTERIOR",
                            'image'     => $value['exterior_img'],
                            'caption'   => ($value['exterior_text'] != NULL ? $value['exterior_text'] : "")
                        );
            }
            if($value['interior_img'] != NULL){
                $result[$i] = array(
                            'title'     => "FOTO EKSTERIOR",
                            'image'     => $value['interior_img'],
                            'caption'   => ($value['interior_text'] != NULL ? $value['interior_text'] : "")
                        );
            }

            $i++;
        }*/

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $dailyreport = DailyReportModel::find($id);
        if (is_null($dailyreport)) {
            return $this->sendError('Daily Report not found.');
        }

        $data = $dailyreport->toArray();

        //$progress_work = DailyReportProgressModel::daily_report_progress($id)->toArray();
        $count_progress_work = DailyReportProgressModel::count_daily_report_progress($id);
        $count_progress_work = $count_progress_work-2;
        $limit = 4;
        $all = ($count_progress_work % $limit == 0 ? $count_progress_work / $limit : ($count_progress_work / $limit)+1 );
        $progress_work = array();
        $progress_work[0] = DailyReportProgressModel::limit_daily_report_progress($id, 0, 2)->toArray();
        for($pw=0; $pw<(int)$all; $pw++){
            $progress_work[$pw+1] = DailyReportProgressModel::limit_daily_report_progress($id, $pw*$limit+2, $limit)->toArray();
        }

        $data['progress_work'] = $progress_work;

        $pengawas = PengawasModel::get_detail_pengawas($data['id_pengawas']);
        $data['pengawas'] = ($pengawas != NULL ? $pengawas : NULL);

        $data['convert_date'] = $this->convertDate($dailyreport['date_report']);

        $content = view('report_pdf/daily', $data)->render();
        //require_once($html2pdf_path);
        
        $html2pdf = new Html2Pdf('P', 'Legal', 'en', true, 'UTF-8', array(25.4, 20.4, 25.4, 20.4));
            $html2pdf->pdf->SetTitle('Laporan Harian - '.$this->convertDate($dailyreport['date_report']));
            $html2pdf->WriteHTML($content);
            $html2pdf->Output(base_path() . '/public/pdf/dailyreport/laporan_harian_'.date('Y_m_d', strtotime($dailyreport['date_report'])).'.pdf', 'F');
            /*$html2pdf->Output(base_path() . '/public/pdf/dailyreport/laporan_harian_'.date('Y_m_d', strtotime($dailyreport['date_report'])).'.pdf');*/

        $result = array(
                'pdf_url'   => url('/') . '/pdf/dailyreport/laporan_harian_'.date('Y_m_d', strtotime($dailyreport['date_report'])).'.pdf'
            );
        return $this->sendResponse($result, 'Create report pdf successfully.');

    }
}