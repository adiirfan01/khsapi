<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\ApiModel\LoginModel;
use App\ApiModel\WeeklyItemModel;
use App\ApiModel\WeeklyReportModel;
use Validator;

class WeeklyReportController extends APIBaseController
{

    public function listitemdealer(Request $request, $id_dealer)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $item_list = WeeklyReportModel::list_item_dealer($id_dealer, 0);
        foreach ($item_list as $key => $value) {
            $item_list[$key]->child = WeeklyReportModel::list_item_dealer($id_dealer, $value->id_weekly_item);

            foreach ($item_list[$key]->child as $child_key => $child_value) {
                $item_list[$key]->child[$child_key]->child = WeeklyReportModel::list_item_dealer($id_dealer, $child_value->id_weekly_item);
            }
        }
        return $this->sendResponse($item_list, 'List Dealer Item retrieved successfully.');
    }

    public function addreport(Request $request, $id_pengawas)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $validator = Validator::make($input, [
            'date_report' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $add_report = WeeklyReportModel::add_report($input, $id_pengawas);
        return $this->sendResponse($add_report, 'Report Item created successfully.');
    }

    public function addreportdetail(Request $request, $id_weekly_report)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        /*$validator = Validator::make($input, [
            'nama_item' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }*/

        $add_report_detail = WeeklyReportModel::add_report_detail($input, $id_weekly_report);
        return $this->sendResponse($add_report_detail, 'Parent Item created successfully.');
    }

    public function approval(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $weeklyreport = WeeklyReportModel::find($id);
        if (is_null($weeklyreport)) {
            return $this->sendError('Dealer not found.');
        }

        if(isset($input['approved'])){
            $weeklyreport->approved = $input['approved'];
        }
        if(isset($input['approval_note'])){
            $weeklyreport->approval_note = $input['approval_note'];
        }
        $weeklyreport->approved_by = LoginModel::get_id_user($token);
        $weeklyreport->save();

        $result = $weeklyreport->toArray();
        unset($result['date_report'], $result['created_at'], $result['updated_at']);

        return $this->sendResponse($result, 'Weekly Report Approval successfully.');
    }

}