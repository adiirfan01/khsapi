<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DailyMomModel extends Model
{

    protected $table = "daily_mom";
    protected $primaryKey = "id_daily_mom";
	protected $fillable = [
        'id_pengawas', 'date_mom', 'common_condition', 'problem', 'attendance', 'report', 'to_do_list', 'approved', 'approved_by', 'approval_note'
    ];

    public static function simple_daily_mom_list($id_pengawas=NULL, $id_dealer=NULL, $date_mom=NULL, $is_approved=NULL){

        $query = DB::table('daily_mom')
        	->select('id_daily_mom', 'daily_mom.id_pengawas', 'user.nama_user AS nama_pengawas', 'date_mom', 'nama_dealer', 'approved', DB::Raw('DATE_FORMAT(date_mom, "%d %M %Y") AS date_format'))
        	->join('pengawas', 'daily_mom.id_pengawas', '=', 'pengawas.id_pengawas')
        	->join('user', 'pengawas.id_user', '=', 'user.id_user')
        	->join('dealer', 'pengawas.id_dealer', '=', 'dealer.id_dealer');

        if($id_pengawas != NULL && $id_pengawas != "-" && $id_pengawas != ""){
            $query->where('daily_mom.id_pengawas', $id_pengawas);
        }

        if($id_dealer != NULL && $id_dealer != "-" && $id_dealer != ""){
            $query->where('pengawas.id_dealer', $id_dealer);
        }

        if($date_mom != NULL && $date_mom != "-" && $date_mom != ""){
            $query->where('daily_mom.date_mom', $date_mom);
        }

        if($is_approved != NULL && $is_approved != "-" && $is_approved != ""){
            $query->where('daily_mom.approved', $is_approved);
        }

        $dailymom = $query
        			->orderBy('date_mom', 'DESC')
        			->get();

        if($dailymom != NULL){
            return $dailymom;
        } else{
            return 0;
        }

    }
    
}