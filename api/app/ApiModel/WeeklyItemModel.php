<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class WeeklyItemModel extends Model
{

	public static function list_parent(){
    	$data = DB::table('weekly_item')
            ->where('id_parent', NULL)
    		->where('is_deleted', 0)
    		->get();

    	return $data;
    }

    public static function find_parent($id){
    	$data = DB::table('weekly_item')->where('id_weekly_item', $id)->get();

    	return $data;
    }

    public static function add_parent($data){
    	$insert = DB::table('weekly_item')->insertGetId($data);

    	$data = DB::table('weekly_item')->where('id_weekly_item', $insert)->get();

    	return $data;
    }

    public static function edit_parent($id, $data){
    	$update = DB::table('weekly_item')->where('id_weekly_item', $id)->update($data);

    	if($update != 0){
    		$update = DB::table('weekly_item')->where('id_weekly_item', $id)->get();
    	}

    	return $update;
    }

    public static function add_parent_dealer($data){
    	$insert = DB::table('weekly_item_dealer')->insertGetId($data);

    	return $insert;
    }

	public static function list_parent_dealer($id_weekly_item){
    	$data = DB::table('weekly_item_dealer')
    		->select('id_weekly_item_dealer', 'id_weekly_item', 'weekly_item_dealer.id_dealer', 'nama_dealer')
    		->join('dealer', 'weekly_item_dealer.id_dealer', '=', 'dealer.id_dealer')
    		->where('id_weekly_item', $id_weekly_item)
    		->where('is_deleted', 0)
    		->get();

    	return $data;
    }

    public static function find_parent_dealer($id){
    	$data = DB::table('weekly_item_dealer')->where('id_weekly_item_dealer', $id)->get();

    	return $data;
    }

    public static function edit_parent_dealer($id, $data){
    	$update = DB::table('weekly_item_dealer')->where('id_weekly_item_dealer', $id)->update($data);

    	if($update != 0){
    		$update = DB::table('weekly_item_dealer')->where('id_weekly_item_dealer', $id)->get();
    	}

    	return $update;
    }


    // start for child item

    public static function list_child($id_parent){
        $data = DB::table('weekly_item')
            ->where('id_parent', $id_parent)
            ->where('is_deleted', 0)
            ->get();

        return $data;
    }

    public static function add_child($data, $id_parent){
        $data['id_parent'] = $id_parent;
        $insert = DB::table('weekly_item')->insertGetId($data);

        $data = DB::table('weekly_item')->where('id_weekly_item', $insert)->get();

        return $data;
    }

    // end for child item
}