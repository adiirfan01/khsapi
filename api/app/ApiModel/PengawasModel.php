<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class PengawasModel extends Model
{

    protected $table = "pengawas";
    protected $primaryKey = "id_pengawas";
	protected $fillable = [
        'id_user', 'id_supervisor', 'id_kontraktor', 'id_main_dealer', 'id_dealer' 
    ];
    
    public static function get_detail_pengawas($id)
    {
    	$detail_pengawas = DB::table('pengawas')
                    ->join('user', 'pengawas.id_user', '=', 'user.id_user')
                    ->join('supervisor', 'pengawas.id_supervisor', '=', 'supervisor.id_supervisor')
                    ->join('kontraktor', 'pengawas.id_kontraktor', '=', 'kontraktor.id_kontraktor')
                    ->join('main_dealer', 'pengawas.id_main_dealer', '=', 'main_dealer.id_main_dealer')
                    ->join('dealer', 'pengawas.id_dealer', '=', 'dealer.id_dealer')
                    ->select('user.nama_user AS nama_pengawas', 'nama_supervisor', 'nama_kontraktor', 'nama_main_dealer', 'nama_dealer')
                    ->where('id_pengawas', $id)
                    ->first();

        if($detail_pengawas != NULL){
            return $detail_pengawas;    
        } else{
            return NULL;
        }
    }

}