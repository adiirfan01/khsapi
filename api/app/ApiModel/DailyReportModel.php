<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DailyReportModel extends Model
{

    protected $table = "daily_report";
    protected $primaryKey = "id_daily_report";
	protected $fillable = [
        'id_pengawas', 'date_report', 'start_time', 'end_time', 'exterior_img', 'exterior_text', 'interior_img', 'interior_text'  
    ];

    public static function simple_daily_report_list($id_pengawas=NULL, $id_dealer=NULL, $date_report=NULL, $is_approved=NULL){

        $query = DB::table('daily_report')
        	->select('id_daily_report AS id_report', 'daily_report.id_pengawas', 'user.nama_user AS nama_pengawas', 'date_report', 'nama_dealer', 'approved', DB::Raw('DATE_FORMAT(date_report, "%d %M %Y") AS date_format'))
        	->join('pengawas', 'daily_report.id_pengawas', '=', 'pengawas.id_pengawas')
        	->join('user', 'pengawas.id_user', '=', 'user.id_user')
        	->join('dealer', 'pengawas.id_dealer', '=', 'dealer.id_dealer');

        if($id_pengawas != NULL && $id_pengawas != "-"){
            $query->where('daily_report.id_pengawas', $id_pengawas);
        }

        if($id_dealer != NULL && $id_dealer != "-"){
            $query->where('pengawas.id_dealer', $id_dealer);
        }

        if($date_report != NULL && $id_pengawas != "-"){
            $query->where('daily_report.date_report', $date_report);
        }

        if($is_approved != NULL && $is_approved != "-" && $is_approved != ""){
            $query->where('daily_report.approved', $is_approved);
        }

        $dailyreport = $query
        			->orderBy('date_report', 'DESC')
        			->orderBy('start_time', 'DESC')
        			->get();

        if($dailyreport != NULL){
            return $dailyreport;
        } else{
            return 0;
        }

    }
    
}