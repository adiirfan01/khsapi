<?php



namespace App\ApiModel;



use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;



class AbsenModel extends Model

{



    protected $table = "absen";

    protected $primaryKey = "id_absen";

	protected $fillable = [

        'id_pengawas', 'tgl_absen', 'start_absen', 'start_latitude', 'start_longitude', 'end_absen', 'end_latitude', 'end_longitude', 'note' 

    ];



    public static function get_absence_list($id_pengawas=NULL, $id_dealer=NULL, $tgl_absen=NULL)

    {

        if($tgl_absen == NULL && $tgl_absen != ""){

            $tgl_absen = date('Y-m-d');

        }



        $query = DB::table('absen')

            ->select('absen.*', DB::Raw('TIME_FORMAT(start_absen, "%H:%i") AS start_absen'), DB::Raw('TIME_FORMAT(end_absen, "%H:%i") AS end_absen'), 'nama_user AS nama_pengawas', 'nama_dealer')

            ->join('pengawas', 'absen.id_pengawas', '=', 'pengawas.id_pengawas')

            ->join('user', 'pengawas.id_user', '=', 'user.id_user')

            ->join('dealer', 'pengawas.id_dealer', '=', 'dealer.id_dealer')

            ->where('absen.tgl_absen', $tgl_absen);



        if($id_pengawas != NULL && $id_pengawas != ""){

            $query->where('absen.id_pengawas', $id_pengawas);

        }



        if($id_dealer != NULL && $id_dealer != ""){

            $query->where('pengawas.id_dealer', $id_dealer);

        }



        $absences = $query->get();



        if($absences != NULL){

            return $absences;

        } else{

            return 0;

        }

    }



    public static function get_dealer_location($token)

    {

        $dealer_location = DB::table('user_token')

                    ->join('pengawas', 'user_token.id_user', '=', 'pengawas.id_user')

                    ->join('dealer', 'pengawas.id_dealer', '=', 'dealer.id_dealer')

                    ->select('dealer.latitude_dealer', 'dealer.longitude_dealer')

                    ->where('data_token', $token)

                    ->first();



        if($dealer_location != NULL){

            return $dealer_location;    

        } else{

            return 0;

        }

    }



    public static function get_current_absence($date)

    {

        $current_absence = DB::table('absen')

                    ->select('id_absen')

                    ->where('tgl_absen', $date)

                    ->first();



        if($current_absence != NULL){

            return $current_absence->id_absen;    

        } else{

            return 0;

        }

    }
    public static function get_current_absence_id($date,$id_absen)

    {

        $current_absence = DB::table('absen')

                    ->select('id_absen')

                    ->where('tgl_absen', $date)
                    ->where('id_absen',$id_absen)

                    ->first();



        if($current_absence != NULL){

            return true;

        } else{

            return false;

        }

    }

}