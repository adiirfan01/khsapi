<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class PagesModel extends Model
{

    public static function historylist($id_pengawas=NULL, $date_report=NULL){

        $query = DB::table('daily_report')
        	->select('id_daily_report AS id_report', DB::raw('1 AS report_type'), 'daily_report.id_pengawas', 'date_report', 'nama_dealer', DB::Raw('DATE_FORMAT(start_time,"%H:%i") AS start_time'), DB::Raw('DATE_FORMAT(end_time,"%H:%i") AS end_time'))
        	->join('pengawas',  'daily_report.id_pengawas', '=', 'pengawas.id_pengawas')
        	->join('dealer', 'pengawas.id_dealer', '=', 'dealer.id_dealer');

        $query2 = DB::table('monthly_report')
        	->select('id_monthly_report AS id_report', DB::raw('3 AS report_type'), 'monthly_report.id_pengawas', 'date_report', 'nama_dealer', DB::Raw('DATE_FORMAT(date_report,"%Y-%m-01") AS start_time'), DB::Raw('LAST_DAY(date_report) AS end_time'))
        	->join('pengawas', 'monthly_report.id_pengawas', '=', 'pengawas.id_pengawas')
        	->join('dealer', 'pengawas.id_dealer', '=', 'dealer.id_dealer');

        if($id_pengawas != NULL && $id_pengawas != "-"){
            $query->where('daily_report.id_pengawas', $id_pengawas);
            $query2->where('monthly_report.id_pengawas', $id_pengawas);
        }

        if($date_report != NULL && $id_pengawas != "-"){
            $query->where('daily_report.date_report', $date_report);
            $query2->where('monthly_report.date_report', $date_report);
        }

        $query2->union($query);

        $dailyreport = $query2->get();

        if($dailyreport != NULL){
            return $dailyreport;
        } else{
            return 0;
        }

    }
    
}