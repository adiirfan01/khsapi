<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DealerModel extends Model
{

    protected $table = "dealer";
    protected $primaryKey = "id_dealer";
	protected $fillable = [
        'nama_dealer', 'latitude_dealer', 'longitude_dealer'
    ];

    public static function other_weekly_dealer($id_weekly_item){

        $weekly_item = DB::table('weekly_item_dealer')
                    ->select('id_dealer')
                    ->where('id_weekly_item', $id_weekly_item)
                    ->where('is_deleted', 0);

        $dealer = DB::table("dealer")->select('*')
            ->whereNOTIn('id_dealer', $weekly_item)
            ->get();

        return $dealer;

    }
    
}