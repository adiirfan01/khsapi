<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class WeeklyReportModel extends Model
{

    protected $table = "weekly_report";
    protected $primaryKey = "id_weekly_report";
    protected $fillable = [
        'id_pengawas', 'date_report', 'approved', 'approved_by', 'approval_note'
    ];

    public static function list_item_dealer($id_dealer, $id_parent){
        if($id_parent != 0){
            $query = DB::table('weekly_item')
                ->select('id_weekly_item', 'nama_item', 'unit_item')
                ->where('is_deleted', 0)
                ->where('id_parent', $id_parent);
        } else{
            $query = DB::table('weekly_item')
                ->join('weekly_item_dealer', 'weekly_item.id_weekly_item', '=', 'weekly_item_dealer.id_weekly_item')
                ->select('weekly_item.id_weekly_item', 'nama_item', 'unit_item')
                ->where('id_dealer', $id_dealer)
                ->where('weekly_item.is_deleted', 0)
                ->where('weekly_item_dealer.is_deleted', 0)
                ->where('weekly_item.id_parent', NULL);
        }

        $data = $query->get();

        return $data;
    }

    public static function add_report($data, $id_pengawas){
        $data['id_pengawas'] = $id_pengawas;
        $insert = DB::table('weekly_report')->insertGetId($data);

        $data = DB::table('weekly_report')->where('id_weekly_report', $insert)->get();

        return $data;
    }

    public static function add_report_detail($data, $id_weekly_report){
        $data['id_weekly_report'] = $id_weekly_report;
        $insert = DB::table('weekly_report_detail')->insertGetId($data);

        $data = DB::table('weekly_report_detail')->where('id_weekly_report_detail', $insert)->get();

        return $data;
    }
    
}