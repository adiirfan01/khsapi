<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class KontraktorModel extends Model
{

    protected $table = "kontraktor";
    protected $primaryKey = "id_kontraktor";
	protected $fillable = [
        'nama_kontraktor'
    ];
    
}