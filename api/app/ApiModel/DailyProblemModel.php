<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DailyProblemModel extends Model
{

    protected $table = "daily_problem";
    protected $primaryKey = "id_daily_problem";
	protected $fillable = [
        'id_pengawas', 'date_problem', 'title_problem', 'desc_problem'
    ];

    public static function simple_daily_problem_list($id_pengawas=NULL, $id_dealer=NULL, $date_problem=NULL){

        $query = DB::table('daily_problem')
        	->select('id_daily_problem', 'daily_problem.id_pengawas', 'user.nama_user AS nama_pengawas', 'title_problem', 'date_problem', 'nama_dealer', DB::Raw('DATE_FORMAT(date_problem, "%d %M %Y") AS date_format'))
        	->join('pengawas', 'daily_problem.id_pengawas', '=', 'pengawas.id_pengawas')
        	->join('user', 'pengawas.id_user', '=', 'user.id_user')
        	->join('dealer', 'pengawas.id_dealer', '=', 'dealer.id_dealer');

        if($id_pengawas != NULL && $id_pengawas != "-"){
            $query->where('daily_problem.id_pengawas', $id_pengawas);
        }

        if($id_dealer != NULL && $id_dealer != "-"){
            $query->where('pengawas.id_dealer', $id_dealer);
        }

        if($date_problem != NULL && $id_pengawas != "-"){
            $query->where('daily_problem.date_problem', $date_problem);
        }

        $dailyproblem = $query
        			->orderBy('date_problem', 'DESC')
        			->get();

        if($dailyproblem != NULL){
            return $dailyproblem;
        } else{
            return 0;
        }

    }
    
}