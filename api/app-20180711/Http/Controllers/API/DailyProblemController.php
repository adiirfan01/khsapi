<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\ApiModel\LoginModel;
use App\ApiModel\DailyProblemModel;
use Validator;

class DailyProblemController extends APIBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        /*$id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }*/

        $dailyproblem = DailyProblemModel::all();
        return $this->sendResponse($dailyproblem->toArray(), 'List daily problem retrieved successfully.');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 3){
            return $this->sendError('You have no access for this page.');
        }

        $validator = Validator::make($input, [
            'date_problem' => 'date_format:"Y-m-d"|required',
            'title_problem' => 'required',
            'desc_problem' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $id_pengawas = LoginModel::get_id_pengawas($token);
        $input['id_pengawas'] = $id_pengawas;

        $dailyproblem = DailyProblemModel::create($input);
        return $this->sendResponse($dailyproblem->toArray(), 'Daily problem created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $dailyproblem = DailyProblemModel::find($id);

        if (is_null($dailyproblem)) {
            return $this->sendError('Daily problem not found.');
        }

        return $this->sendResponse($dailyproblem->toArray(), 'Daily problem retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $validator = Validator::make($input, [
            'date_problem' => 'date_format:"Y-m-d"|required',
            'title_problem' => 'required',
            'desc_problem' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $dailyproblem = DailyProblemModel::find($id);
        if (is_null($dailyproblem)) {
            return $this->sendError('Daily problem not found.');
        }

        $dailyproblem->date_problem = $input['date_problem'];
        $dailyproblem->title_problem = $input['title_problem'];
        $dailyproblem->desc_problem = $input['desc_problem'];
        $dailyproblem->save();

        return $this->sendResponse($dailyproblem->toArray(), 'Daily problem updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $dailyproblem = DailyProblemModel::find($id);

        if (is_null($dailyproblem)) {
            return $this->sendError('Daily problem not found.');
        }

        $dailyproblem->delete();

        return $this->sendResponse($id, 'Daily problem deleted successfully.');
    }

}