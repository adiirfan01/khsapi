<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\ApiModel\LoginModel;
use Validator;

class LoginController extends APIBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*$posts = Login::all();
        return $this->sendResponse($posts->toArray(), 'Login retrieved successfully.');*/

        $input = $request->all();

        $validator = Validator::make($input, [
            'username' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $check_user_login = LoginModel::check_user_login($input['username'], $input['password']);
        return $this->sendResponse($check_user_login, 'Login retrieved successfully.');
        //print_r($tes);die;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'username' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $check_user_login = LoginModel::check_user_login($input['username'], $input['password']);

        $data_pengawas = array();
        if($check_user_login->id_user_category == 3){
            $id_pengawas = LoginModel::get_id_pengawas($check_user_login->token);

            $data_pengawas = LoginModel::get_data_pengawas($id_pengawas);
        }

        return $this->sendResponse(array_merge((array) $check_user_login, (array) $data_pengawas), 'Login retrieved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->sendError('Page not found.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->sendError('Page not found.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->sendError('Page not found.');
    }

    public function destroytoken(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');

        $disable_token = LoginModel::disable_token($token);

        return $this->sendResponse($disable_token, 'Logout successfully.');
    }

    public function datapengawas(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');

        $id_pengawas = LoginModel::get_id_pengawas($token);

        $data_pengawas = LoginModel::get_data_pengawas($id_pengawas);

        return $this->sendResponse($data_pengawas, 'Data Pengawas retrieved successfully.');
    }
}