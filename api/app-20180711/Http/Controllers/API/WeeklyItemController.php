<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\ApiModel\LoginModel;
use App\ApiModel\WeeklyItemModel;
use Validator;

class WeeklyItemController extends APIBaseController
{

    public function listparent(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $item_parent = WeeklyItemModel::list_parent();
        return $this->sendResponse($item_parent, 'List Parent Item retrieved successfully.');
        
    }

    public function showparent(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $item_parent = WeeklyItemModel::find_parent($id);

        if (is_null($item_parent)) {
            return $this->sendError('Parent Item not found.');
        }

        return $this->sendResponse($item_parent, 'Parent Item retrieved successfully.');
    }

    public function addparent(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $validator = Validator::make($input, [
            'nama_item' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $add_parent = WeeklyItemModel::add_parent($input);
        return $this->sendResponse($add_parent, 'Parent Item created successfully.');
    }

    public function editparent(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $validator = Validator::make($input, [
            'nama_item' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $item_parent = WeeklyItemModel::find_parent($id);
        if (is_null($item_parent)) {
            return $this->sendError('Parent Item not found.');
        }

        $update_item_parent = WeeklyItemModel::edit_parent($id, $input);

        return $this->sendResponse($update_item_parent, 'Parent Item updated successfully.');
    }

    public function deleteparent(Request $request, $id)
    {

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $item_parent = WeeklyItemModel::find_parent($id);

        if (is_null($item_parent)) {
            return $this->sendError('Parent Item not found.');
        }

        $update_item_parent = WeeklyItemModel::edit_parent($id, array('is_deleted'=>1));

        return $this->sendResponse($update_item_parent, 'Parent Item deleted successfully.');
    }

    public function addparentdealer(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $validator = Validator::make($input, [
            'id_dealer' => 'required',
            'id_weekly_item' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $add_parent_dealer = WeeklyItemModel::add_parent_dealer($input);
        return $this->sendResponse($add_parent_dealer, 'Parent Item Dealer created successfully.');
    }

    public function listparentdealer(Request $request, $id_weekly_item)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $item_parent = WeeklyItemModel::list_parent_dealer($id_weekly_item);
        return $this->sendResponse($item_parent, 'List Parent Item retrieved successfully.');
        
    }

    public function deleteparentdealer(Request $request, $id)
    {

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $item_parent_dealer = WeeklyItemModel::find_parent_dealer($id);

        if (is_null($item_parent_dealer)) {
            return $this->sendError('Parent Item Dealer not found.');
        }

        $update_item_parent = WeeklyItemModel::edit_parent_dealer($id, array('is_deleted'=>1));

        return $this->sendResponse($update_item_parent, 'Parent Item Dealer deleted successfully.');
    }

    public function showparentdealer(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $item_parent_dealer = WeeklyItemModel::find_parent_dealer($id);

        if (is_null($item_parent_dealer)) {
            return $this->sendError('Parent Item Dealer not found.');
        }

        return $this->sendResponse($item_parent_dealer, 'Parent Item Dealer retrieved successfully.');
    }

    // start for child item

    public function listchild(Request $request, $id_parent)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $item_parent = WeeklyItemModel::list_child($id_parent);
        return $this->sendResponse($item_parent, 'List Child Item retrieved successfully.');
    }

    public function addchild(Request $request, $id_parent)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $validator = Validator::make($input, [
            'nama_item' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $add_child = WeeklyItemModel::add_child($input, $id_parent);
        return $this->sendResponse($add_child, 'Child Item created successfully.');
    }

    // end for child item

}