<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\ApiModel\LoginModel;

class APIBaseController extends Controller
{

    public function sendResponse($result, $message)
    {
    	$response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];

        return response()->json($response, 200);
    }


    public function sendError($error, $errorMessages = [], $code = 404)
    {
    	$response = [
            'success' => false,
            'message' => $error,
        ];

        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }

        return response()->json($response, $code);
    }

    public function currentTime()
    {
        //return date('H:i', strtotime("18:00"));
        return date('H:i', time()+25200);
    }

    public function convertDate($date, $is_month=0)
    {
        $day = date('d', strtotime($date));
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));

        $month_text = "";
        switch ($month) {
            case 1:
                $month_text = "Januari";
                break;
            case 2:
                $month_text = "Februari";
                break;
            case 3:
                $month_text = "Maret";
                break;
            case 4:
                $month_text = "April";
                break;
            case 5:
                $month_text = "Mei";
                break;
            case 6:
                $month_text = "Juni";
                break;
            case 7:
                $month_text = "Juli";
                break;
            case 8:
                $month_text = "Agustus";
                break;
            case 9:
                $month_text = "September";
                break;
            case 10:
                $month_text = "Oktober";
                break;
            case 11:
                $month_text = "November";
                break;
            case 12:
                $month_text = "Desember";
                break;
            default:
                $month_text = "Januari";
        }

        if($is_month == 1){
            return $month_text." ".$year;
        } else{
            return $day." ".$month_text." ".$year;
        }
    }

    /*public function send_notification($token, $title, $message, $data=array())
    {  
        define( 'API_ACCESS_KEY', 'AAAA46l0tHI:APA91bEr1PtGrwZG9wP7e0UkA7KFAKdLGzpvhldUN0mWGYICHTEH4oSYezRAsYF3n8ge6FknTsvij-ZEK79sDGWUE-ynD97XqMnYP1oCvn0I4MFnWcHTHfnVdAmC7LHAQVOgcYbhDEOz');
        
        $msg = array
            (
                'body'  => $message,
                'title' => $title, ,
                'sound'     => 'default',
                'vibrate'   => 1
            );

        $fields = array
            (
                'to'        => $token,
                'notification'  => $msg,
                'data'      => $data
            );
        
        $headers = array
                (
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        
        $result = curl_exec( $ch );
        
        curl_close( $ch );
    }*/
}