<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\ApiModel\LoginModel;
use App\ApiModel\MonthlyReportModel;
use App\ApiModel\PengawasModel;
use Validator;
use App\Http\Requests;
use App\Image;
use Illuminate\Http\File;
use Spipu\Html2Pdf\Html2Pdf;

class MonthlyReportController extends APIBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $monthlyreport = MonthlyReportModel::all();

        return $this->sendResponse($monthlyreport->toArray(), 'List monthly report retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 3){
            return $this->sendError('You have no access for this page.');
        }

        $validator = Validator::make($input, [
            'date_report' => 'date_format:"Y-m-d"|required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $id_pengawas = LoginModel::get_id_pengawas($token);
        $input['id_pengawas'] = $id_pengawas;

        if(isset($input['exterior_siang'])){
            $extension = $request->file('exterior_siang')->guessExtension();
            $name = date('Ymd').'-exterior_siang-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('exterior_siang')->move($destination, $name);

            $input['exterior_siang'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['exterior_malam'])){
            $extension = $request->file('exterior_malam')->guessExtension();
            $name = date('Ymd').'-exterior_malam-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('exterior_malam')->move($destination, $name);

            $input['exterior_malam'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['sales_finance_front'])){
            $extension = $request->file('sales_finance_front')->guessExtension();
            $name = date('Ymd').'-sales_finance_front-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('sales_finance_front')->move($destination, $name);

            $input['sales_finance_front'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['parts_front'])){
            $extension = $request->file('parts_front')->guessExtension();
            $name = date('Ymd').'-parts_front-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('parts_front')->move($destination, $name);

            $input['parts_front'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['service_front'])){
            $extension = $request->file('service_front')->guessExtension();
            $name = date('Ymd').'-service_front-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('service_front')->move($destination, $name);

            $input['service_front'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['advisor_desk'])){
            $extension = $request->file('advisor_desk')->guessExtension();
            $name = date('Ymd').'-advisor_desk-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('advisor_desk')->move($destination, $name);

            $input['advisor_desk'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['hero_stage'])){
            $extension = $request->file('hero_stage')->guessExtension();
            $name = date('Ymd').'-hero_stage-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('hero_stage')->move($destination, $name);

            $input['hero_stage'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['isolate_stage_1'])){
            $extension = $request->file('isolate_stage_1')->guessExtension();
            $name = date('Ymd').'-isolate_stage_1-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('isolate_stage_1')->move($destination, $name);

            $input['isolate_stage_1'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['isolate_stage_2'])){
            $extension = $request->file('isolate_stage_2')->guessExtension();
            $name = date('Ymd').'-isolate_stage_2-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('isolate_stage_2')->move($destination, $name);

            $input['isolate_stage_2'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['wall_system_display'])){
            $extension = $request->file('wall_system_display')->guessExtension();
            $name = date('Ymd').'-wall_system_display-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('wall_system_display')->move($destination, $name);

            $input['wall_system_display'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['glass_showcase_1'])){
            $extension = $request->file('glass_showcase_1')->guessExtension();
            $name = date('Ymd').'-glass_showcase_1-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('glass_showcase_1')->move($destination, $name);

            $input['glass_showcase_1'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['glass_showcase_2'])){
            $extension = $request->file('glass_showcase_2')->guessExtension();
            $name = date('Ymd').'-glass_showcase_2-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('glass_showcase_2')->move($destination, $name);

            $input['glass_showcase_2'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['spec_stand_1'])){
            $extension = $request->file('spec_stand_1')->guessExtension();
            $name = date('Ymd').'-spec_stand_1-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('spec_stand_1')->move($destination, $name);

            $input['spec_stand_1'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['spec_stand_2'])){
            $extension = $request->file('spec_stand_2')->guessExtension();
            $name = date('Ymd').'-spec_stand_2-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('spec_stand_2')->move($destination, $name);

            $input['spec_stand_2'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['spec_stand_3'])){
            $extension = $request->file('spec_stand_3')->guessExtension();
            $name = date('Ymd').'-spec_stand_3-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('spec_stand_3')->move($destination, $name);

            $input['spec_stand_3'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['spec_stand_4'])){
            $extension = $request->file('spec_stand_4')->guessExtension();
            $name = date('Ymd').'-spec_stand_4-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('spec_stand_4')->move($destination, $name);

            $input['spec_stand_4'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['spec_stand_5'])){
            $extension = $request->file('spec_stand_5')->guessExtension();
            $name = date('Ymd').'-spec_stand_5-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('spec_stand_5')->move($destination, $name);

            $input['spec_stand_5'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['brochure_rack'])){
            $extension = $request->file('brochure_rack')->guessExtension();
            $name = date('Ymd').'-brochure_rack-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('brochure_rack')->move($destination, $name);

            $input['brochure_rack'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['internet_counter'])){
            $extension = $request->file('internet_counter')->guessExtension();
            $name = date('Ymd').'-internet_counter-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('internet_counter')->move($destination, $name);

            $input['internet_counter'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['ci_logo'])){
            $extension = $request->file('ci_logo')->guessExtension();
            $name = date('Ymd').'-ci_logo-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('ci_logo')->move($destination, $name);

            $input['ci_logo'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['direction_bengkel'])){
            $extension = $request->file('direction_bengkel')->guessExtension();
            $name = date('Ymd').'-direction_bengkel-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('direction_bengkel')->move($destination, $name);

            $input['direction_bengkel'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['direction_penjualan'])){
            $extension = $request->file('direction_penjualan')->guessExtension();
            $name = date('Ymd').'-direction_penjualan-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('direction_penjualan')->move($destination, $name);

            $input['direction_penjualan'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['direction_ruang_tunggu'])){
            $extension = $request->file('direction_ruang_tunggu')->guessExtension();
            $name = date('Ymd').'-direction_ruang_tunggu-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('direction_ruang_tunggu')->move($destination, $name);

            $input['direction_ruang_tunggu'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['direction_toilet'])){
            $extension = $request->file('direction_toilet')->guessExtension();
            $name = date('Ymd').'-direction_toilet-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('direction_toilet')->move($destination, $name);

            $input['direction_toilet'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['direction_pit'])){
            $extension = $request->file('direction_pit')->guessExtension();
            $name = date('Ymd').'-direction_pit-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('direction_pit')->move($destination, $name);

            $input['direction_pit'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['direction_kasir'])){
            $extension = $request->file('direction_kasir')->guessExtension();
            $name = date('Ymd').'-direction_kasir-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('direction_kasir')->move($destination, $name);

            $input['direction_kasir'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['direction_toilet_wall'])){
            $extension = $request->file('direction_toilet_wall')->guessExtension();
            $name = date('Ymd').'-direction_toilet_wall-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('direction_toilet_wall')->move($destination, $name);

            $input['direction_toilet_wall'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['direction_smoking'])){
            $extension = $request->file('direction_smoking')->guessExtension();
            $name = date('Ymd').'-direction_smoking-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('direction_smoking')->move($destination, $name);

            $input['direction_smoking'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['panel_poster_display'])){
            $extension = $request->file('panel_poster_display')->guessExtension();
            $name = date('Ymd').'-panel_poster_display-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('panel_poster_display')->move($destination, $name);

            $input['panel_poster_display'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['poster_display'])){
            $extension = $request->file('poster_display')->guessExtension();
            $name = date('Ymd').'-poster_display-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('poster_display')->move($destination, $name);

            $input['poster_display'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['spareparts_display'])){
            $extension = $request->file('spareparts_display')->guessExtension();
            $name = date('Ymd').'-spareparts_display-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('spareparts_display')->move($destination, $name);

            $input['spareparts_display'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['helmet_rack'])){
            $extension = $request->file('helmet_rack')->guessExtension();
            $name = date('Ymd').'-helmet_rack-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('helmet_rack')->move($destination, $name);

            $input['helmet_rack'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['magazine_rack'])){
            $extension = $request->file('magazine_rack')->guessExtension();
            $name = date('Ymd').'-magazine_rack-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('magazine_rack')->move($destination, $name);

            $input['magazine_rack'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['banner_stand'])){
            $extension = $request->file('banner_stand')->guessExtension();
            $name = date('Ymd').'-banner_stand-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('banner_stand')->move($destination, $name);

            $input['banner_stand'] = 'uploads/monthlyreport/'.$name;
        }

        if(isset($input['island_display'])){
            $extension = $request->file('island_display')->guessExtension();
            $name = date('Ymd').'-island_display-'.time().'.'.$extension;
            $destination = base_path() . '/public/uploads/monthlyreport';
            $request->file('island_display')->move($destination, $name);

            $input['island_display'] = 'uploads/monthlyreport/'.$name;
        }

        $insert = MonthlyReportModel::create($input);

        return $this->sendResponse($insert->toArray(), 'Insert monthly report successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Login::find($id);

        if (is_null($post)) {
            return $this->sendError('Post not found.');
        }

        return $this->sendResponse($post->toArray(), 'Login retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $validator = Validator::make($input, [
            'id_user_category' => 'required',
            'nama_user' => 'required',
            'username' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        if($input['id_user_category'] == 3){
            $validator = Validator::make($input, [
                'id_supervisor' => 'required',
                'id_kontraktor' => 'required',
                'id_main_dealer' => 'required',
                'id_dealer' => 'required'
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors());       
            }
        }

        $post_user = UserModel::find($id);
        if (is_null($post_user)) {
            return $this->sendError('User not found.');
        }

        $post_user->id_user_category = $input['id_user_category'];
        $post_user->nama_user = $input['nama_user'];
        $post_user->username = $input['username'];
        $post_user->save();

        $result = $post_user->toArray();

        if($input['id_user_category'] == 3){

            $id_pengawas = UserModel::get_pengawas_id($id);

            $post_pengawas = PengawasModel::find($id_pengawas);
            if (is_null($post_pengawas)) {
                return $this->sendError('Pengawas not found.');
            }

            $post_pengawas->id_supervisor = $input['id_supervisor'];
            $post_pengawas->id_kontraktor = $input['id_kontraktor'];
            $post_pengawas->id_main_dealer = $input['id_main_dealer'];
            $post_pengawas->id_dealer = $input['id_dealer'];
            $post_pengawas->save();
            $result = array_merge($post_user->toArray(), $post_pengawas->toArray());
        }

        unset($result['password']);
        return $this->sendResponse($result, 'Update user successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post_user = UserModel::find($id);

        if (is_null($post_user)) {
            return $this->sendError('User not found.');
        }

        $post_user->delete();

        $id_pengawas = UserModel::get_pengawas_id($id);
        if($id_pengawas != 0){
            $post_pengawas = PengawasModel::find($id_pengawas);

            if (is_null($post_pengawas)) {
                return $this->sendError('Pengawas not found.');
            }

            $post_pengawas->delete();
        }

        return $this->sendResponse($id, 'User deleted successfully.');
    }

    public function categories(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $categories = UserModel::get_all_category();
        return $this->sendResponse($categories->toArray(), 'Categories retrieved successfully.');
    }

    public function supervisor(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $supervisor = UserModel::get_all_supervisor();
        return $this->sendResponse($supervisor->toArray(), 'Supervisor retrieved successfully.');
    }

    public function kontraktor(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $kontraktor = UserModel::get_all_kontraktor();
        return $this->sendResponse($kontraktor->toArray(), 'Kontraktor retrieved successfully.');
    }

    public function maindealer(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $main_dealer = UserModel::get_all_main_dealer();
        return $this->sendResponse($main_dealer->toArray(), 'Main Dealer retrieved successfully.');
    }

    public function dealer(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $dealer = UserModel::get_all_dealer();
        return $this->sendResponse($dealer->toArray(), 'Dealer retrieved successfully.');
    }

    public function approval(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $monthlyreport = MonthlyReportModel::find($id);
        if (is_null($monthlyreport)) {
            return $this->sendError('Dealer not found.');
        }

        if(isset($input['approved'])){
            $monthlyreport->approved = $input['approved'];
        }
        if(isset($input['approval_note'])){
            $monthlyreport->approval_note = $input['approval_note'];
        }
        $monthlyreport->approved_by = LoginModel::get_id_user($token);
        $monthlyreport->save();

        $dummy_result = $monthlyreport->toArray();
        $result = array(
                    "id_monthly_report"     => $dummy_result['id_monthly_report'],
                    "id_pengawas"           => $dummy_result['id_pengawas'],
                    "approved"              => $dummy_result['approved'],
                    "approved_by"           => $dummy_result['approved_by'],
                    "approval_note"         => $dummy_result['approval_note']
                );

        return $this->sendResponse($result, 'Monthly Report Approval successfully.');
    }

    public function simplelist(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_pengawas = NULL;
        $id_dealer = NULL;
        $date_report = NULL;
        if(isset($input['id_pengawas'])){
            $id_pengawas = $input['id_pengawas'];
        }
        if(isset($input['id_dealer'])){
            $id_dealer = $input['id_dealer'];
        }
        if(isset($input['date_report'])){
            $date_report = $input['date_report'];
        }
        
        $dailyreport = MonthlyReportModel::simple_monthly_report_list($id_pengawas, $id_dealer, $date_report);

        //$result = $dailyreport->toArray();
        
        /*foreach ($result as $key => $value) {
            $result[$key]['daily_report_progress'] = DailyReportProgressModel::daily_report_progress($value['id_daily_report']);
        }*/

        return $this->sendResponse($dailyreport, 'List monthly report retrieved successfully.');
    }

    public function topdf(Request $request, $id)
    {        

        /*$validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }*/

        $monthlyreport = MonthlyReportModel::find($id);

        $data['images'] = array();
        $j = 0;
        $k = 0;
        $monthlyreport_key = array_keys($monthlyreport->toArray());
        for($i=3; $i<sizeof($monthlyreport_key)-5; $i++){
            if($i%2 == 1 && $monthlyreport[$monthlyreport_key[$i]] != NULL){
                $data['images'][$j][$k] = array(
                        'title'     => $this->get_field_title($monthlyreport_key[$i]),
                        'image'     => $monthlyreport[$monthlyreport_key[$i]],
                        'caption'   => ($monthlyreport[$monthlyreport_key[$i].'_text'] != NULL && $monthlyreport[$monthlyreport_key[$i].'_text'] != '' ? $monthlyreport[$monthlyreport_key[$i].'_text'] : "-")
                    );

                $k++;

                if($k % 4 == 0){
                    $j++;
                    $k = 0;
                }
            }
        }

        /*if($monthlyreport['exterior_siang'] != NULL){

        }*/

        $pengawas = PengawasModel::get_detail_pengawas($monthlyreport['id_pengawas']);
        $data['pengawas'] = ($pengawas != NULL ? $pengawas : NULL);

        $data['convert_date'] = $this->convertDate($monthlyreport['date_report'], 1);

        $content = view('report_pdf/monthly', $data)->render();
        //require_once($html2pdf_path);
        
        $html2pdf = new Html2Pdf('P', 'Legal', 'en', true, 'UTF-8', array(25.4, 20.4, 25.4, 20.4));
            $html2pdf->pdf->SetTitle('Laporan Bulanan - '.$this->convertDate($monthlyreport['date_report']));
            $html2pdf->WriteHTML($content);
            /*$html2pdf->Output(base_path() . '/public/pdf/monthlyreport/laporan_harian_'.date('Y_m_d', strtotime($monthlyreport['date_report'])).'.pdf', 'F');*/
            $html2pdf->Output(base_path() . '/public/pdf/monthlyreport/laporan_harian_'.date('Y_m_d', strtotime($monthlyreport['date_report'])).'.pdf');

        $result = array(
                'pdf_url'   => url('/') . '/pdf/monthlyreport/laporan_harian_'.date('Y_m_d', strtotime($monthlyreport['date_report'])).'.pdf'
            );
        return $this->sendResponse($result, 'Create report pdf successfully.');

    }

    public function get_field_title($field){
        switch ($field) {
            case 'exterior_siang':
                return "Exterior Siang";
                break;

            case 'exterior_malam':
                return "Exterior Malam";
                break;

            case 'sales_finance_front':
                return "Sales and Finance Front";
                break;

            case 'parts_front':
                return "Parts Front";
                break;

            case 'service_front':
                return "Service Front";
                break;

            case 'advisor_desk':
                return "Service Advisor Desk";
                break;

            case 'hero_stage':
                return "Hero Stage";
                break;

            case 'isolate_stage_1':
                return "Isolate Stage I";
                break;

            case 'isolate_stage_2':
                return "Isolate Stage II";
                break;

            case 'wall_system_display':
                return "Wall System Display";
                break;

            case 'glass_showcase_1':
                return "Glass Showcase I";
                break;

            case 'glass_showcase_2':
                return "Glass Showcase II";
                break;

            case 'spec_stand_1':
                return "Spec Stand I";
                break;

            case 'spec_stand_2':
                return "Spec Stand II";
                break;

            case 'spec_stand_3':
                return "Spec Stand III";
                break;

            case 'spec_stand_4':
                return "Spec Stand IV";
                break;

            case 'spec_stand_5':
                return "Spec Stand V";
                break;

            case 'brochure_rack':
                return "Brochure Rack (Self Standing)";
                break;

            case 'internet_counter':
                return "Internet Counter";
                break;

            case 'ci_logo':
                return "CI Logo";
                break;

            case 'direction_bengkel':
                return "Direction Sign Layanan Bengkel dan Suku Cadang";
                break;

            case 'direction_penjualan':
                return "Direction Sign Penjualan dan Pembiayaan";
                break;

            case 'direction_ruang_tunggu':
                return "Direction Sign Ruang Tunggu dan No Smoking";
                break;

            case 'direction_toilet':
                return "Direction Sign Toilet";
                break;

            case 'direction_pit':
                return "Direction Sign Pit";
                break;

            case 'direction_kasir':
                return "Direction Sign Kasir";
                break;

            case 'direction_toilet_wall':
                return "Direction Sign Toilet Wall";
                break;

            case 'direction_smoking':
                return "Direction Sign Smoking";
                break;

            case 'panel_poster_display':
                return "9 Panel Poster Display";
                break;

            case 'poster_display':
                return "Poster Display";
                break;

            case 'spareparts_display':
                return "Spareparts Display & Stock Rack (Khusus 2, 3 Ruko)";
                break;

            case 'helmet_rack':
                return "Helmet Rack";
                break;

            case 'magazine_rack':
                return "Magazine & Newspaper Rack";
                break;

            case 'banner_stand':
                return "HRT Banner Stand";
                break;

            case 'island_display':
                return "Island Display (Khusus 4 Ruko)";
                break;
            
            default:
                return "-";
                break;
        }
    }
}