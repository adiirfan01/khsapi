<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\ApiModel\LoginModel;
use App\ApiModel\UserModel;
use App\ApiModel\PengawasModel;
use Validator;

class UserController extends APIBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->sendError('Page not found.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $validator = Validator::make($input, [
            'id_user_category' => 'required',
            'nama_user' => 'required',
            'username' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }


        if($input['id_user_category'] == 3){
            $validator = Validator::make($input, [
                'id_supervisor' => 'required',
                'id_kontraktor' => 'required',
                'id_main_dealer' => 'required',
                'id_dealer' => 'required'
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors());       
            }
        }

        $input['password'] = md5($input['password']);
        $insert_user = UserModel::create($input);
        $result = $insert_user->toArray();

        if($input['id_user_category'] == 3){
            $input['id_user'] = $result['id_user'];
            $insert_pengawas = PengawasModel::create($input);
            $result = array_merge($insert_user->toArray(), $insert_pengawas->toArray());
        }

        return $this->sendResponse($result, 'Insert user successfully.');

        //$check_user_login = LoginModel::check_user_login($input['username'], $input['password']);
        //return $this->sendResponse($check_user_login, 'Login retrieved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Login::find($id);

        if (is_null($post)) {
            return $this->sendError('Post not found.');
        }

        return $this->sendResponse($post->toArray(), 'Login retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $input = $request->all();

        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $validator = Validator::make($input, [
            'id_user_category' => 'required',
            'nama_user' => 'required',
            'username' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        if($input['id_user_category'] == 3){
            $validator = Validator::make($input, [
                'id_supervisor' => 'required',
                'id_kontraktor' => 'required',
                'id_main_dealer' => 'required',
                'id_dealer' => 'required'
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors());       
            }
        }

        $post_user = UserModel::find($id);
        if (is_null($post_user)) {
            return $this->sendError('User not found.');
        }

        $post_user->id_user_category = $input['id_user_category'];
        $post_user->nama_user = $input['nama_user'];
        $post_user->username = $input['username'];
        $post_user->save();

        $result = $post_user->toArray();

        if($input['id_user_category'] == 3){

            $id_pengawas = UserModel::get_pengawas_id($id);

            $post_pengawas = PengawasModel::find($id_pengawas);
            if (is_null($post_pengawas)) {
                return $this->sendError('Pengawas not found.');
            }

            $post_pengawas->id_supervisor = $input['id_supervisor'];
            $post_pengawas->id_kontraktor = $input['id_kontraktor'];
            $post_pengawas->id_main_dealer = $input['id_main_dealer'];
            $post_pengawas->id_dealer = $input['id_dealer'];
            $post_pengawas->save();
            $result = array_merge($post_user->toArray(), $post_pengawas->toArray());
        }

        unset($result['password']);
        return $this->sendResponse($result, 'Update user successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post_user = UserModel::find($id);

        if (is_null($post_user)) {
            return $this->sendError('User not found.');
        }

        $post_user->delete();

        $id_pengawas = UserModel::get_pengawas_id($id);
        if($id_pengawas != 0){
            $post_pengawas = PengawasModel::find($id_pengawas);

            if (is_null($post_pengawas)) {
                return $this->sendError('Pengawas not found.');
            }

            $post_pengawas->delete();
        }

        return $this->sendResponse($id, 'User deleted successfully.');
    }

    public function categories(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $categories = UserModel::get_all_category();
        return $this->sendResponse($categories->toArray(), 'Categories retrieved successfully.');
    }

    public function supervisor(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $supervisor = UserModel::get_all_supervisor();
        return $this->sendResponse($supervisor->toArray(), 'Supervisor retrieved successfully.');
    }

    public function kontraktor(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $kontraktor = UserModel::get_all_kontraktor();
        return $this->sendResponse($kontraktor->toArray(), 'Kontraktor retrieved successfully.');
    }

    public function maindealer(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $main_dealer = UserModel::get_all_main_dealer();
        return $this->sendResponse($main_dealer->toArray(), 'Main Dealer retrieved successfully.');
    }

    public function dealer(Request $request)
    {
        $validator = Validator::make($request->header(), [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $token = $request->header('token');
        $is_token_active = LoginModel::is_token_active($token);

        if($is_token_active == 0){
            return $this->sendError('Token not found or inactive.');
        }

        $id_user_category = LoginModel::get_user_category($token);
        if($id_user_category != 2){
            return $this->sendError('You have no access for this page.');
        }

        $dealer = UserModel::get_all_dealer();
        return $this->sendResponse($dealer->toArray(), 'Dealer retrieved successfully.');
    }
}