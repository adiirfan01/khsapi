<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\ApiModel\LoginModel;
use App\ApiModel\AbsenModel;
use Validator;

class AbsenController extends APIBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*$posts = Login::all();
        return $this->sendResponse($posts->toArray(), 'Login retrieved successfully.');*/

        $input = $request->all();

        $validator = Validator::make($input, [
            'username' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $check_user_login = LoginModel::check_user_login($input['username'], $input['password']);
        return $this->sendResponse($check_user_login, 'Login retrieved successfully.');
        //print_r($tes);die;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($input, [
            'token' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $is_token_active = LoginModel::is_token_active($input['token']);

        if($is_token_active == false){
            return $this->sendError('Token not found or inactive.');
        }

        $validator = Validator::make($input, [
            'username' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        //$check_user_login = LoginModel::check_user_login($input['username'], $input['password']);
        //return $this->sendResponse($check_user_login, 'Login retrieved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Login::find($id);

        if (is_null($post)) {
            return $this->sendError('Post not found.');
        }

        return $this->sendResponse($post->toArray(), 'Login retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $post = Login::find($id);
        if (is_null($post)) {
            return $this->sendError('Post not found.');
        }

        $post->name = $input['name'];
        $post->description = $input['description'];
        $post->save();

        return $this->sendResponse($post->toArray(), 'Login updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Login::find($id);

        if (is_null($post)) {
            return $this->sendError('Login not found.');
        }

        $post->delete();

        return $this->sendResponse($id, 'Tag deleted successfully.');
    }


    public function tes()
    {
        echo "coba";
    }
}