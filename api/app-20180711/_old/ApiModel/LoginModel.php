<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class LoginModel extends Model
{

	protected $fillable = [
        'username', 'password',
    ];

    public static function check_user_login($username, $password)
    {
    	$user = DB::table('user')
    				->where('username', $username)
                    ->where('password', md5($password))
                    ->first();

        if($user != NULL){

            $disable_user_tokens = DB::table('user_token')
                                    ->where('id_user', $user->id_user)
                                    ->update(['inactive' => 1]);

            $data_token = md5(date('Y-m-d H:i:s').$user->id_user);
            $new_token = DB::table('user_token')->insert(
                            [
                                'id_user' => $user->id_user, 
                                'data_token' => $data_token,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s')
                            ]
                        );

            if($new_token == 1){
                $user->login_status = true;
                $user->token = $data_token;
                $user->message = "Login Success.";
            } else{
                $user->login_status = false;
                $user->message = "Login Failed. Please try again later.";
            }
        } else{
            $user = new \stdClass();
            $user->login_status = false;
            $user->message = "Username or Password wrong.";
        }

	    return $user;
    }

    public static function is_token_active($token)
    {
        $check_token = DB::table('user_token')
                    ->where('data_token', $token)
                    ->where('inactive', 0)
                    ->first();

        if($check_token != NULL){
            return true;
        } else{
            return false;
        }
    }
}