<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{

    protected $table = "user";
    protected $primaryKey = "id_user";
	protected $fillable = [
        'id_user_category', 'nama_user', 'username', 'password' 
    ];

    public static function get_all_category()
    {
        $categories = DB::table('user_category')
                    ->select('id_user_category', 'nama_user_category')
                    ->get();

        return $categories;
    }

    public static function get_all_supervisor()
    {
        $supervisor = DB::table('supervisor')
                    ->select('id_supervisor', 'nama_supervisor')
                    ->get();

        return $supervisor;
    }

    public static function get_all_kontraktor()
    {
        $kontraktor = DB::table('kontraktor')
                    ->select('id_kontraktor', 'nama_kontraktor')
                    ->get();

        return $kontraktor;
    }

    public static function get_all_main_dealer()
    {
        $main_dealer = DB::table('main_dealer')
                    ->select('id_main_dealer', 'nama_main_dealer')
                    ->get();

        return $main_dealer;
    }

    public static function get_all_dealer()
    {
        $dealer = DB::table('dealer')
                    ->select('id_dealer', 'nama_dealer', 'latitude_dealer', 'longitude_dealer')
                    ->get();

        return $dealer;
    }

    public static function get_pengawas_id($id_user)
    {
        $id_pengawas = DB::table('pengawas')
                    ->select('id_pengawas')
                    ->where('id_user', $id_user)
                    ->first();

        if($id_pengawas != NULL){
            return $id_pengawas->id_pengawas;    
        } else{
            return 0;
        }
    }
}