<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class SupervisorModel extends Model
{

    protected $table = "supervisor";
    protected $primaryKey = "id_supervisor";
	protected $fillable = [
        'nama_supervisor'
    ];
    
}