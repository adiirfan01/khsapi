<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DailyReportProgressModel extends Model
{

    protected $table = "daily_report_progress";
    protected $primaryKey = "id_daily_report_progress";
	protected $fillable = [
        'id_daily_report', 'progress_img', 'progress_text' 
    ];

    public static function daily_report_progress($id_daily_report)
    {
        $daily_report_progress = DB::table('daily_report_progress')
                    ->where('id_daily_report', $id_daily_report)
                    ->get();

        if($daily_report_progress != NULL){
            return $daily_report_progress;    
        } else{
            return 0;
        }
    }

    public static function count_daily_report_progress($id_daily_report)
    {
        $daily_report_progress = DB::table('daily_report_progress')
                        ->where('id_daily_report', $id_daily_report)
                        ->count();

        if($daily_report_progress != NULL){
            return $daily_report_progress;    
        } else{
            return 0;
        }
    }

    public static function limit_daily_report_progress($id_daily_report, $offset, $limit)
    {
        $daily_report_progress = DB::table('daily_report_progress')
                    ->where('id_daily_report', $id_daily_report)
                    ->offset($offset)
                    ->limit($limit)
                    ->get();

        if($daily_report_progress != NULL){
            return $daily_report_progress;    
        } else{
            return 0;
        }
    }
    
}