<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class MonthlyReportModel extends Model
{

    protected $table = "monthly_report";
    protected $primaryKey = "id_monthly_report";
	protected $fillable = [
        'id_pengawas', 'date_report', 'exterior_siang', 'exterior_siang_text', 'exterior_malam', 'exterior_malam_text', 'sales_finance_front', 'sales_finance_front_text', 'parts_front', 'parts_front_text', 'service_front', 'service_front_text', 'advisor_desk', 'advisor_desk_text', 'hero_stage', 'hero_stage_text', 'isolate_stage_1', 'isolate_stage_1_text', 'isolate_stage_2', 'isolate_stage_2_text', 'wall_system_display', 'wall_system_display_text', 'glass_showcase_1', 'glass_showcase_1_text', 'glass_showcase_2', 'glass_showcase_2_text', 'spec_stand_1', 'spec_stand_1_text', 'spec_stand_2', 'spec_stand_2_text', 'spec_stand_3', 'spec_stand_3_text', 'spec_stand_4', 'spec_stand_4_text', 'spec_stand_5', 'spec_stand_5_text', 'brochure_rack', 'brochure_rack_text', 'internet_counter', 'internet_counter_text', 'ci_logo', 'ci_logo_text', 'direction_bengkel', 'direction_bengkel_text', 'direction_penjualan', 'direction_penjualan_text', 'direction_ruang_tunggu', 'direction_ruang_tunggu_text', 'direction_toilet', 'direction_toilet_text', 'direction_pit', 'direction_pit_text', 'direction_kasir', 'direction_kasir_text', 'direction_toilet_wall', 'direction_toilet_wall_text', 'direction_smoking', 'direction_smoking_text', 'panel_poster_display', 'panel_poster_display_text', 'poster_display', 'poster_display_text', 'spareparts_display', 'spareparts_display_text', 'helmet_rack', 'helmet_rack_text', 'magazine_rack', 'magazine_rack_text', 'banner_stand', 'banner_stand_text', 'island_display', 'island_display_text'   
    ];

    public static function simple_monthly_report_list($id_pengawas=NULL, $id_dealer=NULL, $date_report=NULL){

        $query = DB::table('monthly_report')
        	->select('id_monthly_report AS id_report', 'monthly_report.id_pengawas', 'user.nama_user AS nama_pengawas', 'date_report', 'nama_dealer', 'approved', DB::Raw('DATE_FORMAT(date_report, "%d %M %Y") AS date_format'))
        	->join('pengawas', 'monthly_report.id_pengawas', '=', 'pengawas.id_pengawas')
        	->join('user', 'pengawas.id_user', '=', 'user.id_user')
        	->join('dealer', 'pengawas.id_dealer', '=', 'dealer.id_dealer');

        if($id_pengawas != NULL && $id_pengawas != "-"){
            $query->where('monthly_report.id_pengawas', $id_pengawas);
        }

        if($id_dealer != NULL && $id_dealer != "-"){
            $query->where('monthly_report.id_dealer', $id_dealer);
        }

        if($date_report != NULL && $id_pengawas != "-"){
            $query->where('monthly_report.date_report', $date_report);
        }

        $dailyreport = $query
        			->orderBy('date_report', 'DESC')
        			->get();

        if($dailyreport != NULL){
            return $dailyreport;
        } else{
            return 0;
        }

    }
    
}