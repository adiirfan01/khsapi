<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DailyProblemModel extends Model
{

    protected $table = "daily_problem";
    protected $primaryKey = "id_daily_problem";
	protected $fillable = [
        'id_pengawas', 'date_problem', 'title_problem', 'desc_problem'
    ];
    
}