<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class MainDealerModel extends Model
{

    protected $table = "main_dealer";
    protected $primaryKey = "id_main_dealer";
	protected $fillable = [
        'nama_main_dealer'
    ];
    
}