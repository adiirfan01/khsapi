<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DailyMomModel extends Model
{

    protected $table = "daily_mom";
    protected $primaryKey = "id_daily_mom";
	protected $fillable = [
        'id_pengawas', 'date_mom', 'common_condition', 'problem', 'attendance', 'report', 'to_do_list', 'approved', 'approved_by', 'approval_note'
    ];
    
}