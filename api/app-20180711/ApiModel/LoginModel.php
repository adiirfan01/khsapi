<?php

namespace App\ApiModel;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class LoginModel extends Model
{

	protected $fillable = [
        'username', 'password',
    ];

    public static function check_user_login($username, $password)
    {
    	$user = DB::table('user')
    				->where('username', $username)
                    ->where('password', md5($password))
                    ->first();

        if($user != NULL){

            $disable_user_tokens = DB::table('user_token')
                                    ->where('id_user', $user->id_user)
                                    ->update(['inactive' => 1]);

            $data_token = md5(date('Y-m-d H:i:s').$user->id_user);
            $new_token = DB::table('user_token')->insert(
                            [
                                'id_user' => $user->id_user, 
                                'data_token' => $data_token,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s')
                            ]
                        );

            if($new_token == 1){
                $user->login_status = true;
                $user->token = $data_token;
                $user->message = "Login Success.";
            } else{
                $user->login_status = false;
                $user->message = "Login Failed. Please try again later.";
            }
        } else{
            $user = new \stdClass();
            $user->login_status = false;
            $user->message = "Username or Password wrong.";
        }

	    return $user;
    }

    public static function is_token_active($token)
    {
        $check_token = DB::table('user_token')
                    ->where('data_token', $token)
                    ->where('inactive', 0)
                    ->first();

        if($check_token != NULL || $check_token != ''){
            return 1;
        } else{
            return 0;
        }
    }

    public static function get_user_category($token)
    {
        $id_user_category = DB::table('user_token')
                    ->join('user', 'user_token.id_user', '=', 'user.id_user')
                    ->select('id_user_category')
                    ->where('data_token', $token)
                    ->first();

        if($id_user_category != NULL){
            return $id_user_category->id_user_category;    
        } else{
            return 0;
        }
        
    }

    public static function get_id_user($token)
    {
        $id_user = DB::table('user_token')
                    ->select('id_user')
                    ->where('data_token', $token)
                    ->first();

        if($id_user != NULL){
            return $id_user->id_user;    
        } else{
            return 0;
        }
        
    }

    public static function get_id_pengawas($token)
    {
        $id_pengawas = DB::table('user_token')
                    ->join('pengawas', 'user_token.id_user', '=', 'pengawas.id_user')
                    ->select('id_pengawas')
                    ->where('data_token', $token)
                    ->first();

        if($id_pengawas != NULL){
            return $id_pengawas->id_pengawas;    
        } else{
            return 0;
        }
        
    }

    public static function get_data_pengawas($id_pengawas)
    {
        $data_pengawas = DB::table('pengawas')
                    ->where('id_pengawas', $id_pengawas)
                    ->first();

        return $data_pengawas;  
    }

    public static function disable_token($token)
    {
        $disable_token = DB::table('user_token')
                            ->where('data_token', $token)
                            ->update(['inactive' => 1]);

        $result = new \stdClass();
        if($disable_token == 1){
            $result->logout_status = true;
            $result->message = "Logout Success.";
        } else{
            $result->logout_status = false;
            $result->message = "Logout Failed. Please try again later.";
        }

        return $result;
    }
}