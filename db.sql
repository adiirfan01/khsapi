-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 28, 2019 at 11:31 PM
-- Server version: 10.2.21-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u5436601_khsapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `absen`
--

CREATE TABLE `absen` (
  `id_absen` int(11) NOT NULL,
  `id_pengawas` int(11) NOT NULL,
  `tgl_absen` date NOT NULL,
  `start_absen` time DEFAULT NULL,
  `start_latitude` varchar(150) NOT NULL,
  `start_longitude` varchar(150) NOT NULL,
  `end_absen` time DEFAULT NULL,
  `end_latitude` varchar(150) DEFAULT NULL,
  `end_longitude` varchar(150) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absen`
--

INSERT INTO `absen` (`id_absen`, `id_pengawas`, `tgl_absen`, `start_absen`, `start_latitude`, `start_longitude`, `end_absen`, `end_latitude`, `end_longitude`, `note`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-02-22', '08:00:00', '-6.242423', '106.844445', '17:30:00', '-6.242423', '106.844445', NULL, '2018-02-02 21:35:03', '2018-02-02 21:44:52'),
(4, 2, '2018-02-22', '08:00:00', '-6.242423', '106.844445', '17:30:00', '-6.242507', '106.844353', NULL, '2018-02-02 23:44:36', '2018-02-02 23:48:32'),
(5, 1, '2018-02-22', '08:00:00', '-6.242423', '106.844445', '17:30:00', '-6.242507', '106.844353', NULL, '2018-02-02 23:44:36', '2018-02-02 23:48:32'),
(6, 2, '2018-02-22', '08:00:00', '-6.242423', '106.844445', '17:30:00', '-6.242507', '106.844353', NULL, '2018-02-02 23:44:36', '2018-02-02 23:48:32'),
(7, 1, '2018-02-22', '08:00:00', '-6.242423', '106.844445', '17:30:00', '-6.242507', '106.844353', NULL, '2018-02-02 23:44:36', '2018-02-02 23:48:32'),
(8, 1, '2018-02-03', '15:01:00', '-6.242423', '106.844445', '20:53:00', '-6.242423', '106.844445', 'nfnf', '2018-02-28 08:01:55', '2018-03-09 13:53:02'),
(9, 1, '2018-03-02', '15:38:00', '-6.3460098', '106.7438279', NULL, NULL, NULL, 'yyy', '2018-03-02 08:38:50', '2018-03-02 08:38:50'),
(10, 1, '2018-03-03', '00:20:00', '-6.3460008', '106.7438152', '14:50:00', '-6.3460098', '106.7438272', 'ks', '2018-03-02 17:20:46', '2018-03-03 07:50:42'),
(11, 1, '2018-03-06', '12:50:00', '-6.3459822', '106.7438148', '14:36:00', '-6.3459759', '106.7438081', 'ddd', '2018-03-06 05:50:25', '2018-03-06 07:36:30'),
(12, 1, '2018-03-07', '12:42:00', '-6.3459751', '106.7438065', '15:05:00', '-6.3460105', '106.7438265', 'j', '2018-03-07 05:42:03', '2018-03-07 08:05:33'),
(13, 1, '2018-03-08', '12:03:00', '-6.3460005', '106.7438149', '12:27:00', '-6.3460076', '106.7438231', 'y', '2018-03-08 05:03:19', '2018-03-08 05:27:19'),
(14, 1, '2018-03-09', '12:25:00', '-6.345963', '106.7438144', '15:11:00', '-6.3459911', '106.7438122', 'j', '2018-03-09 05:25:22', '2018-03-09 08:11:13');

-- --------------------------------------------------------

--
-- Table structure for table `daily_mom`
--

CREATE TABLE `daily_mom` (
  `id_daily_mom` int(11) NOT NULL,
  `id_pengawas` int(11) NOT NULL,
  `date_mom` date NOT NULL,
  `common_condition` text NOT NULL,
  `problem` text NOT NULL,
  `attendance` text NOT NULL,
  `report` text NOT NULL,
  `to_do_list` text NOT NULL,
  `approved` int(1) NOT NULL DEFAULT 0,
  `approved_by` int(11) DEFAULT NULL,
  `approval_note` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_mom`
--

INSERT INTO `daily_mom` (`id_daily_mom`, `id_pengawas`, `date_mom`, `common_condition`, `problem`, `attendance`, `report`, `to_do_list`, `approved`, `approved_by`, `approval_note`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-03-28', 'tes ya', 'tess', 'yaaa', 'kjgjhgjhfjhfgh', 'hhmmm', 1, 2, 'zzzz', '2018-03-28 01:38:16', '2018-07-16 20:44:43'),
(2, 1, '2018-03-28', 'tes ya', 'tess', 'yaaa', 'kjgjhgjhfjhfgh', 'hhmmm', 0, NULL, NULL, '2018-03-28 04:52:09', '2018-03-28 04:52:09'),
(3, 1, '2018-03-28', 'aa', 'a', 'a', 'a', 'a', 3, 2, 'coba', '2018-03-28 08:08:47', '2018-07-17 02:50:13'),
(4, 1, '2018-03-28', 'z', 'sa', 'a', 'a', 'z', 3, 2, 'coba', '2018-03-28 08:17:50', '2018-07-17 02:49:04'),
(5, 1, '2018-04-05', 'x', 'x', 'x', 'x', 'z', 0, NULL, NULL, '2018-04-05 06:28:33', '2018-04-05 06:28:33'),
(6, 1, '2018-04-05', 'g', 'x', 'g', 'g', 'g', 0, NULL, NULL, '2018-04-05 06:35:09', '2018-04-05 06:35:09'),
(7, 1, '2018-04-19', 'd', 'd', 's', 'z', 'szz', 0, NULL, NULL, '2018-04-19 09:06:51', '2018-04-19 09:06:51');

-- --------------------------------------------------------

--
-- Table structure for table `daily_problem`
--

CREATE TABLE `daily_problem` (
  `id_daily_problem` int(11) NOT NULL,
  `id_pengawas` int(11) NOT NULL,
  `date_problem` date NOT NULL,
  `title_problem` varchar(255) NOT NULL,
  `desc_problem` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_problem`
--

INSERT INTO `daily_problem` (`id_daily_problem`, `id_pengawas`, `date_problem`, `title_problem`, `desc_problem`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-03-28', 'dddd', 'ffghgfh gfgfhgf dgdhd gdfgdf', '2018-03-29 02:54:49', '2018-03-29 02:54:49'),
(2, 1, '2018-04-19', 'f', 'd', '2018-04-19 09:06:58', '2018-04-19 09:06:58');

-- --------------------------------------------------------

--
-- Table structure for table `daily_report`
--

CREATE TABLE `daily_report` (
  `id_daily_report` int(11) NOT NULL,
  `id_pengawas` int(11) NOT NULL,
  `date_report` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `exterior_img` varchar(255) NOT NULL,
  `exterior_text` text DEFAULT NULL,
  `interior_img` varchar(255) NOT NULL,
  `interior_text` text DEFAULT NULL,
  `approved` int(1) NOT NULL DEFAULT 0,
  `approved_by` int(11) DEFAULT NULL,
  `approval_note` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_report`
--

INSERT INTO `daily_report` (`id_daily_report`, `id_pengawas`, `date_report`, `start_time`, `end_time`, `exterior_img`, `exterior_text`, `interior_img`, `interior_text`, `approved`, `approved_by`, `approval_note`, `created_at`, `updated_at`) VALUES
(10, 1, '2018-02-06', '08:22:00', '19:22:00', 'uploads/dailyreport/20180206-exterior-1517952296.jpeg', NULL, 'uploads/dailyreport/20180206-interior-1517952296.jpeg', NULL, 0, NULL, NULL, '2018-02-06 21:24:56', '2018-02-06 21:24:56'),
(11, 1, '2018-02-06', '13:45:00', '13:45:00', 'uploads/dailyreport/20180208-exterior-1518110859.jpeg', 'wkkwwkwkwk', 'uploads/dailyreport/20180208-interior-1518110859.jpeg', NULL, 1, NULL, NULL, '2018-02-08 17:27:39', '2018-02-08 17:27:39'),
(12, 1, '2018-02-06', '13:45:00', '13:45:00', 'uploads/dailyreport/20180209-exterior-1518165389.jpeg', 'wkkwwkwkwk', 'uploads/dailyreport/20180209-interior-1518165389.jpeg', NULL, 0, NULL, NULL, '2018-02-09 08:36:29', '2018-02-09 08:36:29'),
(13, 1, '2018-02-06', '08:22:00', '19:22:00', 'uploads/dailyreport/20180212-exterior-1518428610.jpeg', 'dddddd', 'uploads/dailyreport/20180212-interior-1518428610.jpeg', 'eeee', 0, NULL, NULL, '2018-02-12 09:43:30', '2018-02-12 09:43:30'),
(14, 1, '2018-02-06', '08:22:00', '19:22:00', 'uploads/dailyreport/20180212-exterior-1518430756.jpeg', 'dddddd', 'uploads/dailyreport/20180212-interior-1518430756.jpeg', 'eeee', 0, NULL, NULL, '2018-02-12 10:19:16', '2018-02-12 10:19:16'),
(15, 1, '2018-02-06', '08:00:00', '16:00:00', 'uploads/dailyreport/20180213-exterior-1518516120.jpeg', 's', 'uploads/dailyreport/20180213-interior-1518516120.jpeg', NULL, 0, NULL, NULL, '2018-02-13 10:02:00', '2018-02-13 10:02:00'),
(16, 1, '2018-02-06', '08:00:00', '16:00:00', 'uploads/dailyreport/20180213-exterior-1518521054.jpeg', 's', 'uploads/dailyreport/20180213-interior-1518521054.jpeg', 's', 0, NULL, NULL, '2018-02-13 11:24:14', '2018-02-13 11:24:14'),
(17, 1, '2018-02-06', '08:00:00', '16:00:00', 'uploads/dailyreport/20180213-exterior-1518521174.jpeg', 'u', 'uploads/dailyreport/20180213-interior-1518521174.jpeg', 'y', 0, NULL, NULL, '2018-02-13 11:26:14', '2018-02-13 11:26:14'),
(18, 1, '2018-02-06', '08:00:00', '16:00:00', 'uploads/dailyreport/20180214-exterior-1518592373.jpeg', 'z', 'uploads/dailyreport/20180214-interior-1518592373.jpeg', 'a', 0, NULL, NULL, '2018-02-14 07:12:53', '2018-02-14 07:12:53'),
(19, 1, '2018-02-23', '08:00:00', '16:00:00', 'uploads/dailyreport/20180222-exterior-1519319566.jpeg', 'z', 'uploads/dailyreport/20180222-interior-1519319566.jpeg', 's', 0, NULL, NULL, '2018-02-22 17:12:46', '2018-02-22 17:12:46'),
(20, 1, '2018-02-23', '08:00:00', '16:00:00', 'uploads/dailyreport/20180222-exterior-1519319912.jpeg', 'z', 'uploads/dailyreport/20180222-interior-1519319912.jpeg', 's', 0, NULL, NULL, '2018-02-22 17:18:32', '2018-02-22 17:18:32'),
(21, 1, '2018-02-26', '08:00:00', '13:00:00', 'uploads/dailyreport/20180226-exterior-1519637298.jpeg', 'hrj', 'uploads/dailyreport/20180226-interior-1519637298.jpeg', 'jdkdk', 0, NULL, NULL, '2018-02-26 09:28:18', '2018-02-26 09:28:18'),
(22, 1, '2018-02-26', '08:00:00', '13:00:00', 'uploads/dailyreport/20180226-exterior-1519654905.jpeg', 'zz', 'uploads/dailyreport/20180226-interior-1519654905.jpeg', 'sss', 0, NULL, NULL, '2018-02-26 14:21:45', '2018-02-26 14:21:45'),
(23, 1, '2018-02-06', '08:22:00', '19:22:00', 'uploads/dailyreport/20180303-exterior-1520063684.jpeg', 'wew', 'uploads/dailyreport/20180303-interior-1520063684.jpeg', 'we', 0, NULL, NULL, '2018-03-03 07:54:44', '2018-03-03 07:54:44'),
(24, 1, '2018-02-06', '08:22:00', '19:22:00', 'uploads/dailyreport/20180303-exterior-1520063703.jpeg', 'wew', 'uploads/dailyreport/20180303-interior-1520063703.jpeg', 'we', 0, NULL, NULL, '2018-03-03 07:55:03', '2018-03-03 07:55:03'),
(25, 1, '2018-02-06', '08:22:00', '19:22:00', 'uploads/dailyreport/20180305-exterior-1520270976.jpeg', 'wew', 'uploads/dailyreport/20180305-interior-1520270976.jpeg', 'we', 0, NULL, NULL, '2018-03-05 17:29:36', '2018-03-05 17:29:36'),
(26, 1, '2018-03-06', '08:00:00', '13:00:00', 'uploads/dailyreport/20180305-exterior-1520271590.jpeg', 'z', 'uploads/dailyreport/20180305-interior-1520271591.jpeg', 's', 0, NULL, NULL, '2018-03-05 17:39:51', '2018-03-05 17:39:51'),
(27, 1, '2018-03-06', '08:00:00', '14:00:00', 'uploads/dailyreport/20180305-exterior-1520271851.jpeg', 's', 'uploads/dailyreport/20180305-interior-1520271853.jpeg', 's', 0, NULL, NULL, '2018-03-05 17:44:13', '2018-03-05 17:44:13'),
(28, 1, '2018-03-07', '08:00:00', '13:00:00', 'uploads/dailyreport/20180307-exterior-1520402582.jpeg', 'd', 'uploads/dailyreport/20180307-interior-1520402583.jpeg', 's', 0, NULL, NULL, '2018-03-07 06:03:03', '2018-03-07 06:03:03'),
(29, 1, '2018-03-09', '08:00:00', '12:00:00', 'uploads/dailyreport/20180309-exterior-1520581255.jpeg', 'h', 'uploads/dailyreport/20180309-interior-1520581255.jpeg', 'y', 0, NULL, NULL, '2018-03-09 07:40:55', '2018-03-09 07:40:55'),
(30, 1, '2018-03-09', '08:00:00', '13:00:00', 'uploads/dailyreport/20180309-exterior-1520581936.jpeg', 'd', 'uploads/dailyreport/20180309-interior-1520581936.jpeg', 'd', 0, NULL, NULL, '2018-03-09 07:52:16', '2018-03-09 07:52:16'),
(31, 1, '2018-03-09', '08:00:00', '14:00:00', 'uploads/dailyreport/20180309-exterior-1520582113.jpeg', 's', 'uploads/dailyreport/20180309-interior-1520582115.jpeg', 's', 0, NULL, NULL, '2018-03-09 07:55:15', '2018-03-09 07:55:15'),
(32, 1, '2018-03-09', '08:00:00', '15:00:00', 'uploads/dailyreport/20180309-exterior-1520583022.jpeg', 'x', 'uploads/dailyreport/20180309-interior-1520583024.jpeg', 'd', 0, NULL, NULL, '2018-03-09 08:10:24', '2018-03-09 08:10:24'),
(33, 1, '2018-03-10', '08:00:00', '13:00:00', 'uploads/dailyreport/20180310-exterior-1520686631.jpeg', 'd', 'uploads/dailyreport/20180310-interior-1520686632.jpeg', 'd', 0, NULL, NULL, '2018-03-10 12:57:12', '2018-03-10 12:57:12'),
(34, 1, '2018-03-10', '08:00:00', '14:00:00', 'uploads/dailyreport/20180310-exterior-1520689745.jpeg', 'x', 'uploads/dailyreport/20180310-interior-1520689745.jpeg', 'd', 3, 2, 'hhmm', '2018-03-10 13:49:05', '2018-07-11 03:47:50'),
(35, 1, '2018-03-10', '08:00:00', '15:00:00', 'uploads/dailyreport/20180310-exterior-1520690065.jpeg', 'sss', 'uploads/dailyreport/20180310-interior-1520690065.jpeg', 's', 0, NULL, NULL, '2018-03-10 13:54:25', '2018-03-10 13:54:25'),
(36, 1, '2018-03-10', '08:00:00', '14:00:00', 'uploads/dailyreport/20180310-exterior-1520690154.jpeg', 's', 'uploads/dailyreport/20180310-interior-1520690154.jpeg', 'x', 0, NULL, NULL, '2018-03-10 13:55:54', '2018-03-10 13:55:54'),
(37, 1, '2018-03-10', '08:00:00', '14:00:00', 'uploads/dailyreport/20180310-exterior-1520690222.jpeg', 'x', 'uploads/dailyreport/20180310-interior-1520690222.jpeg', 'x', 0, NULL, NULL, '2018-03-10 13:57:02', '2018-03-10 13:57:02'),
(38, 1, '2018-03-10', '08:00:00', '14:00:00', 'uploads/dailyreport/20180310-exterior-1520691042.jpeg', 'ajbsndkdbndmd', 'uploads/dailyreport/20180310-interior-1520691042.jpeg', 'sjsnd', 0, NULL, NULL, '2018-03-10 14:10:42', '2018-03-10 14:10:42'),
(39, 1, '2018-03-10', '08:00:00', '13:00:00', 'uploads/dailyreport/20180310-exterior-1520692393.jpeg', 'd', 'uploads/dailyreport/20180310-interior-1520692393.jpeg', 'd', 3, 2, NULL, '2018-03-10 14:33:13', '2018-07-11 03:40:51'),
(40, 1, '2018-03-12', '08:00:00', '14:00:00', 'uploads/dailyreport/20180312-exterior-1520854908.jpeg', 's', 'uploads/dailyreport/20180312-interior-1520854908.jpeg', 's', 0, NULL, NULL, '2018-03-12 11:41:48', '2018-03-12 11:41:48'),
(41, 1, '2018-03-13', '08:00:00', '11:00:00', 'uploads/dailyreport/20180313-exterior-1520956077.jpeg', 'nsnssjdknddnkddmndbsidmdmjdb', 'uploads/dailyreport/20180313-interior-1520956077.jpeg', 'n_jsnsbdbdbdbdbbdbdbdbfjfnfnfjofkeoeohe82828te7iebe', 2, 2, 'kurang nih', '2018-03-13 15:47:57', '2018-07-11 03:35:16'),
(42, 2, '2018-03-21', '08:00:00', '12:00:00', 'uploads/dailyreport/20180321-exterior-1521607339.jpeg', 'tes', 'uploads/dailyreport/20180321-interior-1521607339.jpeg', 'es', 1, 2, 'nice', '2018-03-21 04:42:19', '2018-07-11 03:34:40');

-- --------------------------------------------------------

--
-- Table structure for table `daily_report_progress`
--

CREATE TABLE `daily_report_progress` (
  `id_daily_report_progress` int(11) NOT NULL,
  `id_daily_report` int(11) NOT NULL,
  `progress_img` varchar(255) NOT NULL,
  `progress_text` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_report_progress`
--

INSERT INTO `daily_report_progress` (`id_daily_report_progress`, `id_daily_report`, `progress_img`, `progress_text`, `created_at`, `updated_at`) VALUES
(18, 10, 'uploads/dailyreport_progress/20180206-1-1517952296.jpeg', 'tes ya', '2018-02-06 21:24:57', '2018-02-06 21:24:57'),
(19, 10, 'uploads/dailyreport_progress/20180206-2-1517952297.jpeg', NULL, '2018-02-06 21:24:57', '2018-02-06 21:24:57'),
(20, 11, 'uploads/dailyreport_progress/20180208-1-1518110859.jpeg', 'ckckckcck', '2018-02-08 17:27:39', '2018-02-08 17:27:39'),
(21, 11, 'uploads/dailyreport_progress/20180208-2-1518110859.jpeg', NULL, '2018-02-08 17:27:39', '2018-02-08 17:27:39'),
(22, 11, 'uploads/dailyreport_progress/20180208-3-1518110859.jpeg', NULL, '2018-02-08 17:27:39', '2018-02-08 17:27:39'),
(23, 12, 'uploads/dailyreport_progress/20180209-1-1518165389.jpeg', 'ckckckcck', '2018-02-09 08:36:29', '2018-02-09 08:36:29'),
(24, 12, 'uploads/dailyreport_progress/20180209-2-1518165389.jpeg', NULL, '2018-02-09 08:36:29', '2018-02-09 08:36:29'),
(25, 12, 'uploads/dailyreport_progress/20180209-3-1518165389.jpeg', NULL, '2018-02-09 08:36:29', '2018-02-09 08:36:29'),
(26, 13, 'uploads/dailyreport_progress/20180212-1-1518428610.jpeg', 'ddddd', '2018-02-12 09:43:30', '2018-02-12 09:43:30'),
(27, 13, 'uploads/dailyreport_progress/20180212-2-1518428610.jpeg', NULL, '2018-02-12 09:43:30', '2018-02-12 09:43:30'),
(28, 14, 'uploads/dailyreport_progress/20180212-1-1518430756.jpeg', 'ddddd', '2018-02-12 10:19:16', '2018-02-12 10:19:16'),
(29, 15, 'uploads/dailyreport_progress/20180213-1-1518516120.jpeg', 's', '2018-02-13 10:02:00', '2018-02-13 10:02:00'),
(30, 16, 'uploads/dailyreport_progress/20180213-1-1518521054.jpeg', 'd', '2018-02-13 11:24:14', '2018-02-13 11:24:14'),
(31, 17, 'uploads/dailyreport_progress/20180213-1-1518521174.jpeg', 'y', '2018-02-13 11:26:14', '2018-02-13 11:26:14'),
(32, 18, 'uploads/dailyreport_progress/20180214-1-1518592373.jpeg', 'a', '2018-02-14 07:12:53', '2018-02-14 07:12:53'),
(33, 19, 'uploads/dailyreport_progress/20180222-1-1519319566.jpeg', 's', '2018-02-22 17:12:46', '2018-02-22 17:12:46'),
(34, 20, 'uploads/dailyreport_progress/20180222-1-1519319912.jpeg', 'a', '2018-02-22 17:18:32', '2018-02-22 17:18:32'),
(35, 21, 'uploads/dailyreport_progress/20180226-1-1519637298.jpeg', 'bejrjrj', '2018-02-26 09:28:18', '2018-02-26 09:28:18'),
(36, 22, 'uploads/dailyreport_progress/20180226-1-1519654905.jpeg', 'ss', '2018-02-26 14:21:45', '2018-02-26 14:21:45'),
(37, 24, 'uploads/dailyreport_progress/20180303-1-1520063703.jpeg', 'wewe', '2018-03-03 07:55:03', '2018-03-03 07:55:03'),
(38, 25, 'uploads/dailyreport_progress/20180305-1-1520270976.jpeg', 'wewe', '2018-03-05 17:29:36', '2018-03-05 17:29:36'),
(39, 26, 'uploads/dailyreport_progress/20180305-1-1520271591.jpeg', 's', '2018-03-05 17:39:53', '2018-03-05 17:39:53'),
(40, 27, 'uploads/dailyreport_progress/20180305-1-1520271853.jpeg', 's', '2018-03-05 17:44:13', '2018-03-05 17:44:13'),
(41, 28, 'uploads/dailyreport_progress/20180307-1-1520402583.jpeg', 's', '2018-03-07 06:03:04', '2018-03-07 06:03:04'),
(42, 29, 'uploads/dailyreport_progress/20180309-1-1520581255.jpeg', 'y', '2018-03-09 07:40:55', '2018-03-09 07:40:55'),
(43, 30, 'uploads/dailyreport_progress/20180309-1-1520581936.jpeg', 'd', '2018-03-09 07:52:16', '2018-03-09 07:52:16'),
(44, 31, 'uploads/dailyreport_progress/20180309-1-1520582115.jpeg', 's', '2018-03-09 07:55:15', '2018-03-09 07:55:15'),
(45, 32, 'uploads/dailyreport_progress/20180309-1-1520583024.jpeg', 's', '2018-03-09 08:10:24', '2018-03-09 08:10:24'),
(46, 33, 'uploads/dailyreport_progress/20180310-1-1520686632.jpeg', 'd', '2018-03-10 12:57:12', '2018-03-10 12:57:12'),
(47, 34, 'uploads/dailyreport_progress/20180310-1-1520689745.jpeg', 'd', '2018-03-10 13:49:05', '2018-03-10 13:49:05'),
(48, 35, 'uploads/dailyreport_progress/20180310-1-1520690065.jpeg', 's', '2018-03-10 13:54:26', '2018-03-10 13:54:26'),
(49, 36, 'uploads/dailyreport_progress/20180310-1-1520690154.jpeg', 'd', '2018-03-10 13:55:54', '2018-03-10 13:55:54'),
(50, 37, 'uploads/dailyreport_progress/20180310-1-1520690222.jpeg', 'd', '2018-03-10 13:57:02', '2018-03-10 13:57:02'),
(51, 38, 'uploads/dailyreport_progress/20180310-1-1520691042.jpeg', 'usbsnsmdkdk', '2018-03-10 14:10:42', '2018-03-10 14:10:42'),
(52, 39, 'uploads/dailyreport_progress/20180310-1-1520692393.jpeg', 'd', '2018-03-10 14:33:13', '2018-03-10 14:33:13'),
(53, 40, 'uploads/dailyreport_progress/20180312-1-1520854908.jpeg', 's', '2018-03-12 11:41:48', '2018-03-12 11:41:48'),
(54, 41, 'uploads/dailyreport_progress/20180313-1-1520956077.jpeg', 'sienwmwkebbejeieneo28y2n2kejennekenerbnrjejebebwjwjwbwnwjwjw', '2018-03-13 15:47:57', '2018-03-13 15:47:57'),
(55, 42, 'uploads/dailyreport_progress/20180321-1-1521607339.jpeg', 'tes', '2018-03-21 04:42:19', '2018-03-21 04:42:19');

-- --------------------------------------------------------

--
-- Table structure for table `dealer`
--

CREATE TABLE `dealer` (
  `id_dealer` int(11) NOT NULL,
  `nama_dealer` varchar(150) NOT NULL,
  `latitude_dealer` varchar(150) NOT NULL,
  `longitude_dealer` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dealer`
--

INSERT INTO `dealer` (`id_dealer`, `nama_dealer`, `latitude_dealer`, `longitude_dealer`, `created_at`, `updated_at`) VALUES
(1, 'PT. Dealer Sejahtera 1', '-6.242423', '106.844445', '2018-02-01 00:00:00', '2018-02-01 00:00:00'),
(2, 'PT. Dealer Sejahtera 2', '-6.242423', '106.844445', '2018-02-01 00:00:00', '2018-02-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `kontraktor`
--

CREATE TABLE `kontraktor` (
  `id_kontraktor` int(11) NOT NULL,
  `nama_kontraktor` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontraktor`
--

INSERT INTO `kontraktor` (`id_kontraktor`, `nama_kontraktor`, `created_at`, `updated_at`) VALUES
(1, 'Kontraktor 1', '2018-02-01 00:00:00', '2018-02-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `main_dealer`
--

CREATE TABLE `main_dealer` (
  `id_main_dealer` int(11) NOT NULL,
  `nama_main_dealer` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_dealer`
--

INSERT INTO `main_dealer` (`id_main_dealer`, `nama_main_dealer`, `created_at`, `updated_at`) VALUES
(1, 'Main Dealer 1', '2018-02-01 00:00:00', '2018-02-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `monthly_report`
--

CREATE TABLE `monthly_report` (
  `id_monthly_report` int(11) NOT NULL,
  `id_pengawas` int(11) NOT NULL,
  `date_report` date NOT NULL,
  `exterior_siang` varchar(255) DEFAULT NULL,
  `exterior_siang_text` text DEFAULT NULL,
  `exterior_malam` varchar(255) DEFAULT NULL,
  `exterior_malam_text` text DEFAULT NULL,
  `sales_finance_front` varchar(255) DEFAULT NULL,
  `sales_finance_front_text` text DEFAULT NULL,
  `parts_front` varchar(255) DEFAULT NULL,
  `parts_front_text` text DEFAULT NULL,
  `service_front` varchar(255) DEFAULT NULL,
  `service_front_text` text DEFAULT NULL,
  `advisor_desk` varchar(255) DEFAULT NULL,
  `advisor_desk_text` text DEFAULT NULL,
  `hero_stage` varchar(255) DEFAULT NULL,
  `hero_stage_text` text DEFAULT NULL,
  `isolate_stage_1` varchar(255) DEFAULT NULL,
  `isolate_stage_1_text` text DEFAULT NULL,
  `isolate_stage_2` varchar(255) DEFAULT NULL,
  `isolate_stage_2_text` text DEFAULT NULL,
  `wall_system_display` varchar(255) DEFAULT NULL,
  `wall_system_display_text` text DEFAULT NULL,
  `glass_showcase_1` varchar(255) DEFAULT NULL,
  `glass_showcase_1_text` text DEFAULT NULL,
  `glass_showcase_2` varchar(255) DEFAULT NULL,
  `glass_showcase_2_text` text DEFAULT NULL,
  `spec_stand_1` varchar(255) DEFAULT NULL,
  `spec_stand_1_text` text DEFAULT NULL,
  `spec_stand_2` varchar(255) DEFAULT NULL,
  `spec_stand_2_text` text DEFAULT NULL,
  `spec_stand_3` varchar(255) DEFAULT NULL,
  `spec_stand_3_text` text DEFAULT NULL,
  `spec_stand_4` varchar(255) DEFAULT NULL,
  `spec_stand_4_text` text DEFAULT NULL,
  `spec_stand_5` varchar(255) DEFAULT NULL,
  `spec_stand_5_text` text DEFAULT NULL,
  `brochure_rack` varchar(255) DEFAULT NULL,
  `brochure_rack_text` text DEFAULT NULL,
  `internet_counter` varchar(255) DEFAULT NULL,
  `internet_counter_text` text DEFAULT NULL,
  `ci_logo` varchar(255) DEFAULT NULL,
  `ci_logo_text` text DEFAULT NULL,
  `direction_bengkel` varchar(255) DEFAULT NULL,
  `direction_bengkel_text` text DEFAULT NULL,
  `direction_penjualan` varchar(255) DEFAULT NULL,
  `direction_penjualan_text` text DEFAULT NULL,
  `direction_ruang_tunggu` varchar(255) DEFAULT NULL,
  `direction_ruang_tunggu_text` text DEFAULT NULL,
  `direction_toilet` varchar(255) DEFAULT NULL,
  `direction_toilet_text` text DEFAULT NULL,
  `direction_pit` varchar(255) DEFAULT NULL,
  `direction_pit_text` text DEFAULT NULL,
  `direction_kasir` varchar(255) DEFAULT NULL,
  `direction_kasir_text` text DEFAULT NULL,
  `direction_toilet_wall` varchar(255) DEFAULT NULL,
  `direction_toilet_wall_text` text DEFAULT NULL,
  `direction_smoking` varchar(255) DEFAULT NULL,
  `direction_smoking_text` text DEFAULT NULL,
  `panel_poster_display` varchar(255) DEFAULT NULL,
  `panel_poster_display_text` text DEFAULT NULL,
  `poster_display` varchar(255) DEFAULT NULL,
  `poster_display_text` text DEFAULT NULL,
  `spareparts_display` varchar(255) DEFAULT NULL,
  `spareparts_display_text` text DEFAULT NULL,
  `helmet_rack` varchar(255) DEFAULT NULL,
  `helmet_rack_text` text DEFAULT NULL,
  `magazine_rack` varchar(255) DEFAULT NULL,
  `magazine_rack_text` text DEFAULT NULL,
  `banner_stand` varchar(255) DEFAULT NULL,
  `banner_stand_text` text DEFAULT NULL,
  `island_display` varchar(255) DEFAULT NULL,
  `island_display_text` text DEFAULT NULL,
  `approved` int(1) NOT NULL DEFAULT 0,
  `approved_by` int(11) DEFAULT NULL,
  `approval_note` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `monthly_report`
--

INSERT INTO `monthly_report` (`id_monthly_report`, `id_pengawas`, `date_report`, `exterior_siang`, `exterior_siang_text`, `exterior_malam`, `exterior_malam_text`, `sales_finance_front`, `sales_finance_front_text`, `parts_front`, `parts_front_text`, `service_front`, `service_front_text`, `advisor_desk`, `advisor_desk_text`, `hero_stage`, `hero_stage_text`, `isolate_stage_1`, `isolate_stage_1_text`, `isolate_stage_2`, `isolate_stage_2_text`, `wall_system_display`, `wall_system_display_text`, `glass_showcase_1`, `glass_showcase_1_text`, `glass_showcase_2`, `glass_showcase_2_text`, `spec_stand_1`, `spec_stand_1_text`, `spec_stand_2`, `spec_stand_2_text`, `spec_stand_3`, `spec_stand_3_text`, `spec_stand_4`, `spec_stand_4_text`, `spec_stand_5`, `spec_stand_5_text`, `brochure_rack`, `brochure_rack_text`, `internet_counter`, `internet_counter_text`, `ci_logo`, `ci_logo_text`, `direction_bengkel`, `direction_bengkel_text`, `direction_penjualan`, `direction_penjualan_text`, `direction_ruang_tunggu`, `direction_ruang_tunggu_text`, `direction_toilet`, `direction_toilet_text`, `direction_pit`, `direction_pit_text`, `direction_kasir`, `direction_kasir_text`, `direction_toilet_wall`, `direction_toilet_wall_text`, `direction_smoking`, `direction_smoking_text`, `panel_poster_display`, `panel_poster_display_text`, `poster_display`, `poster_display_text`, `spareparts_display`, `spareparts_display_text`, `helmet_rack`, `helmet_rack_text`, `magazine_rack`, `magazine_rack_text`, `banner_stand`, `banner_stand_text`, `island_display`, `island_display_text`, `approved`, `approved_by`, `approval_note`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-02-05', 'uploads/monthlyreport/20180214-exterior_siang-1518591305.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2018-02-14 06:55:05', '2018-02-14 06:55:05'),
(2, 1, '2018-02-05', 'uploads/monthlyreport/20180223-exterior_siang-1519402792.jpeg', 'fdfd', 'uploads/monthlyreport/20180223-exterior_malam-1519402792.jpeg', NULL, 'uploads/monthlyreport/20180223-sales_finance_front-1519402794.jpeg', 'sdsd', 'uploads/monthlyreport/20180223-parts_front-1519402794.jpeg', 'sds', 'uploads/monthlyreport/20180223-service_front-1519402794.jpeg', 'sds', 'uploads/monthlyreport/20180223-advisor_desk-1519402794.jpeg', 'sdsd', 'uploads/monthlyreport/20180223-hero_stage-1519402794.jpeg', 'sdsd', 'uploads/monthlyreport/20180223-isolate_stage_1-1519402794.jpeg', 'sdsds', 'uploads/monthlyreport/20180223-isolate_stage_2-1519402794.jpeg', 'sds', 'uploads/monthlyreport/20180223-wall_system_display-1519402794.jpeg', 'sdsd', 'uploads/monthlyreport/20180223-glass_showcase_1-1519402794.jpeg', 'sdsds', 'uploads/monthlyreport/20180223-glass_showcase_2-1519402794.jpeg', 'sdsds', 'uploads/monthlyreport/20180223-spec_stand_1-1519402794.jpeg', 'dsfsdf', 'uploads/monthlyreport/20180223-spec_stand_2-1519402794.jpeg', 'sdsdsd', 'uploads/monthlyreport/20180223-spec_stand_3-1519402794.jpeg', 'ddfd', 'uploads/monthlyreport/20180223-spec_stand_4-1519402794.jpeg', 'dfdf', 'uploads/monthlyreport/20180223-spec_stand_5-1519402794.jpeg', 'sdsds', 'uploads/monthlyreport/20180223-brochure_rack-1519402794.jpeg', 'sdsds', 'uploads/monthlyreport/20180223-internet_counter-1519402794.jpeg', 'sdsds', NULL, 'sdsds', 'uploads/monthlyreport/20180223-direction_bengkel-1519402794.jpeg', 'sdsds', NULL, 'sdsds', NULL, 'sdsds', NULL, 'sdsdsd', NULL, 'sdsds', NULL, 'sdsdsd', NULL, 'sdsds', NULL, 'sdsds', NULL, 'sdsds', NULL, 'sdsds', NULL, 'sdsds', NULL, 'sdsdsd', NULL, 'sdsd', NULL, 'sdsds', NULL, 'sdsdsds', 0, NULL, NULL, '2018-02-23 16:19:54', '2018-02-23 16:19:54'),
(3, 1, '2018-02-05', 'uploads/monthlyreport/20180223-exterior_siang-1519409409.jpeg', 'fdfd', 'uploads/monthlyreport/20180223-exterior_malam-1519409409.jpeg', NULL, 'uploads/monthlyreport/20180223-sales_finance_front-1519409409.jpeg', 'sdsd', 'uploads/monthlyreport/20180223-parts_front-1519409409.jpeg', 'sds', 'uploads/monthlyreport/20180223-service_front-1519409409.jpeg', 'sds', 'uploads/monthlyreport/20180223-advisor_desk-1519409409.jpeg', 'sdsd', 'uploads/monthlyreport/20180223-hero_stage-1519409409.jpeg', 'sdsd', 'uploads/monthlyreport/20180223-isolate_stage_1-1519409409.jpeg', 'sdsds', 'uploads/monthlyreport/20180223-isolate_stage_2-1519409409.jpeg', 'sds', 'uploads/monthlyreport/20180223-wall_system_display-1519409409.jpeg', 'sdsd', 'uploads/monthlyreport/20180223-glass_showcase_1-1519409409.jpeg', 'sdsds', 'uploads/monthlyreport/20180223-glass_showcase_2-1519409409.jpeg', 'sdsds', 'uploads/monthlyreport/20180223-spec_stand_1-1519409409.jpeg', 'dsfsdf', 'uploads/monthlyreport/20180223-spec_stand_2-1519409409.jpeg', 'sdsdsd', 'uploads/monthlyreport/20180223-spec_stand_3-1519409409.jpeg', 'ddfd', 'uploads/monthlyreport/20180223-spec_stand_4-1519409409.jpeg', 'dfdf', 'uploads/monthlyreport/20180223-spec_stand_5-1519409409.jpeg', 'sdsds', 'uploads/monthlyreport/20180223-brochure_rack-1519409409.jpeg', 'sdsds', 'uploads/monthlyreport/20180223-internet_counter-1519409409.jpeg', 'sdsds', 'uploads/monthlyreport/20180223-ci_logo-1519409409.jpeg', 'sdsds', NULL, 'sdsds', NULL, 'sdsds', NULL, 'sdsds', NULL, 'sdsdsd', NULL, 'sdsds', NULL, 'sdsdsd', NULL, 'sdsds', NULL, 'sdsds', NULL, 'sdsds', NULL, 'sdsds', NULL, 'sdsds', NULL, 'sdsdsd', NULL, 'sdsd', NULL, 'sdsds', NULL, 'sdsdsds', 0, NULL, NULL, '2018-02-23 18:10:09', '2018-02-23 18:10:09'),
(4, 1, '2018-02-25', 'uploads/monthlyreport/20180225-exterior_siang-1519577231.jpeg', 'd', 'uploads/monthlyreport/20180225-exterior_malam-1519577231.jpeg', 'd', NULL, 'z', 'uploads/monthlyreport/20180225-parts_front-1519577231.jpeg', 's', 'uploads/monthlyreport/20180225-service_front-1519577231.jpeg', 's', 'uploads/monthlyreport/20180225-advisor_desk-1519577231.jpeg', 's', 'uploads/monthlyreport/20180225-hero_stage-1519577231.jpeg', 's', 'uploads/monthlyreport/20180225-isolate_stage_1-1519577231.jpeg', 'd', 'uploads/monthlyreport/20180225-isolate_stage_2-1519577231.jpeg', 'ddd', 'uploads/monthlyreport/20180225-wall_system_display-1519577231.jpeg', 'z', 'uploads/monthlyreport/20180225-glass_showcase_1-1519577231.jpeg', 'x', 'uploads/monthlyreport/20180225-glass_showcase_2-1519577231.jpeg', 'd', 'uploads/monthlyreport/20180225-spec_stand_1-1519577231.jpeg', 'd', 'uploads/monthlyreport/20180225-spec_stand_2-1519577231.jpeg', 'd', 'uploads/monthlyreport/20180225-spec_stand_3-1519577231.jpeg', 'd', 'uploads/monthlyreport/20180225-spec_stand_4-1519577231.jpeg', 'x', 'uploads/monthlyreport/20180225-spec_stand_5-1519577231.jpeg', 'x', 'uploads/monthlyreport/20180225-brochure_rack-1519577231.jpeg', 'd', 'uploads/monthlyreport/20180225-internet_counter-1519577232.jpeg', 'd', 'uploads/monthlyreport/20180225-ci_logo-1519577232.jpeg', 'd', NULL, 'd', NULL, 'ddd', NULL, 'd', NULL, 'dd', NULL, 'dd', NULL, 's', NULL, 'd', NULL, 'd', NULL, 'd', NULL, 'd', NULL, 's', NULL, 'x', NULL, 'd', NULL, 'd', NULL, 'd', 0, NULL, NULL, '2018-02-25 16:47:12', '2018-02-25 16:47:12'),
(5, 1, '2018-04-19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2018-04-19 08:28:18', '2018-04-19 08:28:18'),
(6, 1, '2018-04-19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2018-04-19 08:41:33', '2018-04-19 08:41:33'),
(7, 1, '2018-04-27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'soosissosodo', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2018-04-27 06:45:16', '2018-04-27 06:45:16');

-- --------------------------------------------------------

--
-- Table structure for table `pengawas`
--

CREATE TABLE `pengawas` (
  `id_pengawas` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_supervisor` int(11) NOT NULL,
  `id_kontraktor` int(11) NOT NULL,
  `id_main_dealer` int(11) NOT NULL,
  `id_dealer` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengawas`
--

INSERT INTO `pengawas` (`id_pengawas`, `id_user`, `id_supervisor`, `id_kontraktor`, `id_main_dealer`, `id_dealer`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 1, 1, 1, '2018-02-01 00:00:00', '2018-02-01 00:00:00'),
(2, 10, 1, 1, 1, 1, '2018-02-19 19:22:27', '2018-02-19 19:22:27'),
(3, 13, 1, 1, 1, 1, '2018-02-19 19:39:59', '2018-02-19 19:39:59'),
(4, 19, 1, 1, 1, 1, '2018-07-24 17:21:37', '2018-07-24 17:21:37');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supervisor`
--

CREATE TABLE `supervisor` (
  `id_supervisor` int(11) NOT NULL,
  `nama_supervisor` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supervisor`
--

INSERT INTO `supervisor` (`id_supervisor`, `nama_supervisor`, `created_at`, `updated_at`) VALUES
(1, 'Supervisor 1', '2018-02-01 00:00:00', '2018-02-01 00:00:00'),
(2, 'Supervisor 2', '2018-02-16 19:07:06', '2018-02-16 19:07:06');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `id_user_category` int(11) NOT NULL,
  `nama_user` varchar(150) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `id_user_category`, `nama_user`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 1, 'User AHM', 'user1', '24c9e15e52afc47c225b757e7bee1f9d', '2018-01-30 00:00:00', '2018-01-30 00:00:00'),
(2, 2, 'User KHS', 'user2', '7e58d63b60197ceb55a1c487989a3720', '2018-01-30 00:00:00', '2018-01-30 00:00:00'),
(3, 3, 'User Pengawas', 'user3', '92877af70a45fd6a2ed7fe81e1236b78', '2018-01-30 00:00:00', '2018-01-30 00:00:00'),
(7, 1, 'ahm 3', 'ahm3', '5d8a3213db806ebe8e001ebd01600291', '2018-02-16 08:53:35', '2018-02-16 08:53:35'),
(8, 1, 'khs 3', 'khs3', 'cf08e9107e2df49f20d7700bce3f9abc', '2018-02-16 14:16:51', '2018-02-16 14:16:51'),
(9, 1, 'ahm 4', 'ahm4', 'a58eb4cc3c6e36193402df0df15f9057', '2018-02-16 17:35:50', '2018-02-16 17:35:50'),
(10, 3, 'Pengawas 3', 'p3', '7bc3ca68769437ce986455407dab2a1f', '2018-02-19 19:22:27', '2018-02-19 19:22:27'),
(11, 1, 'Pengawas 4', 'p4', '13207e3d5722030f6c97d69b4904d39d', '2018-02-19 19:34:50', '2018-02-19 19:34:50'),
(12, 1, 'Pengawas 5', 'p5', 'ed92eff813a02a31a2677be0563a0739', '2018-02-19 19:37:37', '2018-02-19 19:37:37'),
(13, 3, 'Pengawas 6', 'p6', 'c6c27fc98633c82571d75dcb5739bbdf', '2018-02-19 19:39:59', '2018-02-19 19:39:59'),
(14, 1, 'ahm5', 'ahm5', '6bbd2483f05999dcf68ae3227e547f17', '2018-02-21 11:14:15', '2018-02-21 11:14:15'),
(15, 2, 'cobaya1', 'cobaya1', '0cc175b9c0f1b6a831c399e269772661', '2018-02-28 05:49:25', '2018-02-28 05:49:25'),
(16, 2, 'cobaya2', 'cobaya2', '0cc175b9c0f1b6a831c399e269772661', '2018-02-28 05:49:25', '2018-02-28 05:49:25'),
(17, 2, 'Rio', 'Rio', 'd5ed38fdbf28bc4e58be142cf5a17cf5', '2018-07-24 17:21:02', '2018-07-24 17:21:02'),
(18, 1, 'Hario', 'Hario', '9d82b664e2e0c055dbb2c476e59052dd', '2018-07-24 17:21:17', '2018-07-24 17:21:17'),
(19, 3, 'Hario n', 'Harion', 'aba6fec763902edc819b3e17690e24a9', '2018-07-24 17:21:37', '2018-07-24 17:21:37');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'KHS API', 'khsapi@localhost.id', '$2y$10$/APHeKKBgFTr/9BkipADBeWw/MBSJWlm/jprVmuLdrFT5Qc1oxCsG', NULL, '2018-01-29 01:37:35', '2018-01-29 01:37:35');

-- --------------------------------------------------------

--
-- Table structure for table `user_category`
--

CREATE TABLE `user_category` (
  `id_user_category` int(11) NOT NULL,
  `nama_user_category` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_category`
--

INSERT INTO `user_category` (`id_user_category`, `nama_user_category`, `created_at`, `updated_at`) VALUES
(1, 'AHM', '2018-01-30 00:00:00', '2018-01-30 00:00:00'),
(2, 'KHS', '2018-01-30 00:00:00', '2018-01-30 00:00:00'),
(3, 'Pengawas', '2018-01-30 00:00:00', '2018-01-30 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_token`
--

CREATE TABLE `user_token` (
  `id_user_token` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `data_token` varchar(255) NOT NULL,
  `inactive` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_token`
--

INSERT INTO `user_token` (`id_user_token`, `id_user`, `data_token`, `inactive`, `created_at`, `updated_at`) VALUES
(520, 15, '25d1ac5ea1888e67dd549a18601b0cab', 0, '2018-03-21 03:31:41', '2018-03-21 03:31:41'),
(563, 10, 'de83b50ad9414991335bffe9df7388fe', 0, '2018-04-11 06:29:42', '2018-04-11 06:29:42'),
(624, 1, '44ecfa4f207f3426db9154a4ff030310', 1, '2018-07-12 07:11:29', '2018-07-12 07:11:29'),
(632, 2, 'f1fd797d271c884949edd3b7fe7fbd9f', 1, '2018-07-16 19:52:59', '2018-07-16 19:52:59'),
(633, 1, '2bdde456ba0c20448ae943f8f02f6b01', 1, '2018-07-16 20:29:18', '2018-07-16 20:29:18'),
(634, 2, 'f1e050ec3441f9bab5cad62cc5aac977', 1, '2018-07-16 20:37:33', '2018-07-16 20:37:33'),
(635, 2, '466d6dbc0189ec2d456f43b99ea39dcc', 1, '2018-07-22 16:12:17', '2018-07-22 16:12:17'),
(636, 1, '25af79bf472818ac43bad553a831698a', 1, '2018-07-24 17:16:37', '2018-07-24 17:16:37'),
(637, 2, '2e4c16f564b041c1a3e7453af1e62e6a', 1, '2018-07-24 17:19:41', '2018-07-24 17:19:41'),
(638, 1, '735b3de5c6478e2e7a6548fa583a19fc', 1, '2018-07-24 17:19:59', '2018-07-24 17:19:59'),
(639, 1, 'e7364163bdfdfedec360005d0a89db5c', 1, '2018-07-24 17:20:19', '2018-07-24 17:20:19'),
(640, 17, 'ac551d6c4d774ec908d0cf6810e2bc3d', 1, '2018-07-24 17:21:48', '2018-07-24 17:21:48'),
(641, 3, 'd5580aa0345f5a58cb0f070b89619b6f', 1, '2018-07-24 17:22:06', '2018-07-24 17:22:06'),
(642, 1, '233ed885f982f6944f129545fa07670e', 1, '2018-07-24 17:26:43', '2018-07-24 17:26:43'),
(643, 1, '304bff9bd9beeee96f50dc39d7ee284f', 1, '2018-07-24 17:31:01', '2018-07-24 17:31:01'),
(644, 3, '078bc512ad91ee6d8fdcb7ad46af3b45', 1, '2018-07-24 17:32:05', '2018-07-24 17:32:05'),
(645, 2, '2ff168f41c66630d9be071424ebb3218', 1, '2018-07-24 17:42:33', '2018-07-24 17:42:33'),
(646, 3, '5a7175ea4a324c51a113626777dc5cab', 1, '2018-07-24 17:43:23', '2018-07-24 17:43:23'),
(647, 1, 'd492afed7fbaa92ea66d614fdb6def4e', 1, '2018-07-25 01:15:47', '2018-07-25 01:15:47'),
(648, 3, 'a8087093f49ad3ef2f493736b4188bc0', 1, '2018-07-25 01:16:28', '2018-07-25 01:16:28'),
(649, 2, '5aa9ddb68d6f70bab8d13898e4174df3', 1, '2018-07-25 01:20:29', '2018-07-25 01:20:29'),
(650, 3, '85454f3c030f5b835cbee467da093964', 1, '2018-07-25 01:22:58', '2018-07-25 01:22:58'),
(651, 3, 'c6ca258f86cbe159df80341729b69f94', 1, '2018-07-25 01:23:06', '2018-07-25 01:23:06'),
(652, 2, '0b2458407423f2434846e24d97e3c021', 1, '2018-07-25 03:12:55', '2018-07-25 03:12:55'),
(653, 3, 'a847e5bb594d4a4e0ad6b8c53ab07c18', 1, '2018-07-25 03:15:03', '2018-07-25 03:15:03'),
(654, 2, 'd03fbfb390c60c8d35c6ae6ce46ede46', 1, '2018-07-25 03:15:58', '2018-07-25 03:15:58'),
(655, 3, 'a40c305ddf644442b56be679a07c2d6c', 1, '2018-07-25 03:17:16', '2018-07-25 03:17:16'),
(656, 2, 'd26469168a016cc704a84c12a2eed140', 1, '2018-07-25 03:20:43', '2018-07-25 03:20:43'),
(657, 1, 'eeab0d12a527b47cd0b75febea49f051', 1, '2018-07-25 03:23:38', '2018-07-25 03:23:38'),
(658, 3, '3d316d721b9ca6a5198d37e3e040be93', 1, '2018-07-25 03:24:26', '2018-07-25 03:24:26'),
(659, 3, '32159cd345e5417bacf49a11352d0813', 1, '2018-07-30 11:45:18', '2018-07-30 11:45:18'),
(660, 3, 'de21917e938aff07c96aa589900afe02', 1, '2018-08-06 07:06:27', '2018-08-06 07:06:27'),
(661, 2, '2310d4340c4136f5202df6a219709ba6', 1, '2018-08-08 04:43:10', '2018-08-08 04:43:10'),
(662, 3, '215b9946660ccdf83b46510411df20f8', 1, '2018-08-13 07:23:29', '2018-08-13 07:23:29'),
(663, 3, 'ac3225281d657816738f1b4b70e6eac8', 1, '2018-08-15 04:31:36', '2018-08-15 04:31:36'),
(664, 3, 'bc546bee146f75705b73e0b779b9efdc', 1, '2018-08-29 06:38:28', '2018-08-29 06:38:28'),
(665, 1, 'af6f05354bba911b5b4a8b0be306f239', 1, '2018-08-29 08:04:30', '2018-08-29 08:04:30'),
(666, 3, 'ac27ab42f62f42db5f2b6ee32a031ecb', 1, '2018-08-31 01:59:21', '2018-08-31 01:59:21'),
(667, 3, '9d7179947403072e61d5cac06df2ffa2', 1, '2018-09-15 13:30:58', '2018-09-15 13:30:58'),
(668, 3, '6de790a4d4956a7d8a856b164dbfda13', 1, '2018-09-15 13:35:53', '2018-09-15 13:35:53'),
(669, 3, '3c5a22decf97dd6f9a584b4a80f3dda4', 1, '2018-09-15 13:43:40', '2018-09-15 13:43:40'),
(670, 3, '48273dc0d206d54c6a7bfbea27e276b4', 1, '2018-09-15 14:49:21', '2018-09-15 14:49:21'),
(671, 2, 'c1592627c187e401fdf4fd119b01d17d', 1, '2018-09-16 05:08:26', '2018-09-16 05:08:26'),
(672, 3, '1d1ea26a74a13036c17e3e2b87fc8cc5', 1, '2018-09-16 05:27:23', '2018-09-16 05:27:23'),
(673, 2, '69e3219aec3a570fa975bb1025f590aa', 1, '2018-09-16 05:30:55', '2018-09-16 05:30:55'),
(674, 3, '87e07cca07dc3aef6bb77d5ae84d91ae', 1, '2018-09-16 05:43:04', '2018-09-16 05:43:04'),
(675, 3, '1b2e998c1372e4c2f7bfc7d11e0246c4', 1, '2018-09-16 15:26:25', '2018-09-16 15:26:25'),
(676, 3, '0b6655bc4f39e2cfade1a6ea08190f8f', 1, '2018-09-20 15:50:57', '2018-09-20 15:50:57'),
(677, 3, '0b606b132bc0431944fab10d0f34f6f5', 1, '2018-09-20 16:01:31', '2018-09-20 16:01:31'),
(678, 3, '2f3e969608e0311c3d60521fbc72ef47', 1, '2018-09-21 15:36:21', '2018-09-21 15:36:21'),
(679, 3, 'ebd37fbe3966ae3fe195df9f9d423374', 1, '2018-09-22 06:27:17', '2018-09-22 06:27:17'),
(680, 3, '90d18b72ed98cde4004caff20dd3bcf1', 1, '2018-09-22 06:39:28', '2018-09-22 06:39:28'),
(681, 1, '12866c8e63803f55a12342b0da112219', 1, '2018-09-22 06:44:47', '2018-09-22 06:44:47'),
(682, 3, 'f8472e95732a61aa05a385f0e4d29dce', 1, '2018-09-22 06:45:12', '2018-09-22 06:45:12'),
(683, 3, '568be928d2516cfcb6050411406dcff2', 1, '2018-09-25 04:18:26', '2018-09-25 04:18:26'),
(684, 7, '0d6605db1a2cc45c57d8661e1cd019e9', 0, '2019-02-21 06:47:43', '2019-02-21 06:47:43'),
(685, 1, 'a3b26f4ddb741b27ef6964218545d1fa', 1, '2019-02-21 06:48:41', '2019-02-21 06:48:41'),
(686, 1, '1e70a5ade59192abc0d68854207190c6', 0, '2019-02-21 13:03:43', '2019-02-21 13:03:43');

-- --------------------------------------------------------

--
-- Table structure for table `weekly_item`
--

CREATE TABLE `weekly_item` (
  `id_weekly_item` int(11) NOT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `nama_item` varchar(255) NOT NULL,
  `unit_item` varchar(30) DEFAULT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weekly_item`
--

INSERT INTO `weekly_item` (`id_weekly_item`, `id_parent`, `nama_item`, `unit_item`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Pekerjaan Mobilisasi', '', 0, '2018-02-25 03:16:54', '2018-02-25 03:16:54'),
(2, NULL, 'Pekerjaan Serah Terima Barang', '', 0, '2018-02-25 03:18:28', '2018-02-25 03:18:28'),
(3, NULL, 'aaa', '', 1, '2018-02-25 04:52:01', '2018-02-25 04:52:01'),
(4, NULL, 'PEKERJAAN STRUKTUR EKSTERIOR', '', 0, '2018-02-25 21:57:47', '2018-02-25 21:57:47'),
(5, NULL, 'PEKERJAAN PERKUATAN UNTUK BANGUNAN 1 LANTAI', '', 0, '2018-02-25 21:58:25', '2018-02-25 21:58:25'),
(6, NULL, 'PEMASANGAN PENUTUP BELAKANG FACADE', '', 0, '2018-02-25 22:00:21', '2018-02-25 22:00:21'),
(7, NULL, 'PEMASANGAN PENUTUP BELAKANG FACADE', '', 0, '2018-02-25 22:01:04', '2018-02-25 22:01:04'),
(8, NULL, 'PEMASANGAN PENUTUP BELAKANG FACADE', '', 0, '2018-02-25 22:01:35', '2018-02-25 22:01:35'),
(9, NULL, 'INSTALASI LISTRIK', '', 0, '2018-02-25 22:02:06', '2018-02-25 22:02:06'),
(10, NULL, 'JASA PENGURUSAN IZIN REKLAME', '', 0, '2018-02-25 22:02:38', '2018-02-25 22:02:38'),
(12, 9, 'Instalasi Neon Light Box Facade', '', 0, '2018-03-09 01:53:37', '2018-03-09 01:53:37'),
(13, 9, 'Instalasi LED untuk penerangan Nama Dealer', '', 0, '2018-03-09 01:56:16', '2018-03-09 01:56:16'),
(14, 5, 'a. Pekerjaan Pondasi Tanah Normal', NULL, 0, '2018-03-09 02:00:09', '2018-03-09 02:00:09'),
(15, 14, '- Pekerjaan galian tanah', 'm3', 0, '2018-03-09 02:00:26', '2018-03-09 02:00:26'),
(16, 4, 'Semua pekerjaan', NULL, 0, '2018-04-11 15:30:42', '2018-04-11 15:30:42'),
(17, 4, 'Semua pekerjaan', NULL, 0, '2018-04-11 15:30:42', '2018-04-11 15:30:42'),
(18, 4, 'a. Pekerjaan Rangka Utama', NULL, 0, '2018-04-11 15:31:31', '2018-04-11 15:31:31'),
(19, 18, '- Rangka Vertikal', 'kg', 0, '2018-04-11 15:32:39', '2018-04-11 15:32:39'),
(20, 19, 'Rangka Horizontal', '162.62', 0, '2018-04-11 15:56:24', '2018-04-11 15:56:24'),
(21, 18, 'Rangka Horizontal', '162.62', 0, '2018-04-11 15:58:17', '2018-04-11 15:58:17'),
(22, 4, 'b.  Pekerjaan Rangka Sekunder, Diagonal', NULL, 0, '2018-04-11 15:59:38', '2018-04-11 15:59:38'),
(23, 22, '-  Rangka Diagonal Cremona', '203.74', 0, '2018-04-11 15:59:56', '2018-04-11 15:59:56'),
(24, 22, '-  Rangka Deep Cremona', '115.73', 0, '2018-04-11 16:00:11', '2018-04-11 16:00:11'),
(25, 22, '-  Rangka Vertikal', '16.46', 0, '2018-04-11 16:00:25', '2018-04-11 16:00:25'),
(26, 22, '-  Rangka Horisontal', '595.99', 0, '2018-04-11 16:00:41', '2018-04-11 16:00:41'),
(27, 4, 'c.   Bracket', 'kg', 0, '2018-04-11 16:01:43', '2018-04-11 16:01:43'),
(28, 4, 'd.  Mur & Baut (antar rangka)', NULL, 0, '2018-04-11 16:02:38', '2018-04-11 16:02:38'),
(29, 28, '-   M8', 'pcs', 0, '2018-04-11 16:02:55', '2018-04-11 16:02:55'),
(30, 28, '-  M10', 'pcs', 0, '2018-04-11 16:03:10', '2018-04-11 16:03:10'),
(31, 4, 'e.   Dyna Bolt (ke Dinding Bangunan)', NULL, 0, '2018-04-11 16:04:34', '2018-04-11 16:04:34'),
(32, 31, '-  M12', 'pcs', 0, '2018-04-11 16:04:52', '2018-04-11 16:04:52'),
(33, 31, '-  M16', 'pcs', 0, '2018-04-11 16:05:08', '2018-04-11 16:05:08'),
(34, 14, '-  Pondasi beton bertulang', 'm3', 0, '2018-04-11 16:05:47', '2018-04-11 16:05:47'),
(35, 14, '-  Anchor Bolt', 'bh', 0, '2018-04-11 16:06:03', '2018-04-11 16:06:03'),
(36, 14, '-  Grouting', 'm2', 0, '2018-04-11 16:06:20', '2018-04-11 16:06:20'),
(37, 14, '- Pekerjaan anti rayap metode suntik atau semprot Chemical Barrier System (Repellent)', 'm\'', 0, '2018-04-11 16:06:37', '2018-04-11 16:06:37'),
(38, 14, '-  Stiffner', 'kg', 0, '2018-04-11 16:07:03', '2018-04-11 16:07:03'),
(39, 14, '-  Pedestal', 'm3', 0, '2018-04-11 16:07:25', '2018-04-11 16:07:25'),
(40, 14, '-  Base Plate', 'kg', 0, '2018-04-11 16:07:52', '2018-04-11 16:07:52'),
(41, 5, 'b.   Pekerjaan Pondasi Tanah Keras', NULL, 0, '2018-04-11 16:08:21', '2018-04-11 16:08:21'),
(42, 41, '-  Pekerjaan galian tanah', 'm3', 0, '2018-04-11 16:08:42', '2018-04-11 16:08:42'),
(43, 41, '-  Pasir alas dipadatkan', 'm3', 0, '2018-04-11 16:08:57', '2018-04-11 16:08:57'),
(44, 41, '-  Lantai Kerja Beton', 'm3', 0, '2018-04-11 16:10:02', '2018-04-11 16:10:02'),
(45, 41, '-  Pondasi beton bertulang', 'm3', 0, '2018-04-11 16:10:24', '2018-04-11 16:10:24'),
(46, 41, '-  Plat baja dasar tiang & stiffner', 'kg', 0, '2018-04-11 16:10:36', '2018-04-11 16:10:36'),
(47, 41, '-  Anchor Bolt', 'bh', 0, '2018-04-11 16:10:54', '2018-04-11 16:10:54'),
(48, 41, '-  Grouting', 'm2', 0, '2018-04-11 16:11:08', '2018-04-11 16:11:08'),
(49, 41, '-  Pekerjaan anti rayap metode suntik atau semprot Chemical Barrier System (Repellent)', 'm\'', 0, '2018-04-11 16:11:20', '2018-04-11 16:11:20'),
(50, 41, '-  Stiffner', 'kg', 0, '2018-04-11 16:11:31', '2018-04-11 16:11:31'),
(51, 41, '-  Pedestal', 'm3', 0, '2018-04-11 16:11:50', '2018-04-11 16:11:50'),
(52, 41, '-  Base Plate', 'kg', 0, '2018-04-11 16:11:59', '2018-04-11 16:11:59'),
(53, 5, 'c.   Pekerjaan Pondasi Tanah Gambut', NULL, 0, '2018-04-11 16:12:40', '2018-04-11 16:12:40'),
(54, 53, '-  Pekerjaan galian tanah', 'm3', 0, '2018-04-11 16:13:03', '2018-04-11 16:13:03'),
(55, 53, '-  Tanah datang dipadatkan', 'm3', 0, '2018-04-11 16:13:18', '2018-04-11 16:13:18'),
(56, 53, '-  Cerucuk penahan tanah', 'btg', 0, '2018-04-11 16:13:31', '2018-04-11 16:13:31'),
(57, 53, '-  Cerucuk dasar pondasi', 'btg', 0, '2018-04-11 16:13:48', '2018-04-11 16:13:48'),
(58, 53, '-  Pondasi beton bertulang', 'm3', 0, '2018-04-11 16:14:04', '2018-04-11 16:14:04'),
(59, 53, '-  Pasir alas dipadatkan', 'm3', 0, '2018-04-11 16:14:20', '2018-04-11 16:14:20'),
(60, 53, '-  Sirtu dipadatkan', 'm3', 0, '2018-04-11 16:14:39', '2018-04-11 16:14:39'),
(61, 53, '-  Lantai Kerja Beton', 'm3', 0, '2018-04-11 16:14:53', '2018-04-11 16:14:53'),
(62, 53, '-  Anchor Bolt', 'bh', 0, '2018-04-11 16:15:04', '2018-04-11 16:15:04'),
(63, 53, '-  Grouting', 'm2', 0, '2018-04-11 16:15:18', '2018-04-11 16:15:18'),
(64, 53, '-  Pekerjaan anti rayap metode suntik atau semprot Chemical Barrier System (Repellent)', 'm\'', 0, '2018-04-11 16:15:31', '2018-04-11 16:15:31'),
(65, 53, '-  Plat baja dasar tiang & Stiffner', 'kg', 0, '2018-04-11 16:15:49', '2018-04-11 16:15:49'),
(66, 53, '-  Pedestal', 'm3', 0, '2018-04-11 16:16:00', '2018-04-11 16:16:00'),
(67, 53, '-  Base Plate', 'kg', 0, '2018-04-11 16:16:18', '2018-04-11 16:16:18'),
(68, 5, 'd. Cremona Tiang Utama', NULL, 0, '2018-04-11 16:16:37', '2018-04-11 16:16:37'),
(69, 68, '- Rangka Utama', NULL, 0, '2018-04-11 16:16:56', '2018-04-11 16:16:56'),
(70, 69, 'i.  Rangka Vertikal', 'kg', 0, '2018-04-11 16:17:15', '2018-04-11 16:17:15'),
(71, 69, 'ii. Rangka Horisontal', 'kg', 0, '2018-04-11 16:17:31', '2018-04-11 16:17:31'),
(72, 68, '- Rangka Diagonal', NULL, 0, '2018-04-11 16:18:10', '2018-04-11 16:18:10'),
(73, 72, 'i.  Rangka Diagonal Cremona', 'kg', 0, '2018-04-11 16:18:21', '2018-04-11 16:18:21'),
(74, 72, 'ii. Rangka Deep Cremona', 'kg', 0, '2018-04-11 16:18:37', '2018-04-11 16:18:37'),
(75, NULL, 'PEMASANGAN PANEL PENUTUP ATAS, BAWAH, KIRI, KANAN', NULL, 0, '2018-04-11 16:21:45', '2018-04-11 16:21:45'),
(76, 75, 'bahan Aluminium Composite Panel ex. Seven warna Silver tebal 4 mm', NULL, 0, '2018-04-11 16:22:06', '2018-04-11 16:22:06'),
(77, 76, 'a.  Penutup Atas (ceiling)', 'm2', 0, '2018-04-11 16:22:24', '2018-04-11 16:22:24'),
(78, 76, 'b.  Penutup Bawah', 'm2', 0, '2018-04-11 16:22:39', '2018-04-11 16:22:39'),
(79, 76, 'c.   Penutup Kiri', 'm2', 0, '2018-04-11 16:22:59', '2018-04-11 16:22:59'),
(80, 76, 'd.  Penutup Kanan', 'm2', 0, '2018-04-11 16:23:16', '2018-04-11 16:23:16'),
(81, 6, 'bahan plat aluminium tebal 0,5 mm', NULL, 0, '2018-04-11 16:24:00', '2018-04-11 16:24:00'),
(82, 6, '(untuk bagian façade yang tingginya melebihi bangunan)', NULL, 0, '2018-04-11 16:24:14', '2018-04-11 16:24:14'),
(83, NULL, 'PEKERJAAN INSTALASI PANEL NANO & ACM', NULL, 0, '2018-04-11 16:24:40', '2018-04-11 16:24:40'),
(84, 83, 'a.  Eksterior Type S', 'm2', 0, '2018-04-11 16:25:01', '2018-04-11 16:25:01'),
(85, 84, '-  Panel Nano Tipe E (LOGO), 2384 x 2390', 'set', 0, '2018-04-11 16:25:17', '2018-04-11 16:25:17'),
(86, 84, '- Panel Nano Tipe F, 1800 x 2390', 'set', 0, '2018-04-11 16:25:30', '2018-04-11 16:25:30'),
(87, 84, '- Panel Nano Tipe G, 800 x 2390', 'set', 0, '2018-04-11 16:25:41', '2018-04-11 16:25:41'),
(88, 84, '- Panel Nano Tipe H, 450 x 2390', 'set', 0, '2018-04-11 16:25:53', '2018-04-11 16:25:53'),
(89, 84, '- ACM Panel End M11, 766,5 x 1782', 'set', 0, '2018-04-11 16:26:04', '2018-04-11 16:26:04'),
(90, 84, '- ACM Panel End M12, 766,5 x 1608', 'set', 0, '2018-04-11 16:26:14', '2018-04-11 16:26:14'),
(91, 84, '- ACM Panel Round M13, 245 x 1782', 'set', 0, '2018-04-11 16:26:23', '2018-04-11 16:26:23'),
(92, 84, '- ACM Panel Round M14, 245 x 1608', 'set', 0, '2018-04-11 16:26:31', '2018-04-11 16:26:31'),
(93, 84, '-  ACM Panel Bottom M15, 1187 x 500', 'set', 0, '2018-04-11 16:26:43', '2018-04-11 16:26:43'),
(94, 84, '-  ACM Panel Bottom M16, 1800 x 500', 'set', 0, '2018-04-11 16:26:52', '2018-04-11 16:26:52'),
(95, 84, '- ACM Panel Bottom M17, 800 x 500', 'set', 0, '2018-04-11 16:27:02', '2018-04-11 16:27:02'),
(96, 84, '- ACM Panel Bottom M18, 450 x 500', 'set', 0, '2018-04-11 16:27:10', '2018-04-11 16:27:10'),
(97, 84, '- Box Up Letter & Logo Tipe S', 'set', 0, '2018-04-11 16:27:17', '2018-04-11 16:27:17'),
(98, 84, '- Fin I Light Box I Reflective Pane LED Bar', 'set', 0, '2018-04-11 16:27:25', '2018-04-11 16:27:25'),
(99, 84, '-  Lettering  \"NUSANTARA SAKTI \"', 'set', 0, '2018-04-11 16:27:32', '2018-04-11 16:27:32'),
(100, 83, 'b.  Eksterior Type M', 'm2', 0, '2018-04-11 16:28:16', '2018-04-11 16:28:16'),
(101, 100, '- Panel Nano Tipe A (LOGO), 3370 x 3790', 'set', 0, '2018-04-11 16:28:33', '2018-04-11 16:28:33'),
(102, 100, '- Panel Nano Tipe B, 1800 x 3790', 'set', 0, '2018-04-11 16:28:41', '2018-04-11 16:28:41'),
(103, 100, '- Panel Nano Tipe C, 800 x 3790', 'set', 0, '2018-04-11 16:28:49', '2018-04-11 16:28:49'),
(104, 100, '- Panel Nano Tipe D, 450 x 3790', 'set', 0, '2018-04-11 16:28:57', '2018-04-11 16:28:57'),
(105, 100, '- ACM Panel End M1, 795 x 1697', 'set', 0, '2018-04-11 16:29:05', '2018-04-11 16:29:05'),
(106, 100, '- ACM Panel End M2, 795 x 1757', 'set', 0, '2018-04-11 16:29:15', '2018-04-11 16:29:15'),
(107, 100, '- ACM Panel End M3, 795 x 1526', 'set', 0, '2018-04-11 16:29:22', '2018-04-11 16:29:22'),
(108, 100, '- ACM Panel Round M4, 245 x 1697', 'set', 0, '2018-04-11 16:29:29', '2018-04-11 16:29:29'),
(109, 100, '- ACM Panel Round M5, 245 x 1757', 'set', 0, '2018-04-11 16:29:43', '2018-04-11 16:29:43'),
(110, 100, '- ACM Panel Round M6, 245 x 1526', 'set', 0, '2018-04-11 16:29:51', '2018-04-11 16:29:51'),
(111, 100, '- ACM Panel Bottom M7, 1680 x 700', 'set', 0, '2018-04-11 16:29:59', '2018-04-11 16:29:59'),
(112, 100, '- ACM Panel Bottom M8, 1800 x 700', 'set', 0, '2018-04-11 16:30:06', '2018-04-11 16:30:06'),
(113, 100, '- ACM Panel Bottom M9, 800 x 700', 'set', 0, '2018-04-11 16:30:15', '2018-04-11 16:30:15'),
(114, 100, '- ACM Panel Bottom M10, 450 x 700', 'set', 0, '2018-04-11 16:30:24', '2018-04-11 16:30:24'),
(115, 100, '- Box Up Letter & Logo Tipe M', 'set', 0, '2018-04-11 16:30:35', '2018-04-11 16:30:35'),
(116, 100, '- Fin I Light Box I Reflective Pane LED Bar', 'mm', 0, '2018-04-11 16:30:42', '2018-04-11 16:30:42'),
(117, 100, '-  Lettering \" CITRA MOTOR \"', 'pcs', 0, '2018-04-11 16:30:52', '2018-04-11 16:30:52'),
(118, NULL, 'PEMASANGAN PROJECTING SIGN', NULL, 0, '2018-04-11 16:31:32', '2018-04-11 16:31:32'),
(119, 118, 'a.   Pemasangan Wing AHM', 'set', 0, '2018-04-11 16:31:50', '2018-04-11 16:31:50'),
(120, 118, 'b.  Pemasangan Nama Dealer & Fasilitas Layanan', 'set', 0, '2018-04-11 16:32:00', '2018-04-11 16:32:00'),
(121, 118, 'c.  Pemasangan perkuatan Projecting Sign', 'set', 0, '2018-04-11 16:32:09', '2018-04-11 16:32:09'),
(122, 9, 'c.   Instalasi LED untuk Logo Honda di Eksterior', 'ls', 0, '2018-04-11 16:32:42', '2018-04-11 16:32:42'),
(123, NULL, 'PEKERJAAN INSTALASI INTERIOR', NULL, 0, '2018-04-11 16:34:11', '2018-04-11 16:34:11'),
(124, 123, 'a.  Sales/ Finance/ Parts Front (panjang 1200 mm)', 'unit', 0, '2018-04-11 16:34:38', '2018-04-11 16:34:38'),
(125, 123, 'b.  Service Front Desk (panjang 1500 mm)', 'unit', 0, '2018-04-11 16:34:53', '2018-04-11 16:34:53'),
(126, 123, 'c.  Service Advisor Desk', 'unit', 0, '2018-04-11 16:35:08', '2018-04-11 16:35:08'),
(127, 123, 'd.   Hero Stage', 'unit', 0, '2018-04-11 16:35:19', '2018-04-11 16:35:19'),
(128, 123, 'e.   Isolate Stage', 'unit', 0, '2018-04-11 16:35:28', '2018-04-11 16:35:28'),
(129, 123, 'f.   Wall System Display', 'unit', 0, '2018-04-11 16:35:37', '2018-04-11 16:35:37'),
(130, 123, 'g.  Glass Showcase', 'unit', 0, '2018-04-11 16:35:47', '2018-04-11 16:35:47'),
(131, 123, 'h.  Spec Stand', 'unit', 0, '2018-04-11 16:35:59', '2018-04-11 16:35:59'),
(132, 123, 'i.  Brochure Rack', 'unit', 0, '2018-04-11 16:36:10', '2018-04-11 16:36:10'),
(133, 123, 'j.   Internet Counter', 'unit', 0, '2018-04-11 16:36:24', '2018-04-11 16:36:24'),
(134, 123, 'k.  CI Logo', 'unit', 0, '2018-04-11 16:36:36', '2018-04-11 16:36:36'),
(135, 123, 'l.   Direction Sign type 1', 'unit', 0, '2018-04-11 16:36:45', '2018-04-11 16:36:45'),
(136, 123, 'm.  Direction Sign type 2 (Hanging on Service Pit)', 'unit', 0, '2018-04-11 16:36:54', '2018-04-11 16:36:54'),
(137, 123, 'n.  Direction Sign type 3 (Projecting Type) \"KASIR\"', 'unit', 0, '2018-04-11 16:37:05', '2018-04-11 16:37:05'),
(138, 123, 'o.  Direction Sign type 4', 'unit', 0, '2018-04-11 16:37:14', '2018-04-11 16:37:14'),
(139, 123, 'p.  9 Panel Poster Display', 'unit', 0, '2018-04-11 16:37:25', '2018-04-11 16:37:25'),
(140, 123, 'q.  Poster Display', 'unit', 0, '2018-04-11 16:37:36', '2018-04-11 16:37:36'),
(141, 123, 'r.  Sparepart Display & Stock Rack', 'unit', 0, '2018-04-11 16:37:44', '2018-04-11 16:37:44'),
(142, 123, 's.  Helmet Rack', 'unit', 0, '2018-04-11 16:37:55', '2018-04-11 16:37:55'),
(143, 123, 't.  Magazine & Newspaper Rack', 'unit', 0, '2018-04-11 16:38:05', '2018-04-11 16:38:05'),
(144, 123, 'u.  HRT Banner Stand', 'unit', 0, '2018-04-11 16:38:13', '2018-04-11 16:38:13'),
(145, 123, 'v.  Island Display', 'unit', 0, '2018-04-11 16:38:21', '2018-04-11 16:38:21'),
(146, 132, '-  Self Standing', 'unit', 0, '2018-04-11 16:38:45', '2018-04-11 16:38:45'),
(147, 132, '-  Wall Mounted', 'unit', 0, '2018-04-11 16:38:57', '2018-04-11 16:38:57'),
(148, 135, '-  \"Bengkel\"', 'unit', 0, '2018-04-11 16:39:33', '2018-04-11 16:39:33'),
(149, 135, '- \"Suku Cadang\"', 'unit', 0, '2018-04-11 16:39:41', '2018-04-11 16:39:41'),
(150, 135, '- \"Penjualan\"', 'unit', 0, '2018-04-11 16:39:49', '2018-04-11 16:39:49'),
(151, 135, '- \"Pembiayaan\"', 'unit', 0, '2018-04-11 16:39:58', '2018-04-11 16:39:58'),
(152, 135, '- \"Ruang Tunggu\"', 'unit', 0, '2018-04-11 16:40:10', '2018-04-11 16:40:10'),
(153, 135, '-  \"Toilet\"', 'unit', 0, '2018-04-11 16:40:17', '2018-04-11 16:40:17'),
(154, 135, '-  \"Kasir\"', 'unit', 0, '2018-04-11 16:40:26', '2018-04-11 16:40:26'),
(155, 138, '-  \"Smoking\"', 'unit', 0, '2018-04-11 16:40:53', '2018-04-11 16:40:53'),
(156, 138, '-  \"No Smoking\"', 'unit', 0, '2018-04-11 16:41:02', '2018-04-11 16:41:02'),
(157, 138, '- \"Female Toilet\"', 'unit', 0, '2018-04-11 16:41:11', '2018-04-11 16:41:11'),
(158, 138, '-  \"Male Toilet\"', 'unit', 0, '2018-04-11 16:41:19', '2018-04-11 16:41:19'),
(159, 138, '-  \"Toilet\"', 'unit', 0, '2018-04-11 16:41:27', '2018-04-11 16:41:27'),
(160, NULL, 'PEKERJAAN PONDASI BETON BERTULANG', NULL, 0, '2018-04-11 16:42:35', '2018-04-11 16:42:35'),
(161, 160, 'a.  Kondisi tanah Normal', NULL, 0, '2018-04-11 16:42:51', '2018-04-11 16:42:51'),
(162, 160, 'b.  Kondisi tanah Keras', NULL, 0, '2018-04-11 16:42:57', '2018-04-11 16:42:57'),
(163, 160, 'c. Kondisi tanah Gambut', NULL, 0, '2018-04-11 16:43:06', '2018-04-11 16:43:06'),
(164, 161, '-  Pekerjaan galian tanah', 'm3', 0, '2018-04-11 16:43:40', '2018-04-11 16:43:40'),
(165, 161, '-  Pondasi beton bertulang', 'm3', 0, '2018-04-11 16:43:51', '2018-04-11 16:43:51'),
(166, 161, '-  Anchor Bolt', 'bh', 0, '2018-04-11 16:44:06', '2018-04-11 16:44:06'),
(167, 161, '-  Grouting', 'm2', 0, '2018-04-11 16:44:20', '2018-04-11 16:44:20'),
(168, 161, '- Pekerjaan anti rayap metode suntik atau semprot Chemical Barrier System (Repellent)', 'm\'', 0, '2018-04-11 16:44:34', '2018-04-11 16:44:34'),
(169, 161, '-  Stiffner', 'kg', 0, '2018-04-11 16:44:43', '2018-04-11 16:44:43'),
(170, 161, '-  Base Plate', 'kg', 0, '2018-04-11 16:44:56', '2018-04-11 16:44:56'),
(171, 162, '-  Pekerjaan galian tanah', 'm3', 0, '2018-04-11 16:45:25', '2018-04-11 16:45:25'),
(172, 162, '- Pasir alas dipadatkan', 'm3', 0, '2018-04-11 16:45:37', '2018-04-11 16:45:37'),
(173, 162, '- Lantai Kerja Beton', 'm3', 0, '2018-04-11 16:45:47', '2018-04-11 16:45:47'),
(174, 162, '-  Pondasi beton bertulang', 'm3', 0, '2018-04-11 16:45:58', '2018-04-11 16:45:58'),
(175, 162, '- Plat baja dasar tiang & stiffner', 'kg', 0, '2018-04-11 16:46:06', '2018-04-11 16:46:06'),
(176, 162, '-  Anchor Bolt', 'bh', 0, '2018-04-11 16:46:17', '2018-04-11 16:46:17'),
(177, 162, '-  Grouting', 'm2', 0, '2018-04-11 16:46:26', '2018-04-11 16:46:26'),
(178, 162, '- Pekerjaan anti rayap metode suntik atau semprot Chemical Barrier System (Repellent)', 'm\'', 0, '2018-04-11 16:46:37', '2018-04-11 16:46:37'),
(179, 162, '-  Stiffner', 'kg', 0, '2018-04-11 16:46:52', '2018-04-11 16:46:52'),
(180, 162, '-  Base Plate', 'kg', 0, '2018-04-11 16:47:10', '2018-04-11 16:47:10'),
(181, 163, '-  Pekerjaan galian tanah', 'm3', 0, '2018-04-11 16:47:35', '2018-04-11 16:47:35'),
(182, 163, '-  Tanah datang dipadatkan', 'm3', 0, '2018-04-11 16:47:45', '2018-04-11 16:47:45'),
(183, 163, '-  Cerucuk penahan tanah', 'btg', 0, '2018-04-11 16:48:03', '2018-04-11 16:48:03'),
(184, 163, '-  Cerucuk dasar pondasi', 'btg', 0, '2018-04-11 16:48:16', '2018-04-11 16:48:16'),
(185, 163, '-  Pondasi beton bertulang', 'm3', 0, '2018-04-11 16:48:24', '2018-04-11 16:48:24'),
(186, 163, '-  Pasir alas dipadatkan, t=10 cm', 'm3', 0, '2018-04-11 16:48:31', '2018-04-11 16:48:31'),
(187, 163, '-  Sirtu dipadatkan', 'm3', 0, '2018-04-11 16:48:39', '2018-04-11 16:48:39'),
(188, 163, '-  Lantai Kerja Beton 1:3:5, t=5 cm', 'm3', 0, '2018-04-11 16:48:48', '2018-04-11 16:48:48'),
(189, 163, '-  Anchor Bolt M22', 'bh', 0, '2018-04-11 16:48:57', '2018-04-11 16:48:57'),
(190, 163, '-  Grouting, t=20 mm', 'm2', 0, '2018-04-11 16:49:06', '2018-04-11 16:49:06'),
(191, 163, '-  Pekerjaan anti rayap metode suntik atau semprot Chemical Barrier System (Repellent)', 'm\'', 0, '2018-04-11 16:49:17', '2018-04-11 16:49:17'),
(192, 163, '-  Plat baja dasar tiang & Stiffner', 'kg', 0, '2018-04-11 16:49:24', '2018-04-11 16:49:24'),
(193, 163, '-  Base Plate', 'kg', 0, '2018-04-11 16:49:32', '2018-04-11 16:49:32'),
(194, NULL, 'PEKERJAAN PEMASANGAN SIGNPOLE', NULL, 0, '2018-04-11 16:50:22', '2018-04-11 16:50:22'),
(195, 194, 'a.  Sign Pole Tinggi 9 m', NULL, 0, '2018-04-11 16:50:35', '2018-04-11 16:50:35'),
(196, 194, 'b.  Sign Pole Tinggi 12 m', NULL, 0, '2018-04-11 16:50:43', '2018-04-11 16:50:43'),
(197, 195, '-  Pemasangan Panel Wing AHM 2600 x 2500', 'set', 0, '2018-04-11 16:50:59', '2018-04-11 16:50:59'),
(198, 195, '-  Pemasangan Panel nama Dealer 1800 x 2000', 'set', 0, '2018-04-11 16:51:07', '2018-04-11 16:51:07'),
(199, 195, '-  Pengadaan dan pemasangan tiang Signpole dia. 250mm, t = 10 mm', 'm\'', 0, '2018-04-11 16:51:17', '2018-04-11 16:51:17'),
(200, 195, 'Finishing cat duco warna putih Briliant White ex. Dulux (termasuk alat bantu seperti		 scafolding & crane)', NULL, 0, '2018-04-11 16:51:40', '2018-04-11 16:51:40'),
(201, 196, '-  Pemasangan Panel Wing AHM 2600 x 2500', 'set', 0, '2018-04-11 16:52:06', '2018-04-11 16:52:06'),
(202, 196, '-  Pemasangan Panel nama Dealer 1800 x 2000', 'set', 0, '2018-04-11 16:52:13', '2018-04-11 16:52:13'),
(203, 196, '-  Pengadaan dan pemasangan tiang Signpole dia. 250mm, t = 10 mm', 'm\'', 0, '2018-04-11 16:52:20', '2018-04-11 16:52:20'),
(204, 196, 'Finishing cat duco warna putih Briliant White ex. Dulux (termasuk alat bantu seperti		 scafolding & crane)', NULL, 0, '2018-04-11 16:52:27', '2018-04-11 16:52:27'),
(205, NULL, 'INSTALASI LISTRIK 2', NULL, 0, '2018-04-11 16:53:36', '2018-04-11 16:53:36'),
(206, 205, 'a. Instalasi Neon Light Pylon', 'ls', 0, '2018-04-11 16:53:54', '2018-04-11 16:53:54'),
(207, 205, 'b. Instalasi LED untuk Logo Honda di Pylon', 'ls', 0, '2018-04-11 16:54:05', '2018-04-11 16:54:05');

-- --------------------------------------------------------

--
-- Table structure for table `weekly_item_dealer`
--

CREATE TABLE `weekly_item_dealer` (
  `id_weekly_item_dealer` int(11) NOT NULL,
  `id_dealer` int(11) NOT NULL,
  `id_weekly_item` int(11) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weekly_item_dealer`
--

INSERT INTO `weekly_item_dealer` (`id_weekly_item_dealer`, `id_dealer`, `id_weekly_item`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 2, 9, 0, '2018-02-25 22:02:06', '2018-02-25 22:02:06'),
(2, 3, 9, 1, '2018-02-25 22:02:06', '2018-02-25 22:02:06'),
(3, 1, 10, 0, '2018-02-25 22:02:38', '2018-02-25 22:02:38'),
(4, 2, 10, 0, '2018-02-25 22:02:39', '2018-02-25 22:02:39'),
(7, 1, 9, 0, '2018-03-02 02:26:36', '2018-03-02 02:26:36'),
(8, 3, 9, 0, '2018-03-02 02:26:36', '2018-03-02 02:26:36'),
(9, 1, 5, 0, '2018-03-09 02:03:31', '2018-03-09 02:03:31'),
(10, 1, 2, 0, '2018-03-21 08:59:22', '2018-03-21 08:59:22'),
(11, 1, 7, 0, '2018-03-21 10:34:49', '2018-03-21 10:34:49'),
(12, 2, 7, 0, '2018-03-21 10:34:49', '2018-03-21 10:34:49'),
(13, 1, 75, 0, '2018-04-11 16:21:45', '2018-04-11 16:21:45'),
(14, 1, 83, 0, '2018-04-11 16:24:40', '2018-04-11 16:24:40'),
(15, 1, 118, 0, '2018-04-11 16:31:32', '2018-04-11 16:31:32'),
(16, 1, 123, 0, '2018-04-11 16:34:12', '2018-04-11 16:34:12'),
(17, 1, 160, 0, '2018-04-11 16:42:35', '2018-04-11 16:42:35'),
(18, 1, 194, 0, '2018-04-11 16:50:22', '2018-04-11 16:50:22'),
(19, 1, 205, 0, '2018-04-11 16:53:36', '2018-04-11 16:53:36');

-- --------------------------------------------------------

--
-- Table structure for table `weekly_report`
--

CREATE TABLE `weekly_report` (
  `id_weekly_report` int(11) NOT NULL,
  `id_pengawas` int(11) NOT NULL,
  `date_report` date NOT NULL,
  `approved` int(1) NOT NULL DEFAULT 0,
  `approved_by` int(11) DEFAULT NULL,
  `approval_note` text DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weekly_report`
--

INSERT INTO `weekly_report` (`id_weekly_report`, `id_pengawas`, `date_report`, `approved`, `approved_by`, `approval_note`, `created_at`, `updated_at`) VALUES
(1, 3, '2018-03-01', 0, 2, 'tes aja', '2018-03-09 02:51:32', '2018-03-13 08:40:08');

-- --------------------------------------------------------

--
-- Table structure for table `weekly_report_detail`
--

CREATE TABLE `weekly_report_detail` (
  `id_weekly_report_detail` int(11) NOT NULL,
  `id_weekly_report` int(11) NOT NULL,
  `id_weekly_item` int(11) NOT NULL,
  `volume` varchar(15) DEFAULT NULL,
  `persentase` varchar(15) DEFAULT NULL,
  `keterangan` varchar(20) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weekly_report_detail`
--

INSERT INTO `weekly_report_detail` (`id_weekly_report_detail`, `id_weekly_report`, `id_weekly_item`, `volume`, `persentase`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 1, 12, '444', '33', 'on progres', '2018-03-09 02:54:56', '2018-03-09 02:54:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absen`
--
ALTER TABLE `absen`
  ADD PRIMARY KEY (`id_absen`);

--
-- Indexes for table `daily_mom`
--
ALTER TABLE `daily_mom`
  ADD PRIMARY KEY (`id_daily_mom`);

--
-- Indexes for table `daily_problem`
--
ALTER TABLE `daily_problem`
  ADD PRIMARY KEY (`id_daily_problem`);

--
-- Indexes for table `daily_report`
--
ALTER TABLE `daily_report`
  ADD PRIMARY KEY (`id_daily_report`);

--
-- Indexes for table `daily_report_progress`
--
ALTER TABLE `daily_report_progress`
  ADD PRIMARY KEY (`id_daily_report_progress`);

--
-- Indexes for table `dealer`
--
ALTER TABLE `dealer`
  ADD PRIMARY KEY (`id_dealer`);

--
-- Indexes for table `kontraktor`
--
ALTER TABLE `kontraktor`
  ADD PRIMARY KEY (`id_kontraktor`);

--
-- Indexes for table `main_dealer`
--
ALTER TABLE `main_dealer`
  ADD PRIMARY KEY (`id_main_dealer`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monthly_report`
--
ALTER TABLE `monthly_report`
  ADD PRIMARY KEY (`id_monthly_report`);

--
-- Indexes for table `pengawas`
--
ALTER TABLE `pengawas`
  ADD PRIMARY KEY (`id_pengawas`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supervisor`
--
ALTER TABLE `supervisor`
  ADD PRIMARY KEY (`id_supervisor`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_category`
--
ALTER TABLE `user_category`
  ADD PRIMARY KEY (`id_user_category`);

--
-- Indexes for table `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id_user_token`);

--
-- Indexes for table `weekly_item`
--
ALTER TABLE `weekly_item`
  ADD PRIMARY KEY (`id_weekly_item`);

--
-- Indexes for table `weekly_item_dealer`
--
ALTER TABLE `weekly_item_dealer`
  ADD PRIMARY KEY (`id_weekly_item_dealer`);

--
-- Indexes for table `weekly_report`
--
ALTER TABLE `weekly_report`
  ADD PRIMARY KEY (`id_weekly_report`);

--
-- Indexes for table `weekly_report_detail`
--
ALTER TABLE `weekly_report_detail`
  ADD PRIMARY KEY (`id_weekly_report_detail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absen`
--
ALTER TABLE `absen`
  MODIFY `id_absen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `daily_mom`
--
ALTER TABLE `daily_mom`
  MODIFY `id_daily_mom` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `daily_problem`
--
ALTER TABLE `daily_problem`
  MODIFY `id_daily_problem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `daily_report`
--
ALTER TABLE `daily_report`
  MODIFY `id_daily_report` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `daily_report_progress`
--
ALTER TABLE `daily_report_progress`
  MODIFY `id_daily_report_progress` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `dealer`
--
ALTER TABLE `dealer`
  MODIFY `id_dealer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kontraktor`
--
ALTER TABLE `kontraktor`
  MODIFY `id_kontraktor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `main_dealer`
--
ALTER TABLE `main_dealer`
  MODIFY `id_main_dealer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `monthly_report`
--
ALTER TABLE `monthly_report`
  MODIFY `id_monthly_report` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pengawas`
--
ALTER TABLE `pengawas`
  MODIFY `id_pengawas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supervisor`
--
ALTER TABLE `supervisor`
  MODIFY `id_supervisor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_category`
--
ALTER TABLE `user_category`
  MODIFY `id_user_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id_user_token` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=687;

--
-- AUTO_INCREMENT for table `weekly_item`
--
ALTER TABLE `weekly_item`
  MODIFY `id_weekly_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;

--
-- AUTO_INCREMENT for table `weekly_item_dealer`
--
ALTER TABLE `weekly_item_dealer`
  MODIFY `id_weekly_item_dealer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `weekly_report`
--
ALTER TABLE `weekly_report`
  MODIFY `id_weekly_report` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `weekly_report_detail`
--
ALTER TABLE `weekly_report_detail`
  MODIFY `id_weekly_report_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
